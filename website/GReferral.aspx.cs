﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

public partial class GReferral : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            btnReferral.Visible = false;
        }
    }
    
    public bool AddUserToDB_Referral(string UID, int ReferredBy)
    {
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;
        SqlConnection con = null;
        SqlCommand cmd = null;

        String sQuery = "INSERT INTO P_Referral (UID, ReferredBy) VALUES ('" + UID + "','" + ReferredBy + "')";

        try
        {
            con = new SqlConnection(conString);
            cmd = new SqlCommand(sQuery, con);
            con.Open();
            cmd.ExecuteNonQuery();

        }

        catch
        {

        }

        finally
        {
            con.Close();
        }

        return true;
    }

    protected void btnReferral_Click(object sender, EventArgs e)
    {
        DataTable iDataTable = new DataTable();
        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

        iSqlConnection.Open();

        string UID = "";

        if (Session["UID"] != null)
        {
            UID = Session["UID"].ToString();
        }

        int ReferredBy = Convert.ToInt32(Label2.Text);

        AddUserToDB_Referral(UID, ReferredBy);

        btnReferral.Visible = false;

        txtSearch.Visible = false;

        btnSearch.Visible = false;

        Label1.Text = "Thanks, your referral has been submitted!";


    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

        string srch = txtSearch.Text;

        string connectionStr = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();

        System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(connectionStr);

        connection.Open();

        string commandStr = "SELECT PID, FirstName, LastName, Picture FROM P_Profile WHERE FirstName LIKE " + "'%" + srch + "%'" + " OR LastName LIKE " + "'%" + srch + "%'";

        System.Data.SqlClient.SqlCommand myCommand = new System.Data.SqlClient.SqlCommand(commandStr, connection);

        System.Data.SqlClient.SqlDataReader myReader = myCommand.ExecuteReader();





        GV.DataSource = myReader;

        GV.DataBind();

        myReader.Close();

        string retGV = Convert.ToString(myCommand.ExecuteScalar());

        if (retGV == "")
        {
            Label3.Text = "Your search result returned no value. Please search again";
            btnReferral.Visible = false;

        }
        else
        {
            Label3.Visible = false;

        }

        connection.Close();

    }

    protected void GV_SelectedIndexChanged(object sender, EventArgs e)
    {
        btnReferral.Visible = true;
        GridViewRow row = GV.SelectedRow;

        string fname = row.Cells[0].Text;
        string lname = row.Cells[1].Text;

        btnReferral.Text = "Add as your referrar: " + fname + " " + lname;

        Label2.Text = row.Cells[2].Text;

        GV.Visible = false;
    }


    public String FrmImg(String str)
    {
        if (str != "")
            return ("files/P/picture/" + str);
        else
            return "images/no_picture_small.gif";


    }

}