﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Net;

/// <summary>
/// Summary description for Transaction
/// </summary>
public class Transaction
{
	public Transaction()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static TransactionResponseInfo ProcessPayment(
        TransactionRequestInfo transaction, AuthNetAccountInfo account)
    {
        TransactionResponseInfo response = new TransactionResponseInfo();

        WebClient objRequest = new WebClient();
        System.Collections.Specialized.NameValueCollection objInf =
          new System.Collections.Specialized.NameValueCollection(30);
        //System.Collections.Specialized.NameValueCollection objRetInf =
        //  new System.Collections.Specialized.NameValueCollection(30);
        byte[] objRetBytes;
        string[] objRetVals;
        string strError;

        #region Set Request Values
        objInf.Add("x_version", account.AuthNetVersion);
        objInf.Add("x_delim_data", "True");
        objInf.Add("x_login", account.AuthNetLoginID);
        // objInf.Add("x_password", account.AuthNetPassword);
        objInf.Add("x_tran_key", account.AuthNetTransKey);
        objInf.Add("x_relay_response", "False");
        objInf.Add("x_delim_char", ",");
        objInf.Add("x_encap_char", "|");

        // Billing Address
        objInf.Add("x_first_name", transaction.FirstName);
        objInf.Add("x_last_name", transaction.LastName);
        objInf.Add("x_address", transaction.Address);
        objInf.Add("x_city", transaction.City);
        objInf.Add("x_state", transaction.State);
        objInf.Add("x_zip", transaction.Zip);
        objInf.Add("x_country", transaction.Country);

        // Card Details
        objInf.Add("x_card_num", transaction.CardNumber);
        objInf.Add("x_exp_date", transaction.ExpDate);

        // Authorization code of the card (CCV)
        objInf.Add("x_card_code", transaction.SecurityCode);

        objInf.Add("x_method", "CC");
        objInf.Add("x_type", "AUTH_CAPTURE");
        objInf.Add("x_amount", transaction.ChargeAmount.ToString());
        objInf.Add("x_description", transaction.Description);

        // Currency setting. Check the guide for other supported currencies
        objInf.Add("x_currency_code", "USD");

        if (account.IsTestMode)
        {
            // Pure Test Server
            objInf.Add("x_test_request", "True");
            objRequest.BaseAddress =
              "https://test.authorize.net/gateway/transact.dll";
        }
        else if (!account.IsTestMode)
        {
            // Actual Server
            objInf.Add("x_test_request", "False");
            objRequest.BaseAddress =
              "https://secure.authorize.net/gateway/transact.dll";
        }
        else
        {
            throw new Exception("Transaction Mode Invalid");
        }
        #endregion

        try
        {
            // POST request
            objRetBytes =
              objRequest.UploadValues(objRequest.BaseAddress, "POST", objInf);
            objRetVals =
              System.Text.Encoding.ASCII.GetString(objRetBytes).Split(",".ToCharArray());

            // Process Return Values
            response.ReturnCode = objRetVals[0].Trim(char.Parse("|"));

            if (objRetVals[0].Trim(char.Parse("|")) == "1")
            {
                // Returned Authorisation Code
                response.AuthorizationCode = objRetVals[4].Trim(char.Parse("|"));
                // Returned Transaction ID
                response.TransactionID = objRetVals[6].Trim(char.Parse("|"));
                strError = "Transaction completed successfully.";
            }
            else
            {
                // Error!
                strError = objRetVals[3].Trim(char.Parse("|")) + " (" +
                  objRetVals[2].Trim(char.Parse("|")) + ")";

                if (objRetVals[2].Trim(char.Parse("|")) == "44")
                {
                    // CCV transaction decline
                    strError += "Our Card Code Verification (CCV) returned " +
                      "the following error: ";

                    switch (objRetVals[38].Trim(char.Parse("|")))
                    {
                        case "N":
                            strError += "Card Code does not match.";
                            break;
                        case "P":
                            strError += "Card Code was not processed.";
                            break;
                        case "S":
                            strError += "Card Code should be on card but was not indicated.";
                            break;
                        case "U":
                            strError += "Issuer was not certified for Card Code.";
                            break;
                    }
                }

                if (objRetVals[2].Trim(char.Parse("|")) == "45")
                {
                    if (strError.Length > 1)
                        strError += "n";

                    // AVS transaction decline
                    strError += "Our Address Verification System (AVS) " +
                      "returned the following error: ";

                    switch (objRetVals[5].Trim(char.Parse("|")))
                    {
                        case "A":
                            strError += " the zip code entered does not match " +
                              "the billing address.";
                            break;
                        case "B":
                            strError += " no information was provided for the AVS check.";
                            break;
                        case "E":
                            strError += " a general error occurred in the AVS system.";
                            break;
                        case "G":
                            strError += " the credit card was issued by a non-US bank.";
                            break;
                        case "N":
                            strError += " neither the entered street address nor zip " +
                              "code matches the billing address.";
                            break;
                        case "P":
                            strError += " AVS is not applicable for this transaction.";
                            break;
                        case "R":
                            strError += " please retry the transaction; the AVS system " +
                              "was unavailable or timed out.";
                            break;
                        case "S":
                            strError += " the AVS service is not supported by your " +
                              "credit card issuer.";
                            break;
                        case "U":
                            strError += " address information is unavailable for the " +
                              "credit card.";
                            break;
                        case "W":
                            strError += " the 9 digit zip code matches, but the " +
                              "street address does not.";
                            break;
                        case "Z":
                            strError += " the zip code matches, but the address does not.";
                            break;
                    }
                }

            }
        }
        catch (Exception ex)
        {
            strError = ex.Message;
        }

        response.Message = strError;

        return response;
    }
}
