﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for TransactionResponseInfo
/// </summary>
public class TransactionResponseInfo
{
	public TransactionResponseInfo()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private string _authorizationCode;
    public string AuthorizationCode
    {
        get
        {
            return _authorizationCode;

        }
        set
        {
            _authorizationCode = value;
        }
    }
    private string _transactionID;
    public string TransactionID
    {
        get
        {
            return _transactionID;

        }
        set
        {
            _transactionID = value;
        }
    }
    private string _returnCode;
    //public string TransactionID { get; set; }
    public string ReturnCode
    {
        get
        {
            return _returnCode;

        }
        set
        {
            _returnCode = value;
        }
    }
    private string _message;

    public string Message
    {
        get
        {
            return _message;

        }
        set
        {
            _message = value;
        }
    }
}