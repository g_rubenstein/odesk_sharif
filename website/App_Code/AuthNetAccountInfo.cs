﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for AuthNetAccountInfo
/// </summary>
public class AuthNetAccountInfo
{
    private string _authNetVersion;
    public string AuthNetVersion
    {
        get
        {
            return _authNetVersion;

        }
        set
        {
            _authNetVersion = value;
        }
    }
    private string _authNetLoginID;
    public string AuthNetLoginID
    {
        get
        {
            return _authNetLoginID;

        }
        set
        {
            _authNetLoginID = value;
        }
    }
   
    // public string AuthNetPassword { get; set; }
    private string _authNetTransKey;
    public string AuthNetTransKey
    {
        get
        {
            return _authNetTransKey;

        }
        set
        {
            _authNetTransKey = value;
        }
    }
    bool _isTestMode;
    public bool IsTestMode
    {
        get
        {
            return _isTestMode;

        }
        set
        {
            _isTestMode = value;
        }
    }
}