﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for TransactionRequestInfo
/// </summary>
public class TransactionRequestInfo
{
    private string _firstName;
    public string FirstName {
        get
        {
            return _firstName;
        
        } 
        set{
            _firstName = value;
        } 
    }
    private string _lastName;
    public string LastName
    {
        get
        {
            return _lastName;

        }
        set
        {
            _lastName = value;
        }
    }
    private string _address;
    public string Address
    {
        get
        {
            return _address;

        }
        set
        {
            _address = value;
        }
    }
    private string _city;
    public string City
    {
        get
        {
            return _city;

        }
        set
        {
            _city = value;
        }
    }
    private string _state;
    public string State
    {
        get
        {
            return _state;

        }
        set
        {
            _state = value;
        }
    }
    private string _country;
    public string Country
    {
        get
        {
            return _country;

        }
        set
        {
            _country = value;
        }
    }
    private string _description;
    public string Description
    {
        get
        {
            return _description;

        }
        set
        {
            _description = value;
        }
    }

    private decimal _amount;
    public decimal ChargeAmount
    {
        get
        {
            return _amount;
        }
        set
        {
            _amount = Decimal.Round(value, 2);
        }
    }

    private string _zip;
    public string Zip
    {
        get
        {
            return _zip;
        }
        set
        {
            int res;
            if (int.TryParse(value, out res))
            {
                if (res > 99999)
                {
                    throw new ArgumentException("Zip Code Value invalid");
                }
                else
                {
                    _zip = res.ToString().PadLeft(5, '0');
                }
            }
            else
            {
                throw new ArgumentException("Zip code must be numeric");
            }
        }

    }

    private string _securityCode;
    public string SecurityCode
    {
        get
        {
            return _securityCode;
        }
        set
        {
            int res;
            if (int.TryParse(value, out res))
            {
                if (res > 999)
                {
                    throw new ArgumentException("Security Code Value invalid");
                }
                else
                {
                    _securityCode = res.ToString().PadLeft(3, '0');
                }
            }
            else
            {
                throw new ArgumentException("Security code must be numeric");
            }
        }

    }

    private string _cardNumber;
    public string CardNumber
    {
        get
        {
            return _cardNumber;
        }
        set
        {
            long res;
            if (long.TryParse(value, out res))
            {
                _cardNumber = res.ToString();
            }
            else
            {
                throw new ArgumentException("Card Number may only contain numbers");
            }
        }
    }

    private string _expDate;
    public string ExpDate
    {
        get
        {
            return _expDate;
        }
        set
        {
            int res;
            if (int.TryParse(value, out res))
            {
                string exp = res.ToString().PadLeft(4, '0');
                int month = int.Parse(exp.Substring(0, 2));
                int yr = int.Parse(exp.Substring(2, 2));

                if (yr > DateTime.Now.Year ||
                    (yr == DateTime.Now.Year && month >= DateTime.Now.Month))
                {
                    _expDate = month.ToString().PadLeft(2, '0') +
                        "/" + yr.ToString().PadLeft(2, '0');
                }
                else
                {
                    throw new ArgumentException("Expiration Date already passed");
                }
            }
            else
            {
                throw new ArgumentException("Zip code must be numeric");
            }
        }
    }
}