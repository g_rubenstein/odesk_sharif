﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpOut.master" AutoEventWireup="true" CodeFile="SessionTimeOut.aspx.cs" Inherits="SessionTimeOut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        Sorry, your session has expired. Please <a href="default.aspx">click here</a> to log back in. We apologize for this inconvenience!</p>
</asp:Content>

