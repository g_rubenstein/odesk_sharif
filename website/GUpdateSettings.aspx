﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpg.master" AutoEventWireup="true" CodeFile="GUpdateSettings.aspx.cs" Inherits="GUpdateSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<br /><br />
<h1>Update Account</h1>
<br /><br />
<asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" 
    DataKeyNames="uid" DataSourceID="SqlDataSource1" DefaultMode="Edit" 
        Height="50px" Width="301px" CellPadding="5">
    <Fields>
        <asp:BoundField DataField="uid" HeaderText="USER ID" InsertVisible="False" 
            ReadOnly="True" SortExpression="uid" />
        <asp:TemplateField HeaderText="User Email" SortExpression="email">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" ReadOnly="True" 
                    Text='<%# Bind("email") %>' BackColor="#CCCCCC"></asp:TextBox>
            </EditItemTemplate>
            <InsertItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("email") %>'></asp:TextBox>
            </InsertItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# Bind("email") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Change Password" SortExpression="password">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("password") %>' 
                    TextMode="Password"></asp:TextBox>
            </EditItemTemplate>
            <InsertItemTemplate>
                <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("password") %>'></asp:TextBox>
            </InsertItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label4" runat="server" Text='<%# Bind("password") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="TCode" SortExpression="type">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" ReadOnly="True" 
                    Text='<%# Bind("type") %>' BackColor="#CCCCCC"></asp:TextBox>
            </EditItemTemplate>
            <InsertItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("type") %>'></asp:TextBox>
            </InsertItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label2" runat="server" Text='<%# Bind("type") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="status" SortExpression="status">
            <EditItemTemplate>
                <asp:TextBox ID="TextBox3" runat="server" ReadOnly="True" 
                    Text='<%# Bind("status") %>'></asp:TextBox>
            </EditItemTemplate>
            <InsertItemTemplate>
                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("status") %>'></asp:TextBox>
            </InsertItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label3" runat="server" Text='<%# Bind("status") %>'></asp:Label>
            </ItemTemplate>
            <HeaderStyle CssClass="hiddencol" />
            <ItemStyle CssClass="hiddencol" />
        </asp:TemplateField>
        <asp:CommandField ShowEditButton="True" ButtonType="Image" 
            CancelImageUrl="~/images/cancel.gif" UpdateImageUrl="~/images/update.gif" />
    </Fields>
</asp:DetailsView>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
    ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
    DeleteCommand="DELETE FROM [G_Profiles] WHERE [uid] = @uid" 
    InsertCommand="INSERT INTO [G_Profiles] ([email], [password], [type], [status]) VALUES (@email, @password, @type, @status)" 
    SelectCommand="SELECT [uid], [email], [password], [type], [status] FROM [G_Profiles] WHERE ([uid] = @uid)" 
    
        UpdateCommand="UPDATE [G_Profiles] SET [email] = @email, [password] = @password, [type] = @type, [status] = @status WHERE [uid] = @uid">
    <DeleteParameters>
        <asp:Parameter Name="uid" Type="Int32" />
    </DeleteParameters>
    <InsertParameters>
        <asp:Parameter Name="email" Type="String" />
        <asp:Parameter Name="password" Type="String" />
        <asp:Parameter Name="type" Type="String" />
        <asp:Parameter Name="status" Type="Int32" />
    </InsertParameters>
    <SelectParameters>
        <asp:SessionParameter Name="uid" SessionField="UID" Type="Int32" />
    </SelectParameters>
    <UpdateParameters>
        <asp:Parameter Name="email" Type="String" />
        <asp:Parameter Name="password" Type="String" />
        <asp:Parameter Name="type" Type="String" />
        <asp:Parameter Name="status" Type="Int32" />
        <asp:Parameter Name="uid" Type="Int32" />
    </UpdateParameters>
</asp:SqlDataSource>

    <br /> <br />
    <h2><asp:HyperLink ID="HyperLink1" runat="server">Update Membership »</asp:HyperLink></h2>


    <br />
</asp:Content>

