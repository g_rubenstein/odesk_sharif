﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class PProfileProfessionalBackgrounds : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string SPID = "";
        string SFirstName = "";

        if (Session["UID"] != null)
        {
            SPID = Session["UID"].ToString();
        }

        DataTable iDataTable = new DataTable();

        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();

        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

        iSqlConnection.Open();

        SqlDataAdapter iSqlDataAdapter = new SqlDataAdapter("SELECT FirstName FROM P_Profile WHERE  (PID = '" + SPID + "')", iSqlConnection);

        iSqlDataAdapter.Fill(iDataTable);

        if (iDataTable.Rows.Count > 0)
        {
            SFirstName = iDataTable.Rows[0][0].ToString();
        }

        iSqlConnection.Close();

        Label1.Text = "Hello " + SFirstName + ", please add some Professional Backgrounds to your profile.";
    }

    public bool AddUserToDB(string UID, string Field, string Profession, string Skills)
    {
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;
        SqlConnection con = null;
        SqlDataAdapter da = null;
        SqlDataReader reader = null;
        SqlCommand cmd = null;

        String sQuery = "INSERT INTO P_ProfessionalBackgroundsSelections (UID, Field, Profession, Skills) VALUES ('" + UID + "','" + Field + "','" + Profession + "','" + Skills + "')";

        try
        {
            con = new SqlConnection(conString);
            cmd = new SqlCommand(sQuery, con);
            con.Open();
            cmd.ExecuteNonQuery();

        }

        catch
        {

        }

        finally
        {
            con.Close();
        }

        return true;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        DataTable iDataTable = new DataTable();
        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

        iSqlConnection.Open();

        string UID = "";
        if (Session["UID"] != null)
        {
            UID = Session["UID"].ToString();
        }

        //string Field = dpdFields.SelectedValue;

        //string Field = CheckBoxList1.SelectedValue;

        //string Profession = CheckBoxList2.SelectedValue;
        //string Skills = CheckBoxList3.SelectedValue;
        foreach (ListItem s in CheckBoxList1.Items)
        {
            if (s.Selected)
            {

                AddUserToDB(UID, s.Value, "", "");

            }
        }
        foreach (ListItem s in CheckBoxList2.Items)
        {
            if (s.Selected)
            {

                AddUserToDB(UID, "", s.Value, "");

            }
        }
        foreach (ListItem s in CheckBoxList3.Items)
        {
            if (s.Selected)
            {

                AddUserToDB(UID, "", "", s.Value);

            }
        }
        //AddUserToDB(UID, Field, Profession, Skills);


        Response.Redirect("PSignupProfessionalBackgrounds.aspx");

    }
}