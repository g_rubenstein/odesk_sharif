﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class InMailRead : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string InMailID = Request.QueryString["InMailID"];

        string connectionStr = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(connectionStr);
        connection.Open();

        string commandStr = "UPDATE G_InMail SET Status=0 WHERE (InMailID='" + InMailID + "')";
        System.Data.SqlClient.SqlCommand myCommand = new System.Data.SqlClient.SqlCommand(commandStr, connection);
        System.Data.SqlClient.SqlDataReader myReader = myCommand.ExecuteReader();
        myReader.Close();

        connection.Close();
    }
    private string GetUserData(string uid)
    {
        string userinfo = string.Empty;
        string userType = GetUserType(uid);
        DataTable dtCustomerInfo = new DataTable();
        SqlDataAdapter daCustomerInfo = new SqlDataAdapter();
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;

        SqlConnection con = null;
        string commandStr = "";
        if (userType == "A")
        {
            commandStr = "SELECT Companyname  FROM A_Profile WHERE  AID = " + Convert.ToInt32(uid);
        }
        if (userType == "B")
        {
            commandStr = "SELECT  Companyname FROM B_Profile  WHERE BID = " + Convert.ToInt32(uid);
        }
        if (userType == "P")
        {
            commandStr = "SELECT Firstname, LastName FROM P_Profile   WHERE PID = " + Convert.ToInt32(uid);
        }
        if (userType == "S")
        {
            commandStr = "SELECT Firstname, LastName FROM S_Profile WHERE SID = " + Convert.ToInt32(uid);
        }

        try
        {
            if (userType != "")
            {
                con = new SqlConnection(conString);
                SqlCommand myCommand = new SqlCommand(commandStr, con);
                daCustomerInfo.SelectCommand = myCommand;
                daCustomerInfo.Fill(dtCustomerInfo);
                if ((userType == "S") || (userType == "P"))
                    userinfo = dtCustomerInfo.Rows[0]["FirstName"].ToString() + " " + dtCustomerInfo.Rows[0]["LastName"].ToString();
                else
                    userinfo = dtCustomerInfo.Rows[0]["Companyname"].ToString();
                con.Close();
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }
        finally
        {

        }
        return userinfo;
    }
    private string GetUserType(string uid)
    {
        string userType = "";
        DataTable dtCustomerInfo = new DataTable();
        SqlDataAdapter daCustomerInfo = new SqlDataAdapter();
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;

        SqlConnection con = null;
        string commandStr = "";

        commandStr = "SELECT type FROM G_profiles WHERE type is not null and  UID=" + Convert.ToInt32(uid);

        try
        {
            con = new SqlConnection(conString);
            SqlCommand myCommand = new SqlCommand(commandStr, con);
            daCustomerInfo.SelectCommand = myCommand;
            daCustomerInfo.Fill(dtCustomerInfo);
            if (dtCustomerInfo.Rows.Count > 0)
            {
                if (dtCustomerInfo.Rows[0]["Type"] != DBNull.Value)
                    userType = Convert.ToString(dtCustomerInfo.Rows[0]["Type"]);
                if (userType == "1")
                    userType = "P";
                if (userType == "2")
                    userType = "B";
                if (userType == "3")
                    userType = "S";
                if (userType == "4")
                    userType = "A";
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }
        finally
        {
            con.Close();
        }
        return userType;
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblFrom = (Label)e.Row.FindControl("Label4");
            //DataRow row = ((DataRowView)e.Row.DataItem).Row;
            lblFrom.Text = GetUserData(lblFrom.Text);
        }
    }
}