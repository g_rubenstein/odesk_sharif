﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

public partial class PSignupMembershipConfirmation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string ConfirmCode = Request.QueryString["ConfirmCode"];
        string connectionStr = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(connectionStr);
        connection.Open();
        
        string verify = "Select PID from P_Profile where ConfirmCode=" + ConfirmCode;
        System.Data.SqlClient.SqlCommand myCommand = new System.Data.SqlClient.SqlCommand(verify, connection);
        System.Data.SqlClient.SqlDataReader myReader = myCommand.ExecuteReader();

        myReader.Close();

        string UID = Convert.ToString(myCommand.ExecuteScalar());

        Label1.Text = UID;

        if (UID != "")
        {
            string commandStr = "UPDATE G_Profiles SET status = '1' WHERE UID=" + UID;

            System.Data.SqlClient.SqlCommand myCommand2 = new System.Data.SqlClient.SqlCommand(commandStr, connection);
            System.Data.SqlClient.SqlDataReader myReader2 = myCommand2.ExecuteReader();
            myReader2.Close();
            Confirmation.Text = "Thanks you for Confirming your membership!";
        }
        else
        {
            Confirmation.Text = "The confirmation code doesn't exist";
        }
        
        connection.Close();

        //////

        //DataTable iDataTable = new DataTable();
        //string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        //SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

        //iSqlConnection.Open();


        //SqlDataAdapter iSqlDataAdapter = new SqlDataAdapter("Select FirstName, Email from P_Profile where (ConfirmCode= '" + ConfirmCode + "')", iSqlConnection);
        //iSqlDataAdapter.Fill(iDataTable);

        //string FirstName = iDataTable.Rows[0][0].ToString();
        //string Email = iDataTable.Rows[0][1].ToString();
        //string Password = iDataTable.Rows[0][2].ToString();


        //MailMessage EmailMsg = new MailMessage();
        //EmailMsg.From = new MailAddress("The UBHub Team<support@theubhub.com>");
        //EmailMsg.To.Add(new MailAddress(Email));
        //EmailMsg.Subject = "Welcome to The Ultimate Business Hub " + FirstName + "!";
        //EmailMsg.Body = "Hi " + FirstName + ",\n\nThanks for joining The UBHub! \n\nHere's your account info for logging in:\nE-mail: " + Email + "\nPassword: " + Password + "\n\nPlease keep this information in a safe place.\n\nThanks again,\nTheUbHub Team";
        //SmtpClient MailClient = new SmtpClient("mail.theubhub.com", 587);
        //MailClient.Credentials = new System.Net.NetworkCredential("support@theubhub.com", "misty.77");
        //MailClient.Send(EmailMsg);


        DataTable iDataTable = new DataTable();
        DataTable iDataTable2 = new DataTable();
        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);
        iSqlConnection.Open();


        SqlDataAdapter iSqlDataAdapter = new SqlDataAdapter("Select FirstName from P_Profile where (PID= '" + UID + "')", iSqlConnection);
        iSqlDataAdapter.Fill(iDataTable);

        string FirstName = iDataTable.Rows[0][0].ToString();

        SqlDataAdapter iSqlDataAdapter2 = new SqlDataAdapter("Select Email from G_Profiles where (UID= '" + UID + "')", iSqlConnection);
        iSqlDataAdapter2.Fill(iDataTable2);

        string Email = iDataTable2.Rows[0][0].ToString();

        MailMessage EmailMsg = new MailMessage();
        EmailMsg.From = new MailAddress("The UBHub Team<support@theubhub.com>");
        EmailMsg.To.Add(new MailAddress(Email));
        EmailMsg.Subject = "Welcome to The Ultimate Business Hub " + FirstName + " - this is an auto-generated email of your membership confirmation.";
        EmailMsg.Body = "Hi " + FirstName + ",\n\nThanks for joining The UBHub! \n\nTheUbHub Team";
        SmtpClient MailClient = new SmtpClient("mail.theubhub.com", 587);
        MailClient.Credentials = new System.Net.NetworkCredential("support@theubhub.com", "misty.77");
        MailClient.Send(EmailMsg);

        iSqlConnection.Close();

    }
}