﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpNoCSS.master" AutoEventWireup="true" CodeFile="GProfile_Recommendation.aspx.cs" Inherits="GProfile_Recommendation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    

    <h1>Here is what others said....</h1>

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="RecommendationID" DataSourceID="SqlDataSource1" 
        EmptyDataText="There are no data records to display." BorderWidth="0px" 
        ShowHeader="False" onrowdatabound="GridView1_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="Recommendation" SortExpression="Recommendation">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Recommendation") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <br />
                    <div class="bigtext"><asp:Label ID="Label1" runat="server" Text='<%# Bind("Recommendation") %>'></asp:Label></div>
                    
                    <i>
                    - <asp:Label ID="Label2" runat="server" Text='<%# Bind("FromWho") %>'></asp:Label>
                    ,
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
                    </i>
                    <br />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <br />
    <br />
    <br />
    <h2><asp:Label ID="Label4" runat="server" Text="Leave a comment here!"></asp:Label></h2>

    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" 
        DataKeyNames="RecommendationID" DataSourceID="SqlDataSource1" 
        DefaultMode="Insert" Height="50px" Width="125px" BorderWidth="0px" 
        ondatabound="DetailsView1_DataBound">
        <Fields>
            <asp:BoundField DataField="RecommendationID" HeaderText="RecommendationID" 
                InsertVisible="False" ReadOnly="True" SortExpression="RecommendationID" />
            <asp:TemplateField HeaderText="UID" SortExpression="UID">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("UID") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("UID") %>' Value='<%# Request.QueryString["UID"] %>' ></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("UID") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="hiddencol" />
                <ItemStyle CssClass="hiddencol" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Recommendation" SortExpression="Recommendation">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Recommendation") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Recommendation") %>' 
                        Height="117px" TextMode="MultiLine" Width="677px"></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Recommendation") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="hiddencol" />
                <ItemStyle BorderWidth="0px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="FromWho" SortExpression="FromWho">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("FromWho") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("FromWho") %>'   Value='<%# (string)Session["UID"] %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("FromWho") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="hiddencol" />
                <ItemStyle CssClass="hiddencol" />
            </asp:TemplateField>
            <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" >
            <HeaderStyle CssClass="hiddencol" />
            <ItemStyle CssClass="hiddencol" />
            </asp:BoundField>
            <asp:BoundField DataField="Status" HeaderText="Status" 
                SortExpression="Status" >
            <HeaderStyle CssClass="hiddencol" />
            <ItemStyle CssClass="hiddencol" />
            </asp:BoundField>
            <asp:CommandField ShowInsertButton="True" InsertText="Submit" 
                ButtonType="Image" CancelImageUrl="~/images/cancel.gif" 
                InsertImageUrl="~/images/submit.gif" >
            <ItemStyle BorderWidth="0px" />
            </asp:CommandField>
        </Fields>
    </asp:DetailsView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
        DeleteCommand="DELETE FROM [G_Recommendation] WHERE [RecommendationID] = @RecommendationID" 
        InsertCommand="INSERT INTO [G_Recommendation] ([UID], [Recommendation], [FromWho], [Date], [Status]) VALUES (@UID, @Recommendation, @FromWho, GetDate(), 0)" 
        SelectCommand="SELECT [G_Recommendation].[RecommendationID], [G_Recommendation].[UID], [G_Recommendation].[Recommendation], [G_Recommendation].[FromWho], [G_Recommendation].[Date], [G_Recommendation].[Status] FROM [G_Recommendation] WHERE ([UID] = @UID) AND (Status =1)" 
        UpdateCommand="UPDATE [G_Recommendation] SET [UID] = @UID, [Recommendation] = @Recommendation, [FromWho] = @FromWho, [Date] = @Date, [Status] = @Status WHERE [RecommendationID] = @RecommendationID">
        <DeleteParameters>
            <asp:Parameter Name="RecommendationID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="UID" Type="Int32" />
            <asp:Parameter Name="Recommendation" Type="String" />
            <asp:Parameter Name="FromWho" Type="Int32" />
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Status" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="UID" QueryStringField="UID" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="UID" Type="Int32" />
            <asp:Parameter Name="Recommendation" Type="String" />
            <asp:Parameter Name="FromWho" Type="Int32" />
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Status" Type="Int32" />
            <asp:Parameter Name="RecommendationID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <br />
    <br />


</asp:Content>

