﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class mpp : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        PanelMemberViewP.Visible = false;

        string UID = "";
        string QID = "";

        UID = (string)Session["UID"];
        QID = Request.QueryString["UID"];

        if (UID == QID)
        {
            PanelMemberViewP.Visible = true;
        }
        else
            PanelMemberViewP.Visible = false;


        lblID.Text = UID;

// Type
        if (UID != null)
        {
            string connectionStr = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(connectionStr);
            connection.Open();
            string commandStr = "SELECT Type FROM G_Profiles WHERE UID =" + UID;
            System.Data.SqlClient.SqlCommand myCommand = new System.Data.SqlClient.SqlCommand(commandStr, connection);
            System.Data.SqlClient.SqlDataReader myReader = myCommand.ExecuteReader();
            myReader.Close();
            string type = Convert.ToString(myCommand.ExecuteScalar());
            //if (type == "")
            //{
            //    MemberHome.Visible = false;
            //}
            if (type == "1")
            {
                MemberHome.NavigateUrl = "pprofile.aspx?uid=" + UID;
            }
            if (type == "2")
            {
                MemberHome.NavigateUrl = "bprofile.aspx?uid=" + UID;
            }
            if (type == "3")
            {
                MemberHome.NavigateUrl = "sprofile.aspx?uid=" + UID;
            }
            connection.Close();
        }
        else
        {
            MemberNav.Visible = false;
            
        }

//

        ProfileUpdateLink.NavigateUrl = "PProfileUpdate.aspx?uid=" + QID;
        SearchLink.NavigateUrl = "search.aspx?uid=" + QID;
        InMailLink.NavigateUrl = "inmail.aspx?uid=" + QID;
        SettingsUpdateLink.NavigateUrl = "GUpdateSettings.aspx?uid=" + QID;
        RecommendationApproveLink.NavigateUrl = "GProfile_Recommendation_Approve.aspx?uid=" + QID;
        CalendarLink.NavigateUrl = "PCalendar.aspx?uid=" + QID;        
    }
}
