﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //string SPID = (string)Session["PID"];

        //if (SPID != null)
        //{
        //    Response.Redirect("PProfile.aspx");
        //}

    }

    protected void btnPLogin_Click(object sender, EventArgs e)
    {

        string Email = txtEmail.Text.Trim(); ;
        string Password = txtPassword.Text.Trim();
        string UID = "";

        DataTable iDataTable = new DataTable();
        DataTable vDataTable = new DataTable();
        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);
        iSqlConnection.Open();


        SqlDataAdapter iSqlDataAdapter = new SqlDataAdapter("Select Status from G_Profiles where (Email= '" + Email + "')", iSqlConnection);
        iSqlDataAdapter.Fill(iDataTable);
        try
        {

            string verify = iDataTable.Rows[0][0].ToString();

            if (verify == "0")
            {
                Confirmation.Text = "Sorry, your account is not activated";
            }
            if (verify == "1")
            {
                SqlDataAdapter vSqlDataAdapter = new SqlDataAdapter("SELECT UID FROM G_Profiles WHERE  (Email = '" + Email + "') AND (type = '1') AND (Password = '" + Password + "')", iSqlConnection);
                vSqlDataAdapter.Fill(vDataTable);
                if (vDataTable.Rows.Count > 0)
                {
                    UID = vDataTable.Rows[0][0].ToString();
                    Session["UID"] = UID;
                    Response.Redirect("PProfile.aspx?UID=" + UID);
                }
                else
                {
                    Response.Redirect("error.aspx");
                }
            }
        }
        catch
        {
            Confirmation.Text = "Sorry, your login was not successful. Please try again.";
        }


        iSqlConnection.Close();


    }

    protected void btnBLogin_Click(object sender, EventArgs e)
    {

        string Email = txtBEmail.Text.Trim(); ;
        string Password = txtBPassword.Text.Trim();
        string UID = "";

        DataTable iDataTable = new DataTable();
        DataTable vDataTable = new DataTable();
        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);
        iSqlConnection.Open();


        SqlDataAdapter iSqlDataAdapter = new SqlDataAdapter("Select Status from G_Profiles where (Email= '" + Email + "')", iSqlConnection);
        iSqlDataAdapter.Fill(iDataTable);
        try
        {

            string verify = iDataTable.Rows[0][0].ToString();

            if (verify == "0")
            {
                lblBConfirmation.Text = "Sorry, your account is not activated";
            }
            if (verify == "1")
            {
                SqlDataAdapter vSqlDataAdapter = new SqlDataAdapter("SELECT UID FROM G_Profiles WHERE  (Email = '" + Email + "') AND (type = '2') AND (Password = '" + Password + "')", iSqlConnection);
                vSqlDataAdapter.Fill(vDataTable);
                if (vDataTable.Rows.Count > 0)
                {
                    UID = vDataTable.Rows[0][0].ToString();
                    Session["UID"] = UID;
                    Response.Redirect("BProfile.aspx?UID=" + UID);
                }
                else
                {
                    Response.Redirect("error.aspx");
                }
            }
        }
        catch
        {
            lblBConfirmation.Text = "Sorry, your login was not successful. Please try again.";
        }


        iSqlConnection.Close();


    }

    protected void btnSLogin_Click(object sender, EventArgs e)
    {
        string Email = txtSEmail.Text.Trim(); ;
        string Password = txtSPassword.Text.Trim();
        string UID = "";

        DataTable iDataTable = new DataTable();
        DataTable vDataTable = new DataTable();
        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);
        iSqlConnection.Open();


        SqlDataAdapter iSqlDataAdapter = new SqlDataAdapter("Select Status from G_Profiles where (Email= '" + Email + "')", iSqlConnection);
        iSqlDataAdapter.Fill(iDataTable);
        try
        {

            string verify = iDataTable.Rows[0][0].ToString();

            if (verify == "0")
            {
                lblSConfirmation.Text = "Sorry, your account is not activated";
            }
            if (verify == "1")
            {
                SqlDataAdapter vSqlDataAdapter = new SqlDataAdapter("SELECT UID FROM G_Profiles WHERE  (Email = '" + Email + "') AND (type = '3') AND (Password = '" + Password + "')", iSqlConnection);
                vSqlDataAdapter.Fill(vDataTable);
                if (vDataTable.Rows.Count > 0)
                {
                    UID = vDataTable.Rows[0][0].ToString();
                    Session["UID"] = UID;
                    Response.Redirect("SProfile.aspx?UID=" + UID);
                }
                else
                {
                    Response.Redirect("error.aspx");
                }
            }
        }
        catch
        {
            lblBConfirmation.Text = "Sorry, your login was not successful. Please try again.";
        }


        iSqlConnection.Close();

    }

    protected void btnALogin_Click(object sender, EventArgs e)
    {
        string Email = txtAEmail.Text.Trim(); ;
        string Password = txtAPassword.Text.Trim();
        string UID = "";

        DataTable iDataTable = new DataTable();
        DataTable vDataTable = new DataTable();
        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);
        iSqlConnection.Open();


        SqlDataAdapter iSqlDataAdapter = new SqlDataAdapter("Select Status from G_Profiles where (Email= '" + Email + "')", iSqlConnection);
        iSqlDataAdapter.Fill(iDataTable);
        try
        {

            string verify = iDataTable.Rows[0][0].ToString();

            if (verify == "0")
            {
                lblAConfirmation.Text = "Sorry, your account is not activated";
            }
            if (verify == "1")
            {
                SqlDataAdapter vSqlDataAdapter = new SqlDataAdapter("SELECT UID FROM G_Profiles WHERE  (Email = '" + Email + "') AND (type = '4') AND (Password = '" + Password + "')", iSqlConnection);
                vSqlDataAdapter.Fill(vDataTable);
                if (vDataTable.Rows.Count > 0)
                {
                    UID = vDataTable.Rows[0][0].ToString();
                    Session["UID"] = UID;
                    Response.Redirect("AProfile.aspx?UID=" + UID);
                }
                else
                {
                    Response.Redirect("error.aspx");
                }
            }
        }
        catch
        {
            lblBConfirmation.Text = "Sorry, your login was not successful. Please try again.";
        }


        iSqlConnection.Close();

    }
}