﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class GUpdateSettings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        string UID = (string)Session["UID"];
        string QID = Request.QueryString["UID"];

        if (UID != null)
        {
            string connectionStr = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(connectionStr);
            connection.Open();
            string commandStr = "SELECT Type FROM G_Profiles WHERE UID =" + UID;
            System.Data.SqlClient.SqlCommand myCommand = new System.Data.SqlClient.SqlCommand(commandStr, connection);
            System.Data.SqlClient.SqlDataReader myReader = myCommand.ExecuteReader();
            myReader.Close();
            string type = Convert.ToString(myCommand.ExecuteScalar());

            if (type == "1")
            {
                HyperLink1.NavigateUrl = "PUpdateMembership.aspx?uid=" + UID;
            }
            if (type == "2")
            {
                HyperLink1.NavigateUrl = "BUpdateMembership.aspx?uid=" + UID;
            }
            if (type == "3")
            {
                HyperLink1.NavigateUrl = "SUpdateMembership.aspx?uid=" + UID;
            }
            if (type == "4")
            {
                HyperLink1.NavigateUrl = "AUpdateMembership.aspx?uid=" + UID;
            }
            connection.Close();
        }


    }
}