﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpOut.master" AutoEventWireup="true" CodeFile="PSignupAccomplishmentsAndSkills.aspx.cs" Inherits="PSignupAccomplishmentsAndSkills" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<center><h1>Accomplishment and Skills</h1></center><br /><br />








<table border="0" cellpadding="50" cellspacing="0" width="80%">
  <tr>
    <td width="50%" valign="top" align="left">

    <asp:Label ID="Label1" runat="server"></asp:Label>
        <br />
        <br />
Accomplishments<br />

    <asp:TextBox ID="txtAccomplishments" runat="server"></asp:TextBox><br />
    <asp:Button ID="btnAccomplishments" runat="server" Text="Add" 
        onclick="btnAccomplishments_Click" />
    <br /><br />


Skills&nbsp;&nbsp;&nbsp; <br />
    <asp:TextBox ID="txtSkills" runat="server"></asp:TextBox><br />
    <asp:Button ID="btnSkills" runat="server" Text="Add" 
        onclick="btnSkills_Click" />
    <br />
    </td>
    <td width="50%" valign="top" align="left">
    

    Your added Accomplishments: <br /><br />

        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="AccomplishmentsID" DataSourceID="SqlDataSource1" 
            EmptyDataText="There are no data records to display." 
            EnableModelValidation="True">
            <Columns>
                <asp:BoundField DataField="AccomplishmentsID" HeaderText="AccomplishmentsID" 
                    ReadOnly="True" SortExpression="AccomplishmentsID" />
                <asp:BoundField DataField="UID" HeaderText="UID" SortExpression="UID" />
                <asp:BoundField DataField="Accomplishments" HeaderText="Accomplishments" 
                    SortExpression="Accomplishments" />
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
            DeleteCommand="DELETE FROM [P_Accomplishments] WHERE [AccomplishmentsID] = @AccomplishmentsID" 
            InsertCommand="INSERT INTO [P_Accomplishments] ([UID], [Accomplishments]) VALUES (@UID, @Accomplishments)" 
            SelectCommand="SELECT [AccomplishmentsID], [UID], [Accomplishments] FROM [P_Accomplishments] WHERE ([UID] = @UID)" 
            UpdateCommand="UPDATE [P_Accomplishments] SET [UID] = @UID, [Accomplishments] = @Accomplishments WHERE [AccomplishmentsID] = @AccomplishmentsID">
            <DeleteParameters>
                <asp:Parameter Name="AccomplishmentsID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="UID" Type="Int32" />
                <asp:Parameter Name="Accomplishments" Type="String" />
            </InsertParameters>
            <SelectParameters>
                <asp:SessionParameter Name="UID" SessionField="UID" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="UID" Type="Int32" />
                <asp:Parameter Name="Accomplishments" Type="String" />
                <asp:Parameter Name="AccomplishmentsID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <br />
        <br />
        <br />
        Your added Skills:<br />
        <br />
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="SkillsID" DataSourceID="SqlDataSource2" 
            EmptyDataText="There are no data records to display." 
            EnableModelValidation="True">
            <Columns>
                <asp:BoundField DataField="SkillsID" HeaderText="SkillsID" ReadOnly="True" 
                    SortExpression="SkillsID" />
                <asp:BoundField DataField="UID" HeaderText="UID" SortExpression="UID" />
                <asp:BoundField DataField="Skills" HeaderText="Skills" 
                    SortExpression="Skills" />
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
            DeleteCommand="DELETE FROM [P_Skills] WHERE [SkillsID] = @SkillsID" 
            InsertCommand="INSERT INTO [P_Skills] ([UID], [Skills]) VALUES (@UID, @Skills)" 
            SelectCommand="SELECT [SkillsID], [UID], [Skills] FROM [P_Skills] WHERE ([UID] = @UID)" 
            UpdateCommand="UPDATE [P_Skills] SET [UID] = @UID, [Skills] = @Skills WHERE [SkillsID] = @SkillsID">
            <DeleteParameters>
                <asp:Parameter Name="SkillsID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="UID" Type="Int32" />
                <asp:Parameter Name="Skills" Type="String" />
            </InsertParameters>
            <SelectParameters>
                <asp:SessionParameter Name="UID" SessionField="UID" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="UID" Type="Int32" />
                <asp:Parameter Name="Skills" Type="String" />
                <asp:Parameter Name="SkillsID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
    
    
    </td>
  </tr>
  <tr>
    <td width="50%" valign="top" align="left">

   <h3> <a href="PSignupExemplars.aspx">Go Next Page »</a></h3></td>
    <td width="50%" valign="top" align="left">
    

        &nbsp;</td>
  </tr>
</table>




























    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
<br />
</asp:Content>

