﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpOut.master" AutoEventWireup="true"
    CodeFile="PSignupExemplars.aspx.cs" Inherits="PSignupExemplars" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
        <h1>
            Portfolio</h1>
    </center>
    <br />
    <br />
    <table border="0" cellpadding="50" cellspacing="0" width="80%">
        <tr>
            <td width="50%" valign="top" align="left">
                <asp:Label ID="Label1" runat="server"></asp:Label>
                <br />
                <br />
                Upload Document:<br />
                <asp:FileUpload ID="ucDoc" runat="server" />
                <asp:RegularExpressionValidator ID="revReume" runat="server" ControlToValidate="ucDoc"
                    ErrorMessage=".doc, .docx,.txt & pdf formats are allowed" ValidationExpression="(.*?)\.(|doc|pdf|docx|jpeg|gif|png|jpg)$"></asp:RegularExpressionValidator>
                <asp:Label runat="server" ID="lblResumeU" />
                <asp:CustomValidator ID="DocValidator" runat="server" Text="" ToolTip="" ErrorMessage="FileSize Exceeds the Limits.Please Try uploading smaller size files."
                    ControlToValidate="ucDoc" ononclient="checkfilesize"></asp:CustomValidator>
                <asp:Button ID="btnFileName" runat="server" Text="Submit" OnClick="btnFileName_Click" />
                <br />
                <br />
                <br />
                Upload video:<br />
                <asp:FileUpload ID="UCVideo" runat="server" />
                <asp:RegularExpressionValidator ID="revVideo" runat="server" ControlToValidate="UCVideo"
                    ErrorMessage=".zip,rar,flv,avi,mpg,wmv,mp4 & mov formats are allowed" ValidationExpression="(.*?)\.(|zip|rar|flv|avi|mpg|wmv|mp4|mov)$"></asp:RegularExpressionValidator>
                <asp:Label runat="server" ID="Label2" />
                <asp:CustomValidator ID="VideoValidator" runat="server" Text="" ToolTip="" ErrorMessage="FileSize Exceeds the Limits.Please Try uploading smaller size files."
                    ControlToValidate="UCVideo" ononclient="checkfilesize1"></asp:CustomValidator>
                <asp:Button ID="btnVideoURL" runat="server" Text="Submit" OnClick="btnVideoURL_Click" />
                <br />
                <br />
            </td>
            <td width="50%" valign="top" align="left">
                Your uploaded document:
                <br />
                <br />
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"
                    DataKeyNames="WorkExFilesID" DataSourceID="SqlDataSource1" EmptyDataText="There are no data records to display.">
                    <Columns>
                        <asp:BoundField DataField="FileName" HeaderText="File Name" SortExpression="FileName" />
                        <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>"
                    DeleteCommand="DELETE FROM [P_WorkExFiles] WHERE [WorkExFilesID] = @WorkExFilesID"
                    InsertCommand="INSERT INTO [P_WorkExFiles] ([UID], [FileName]) VALUES (@UID, @FileName)"
                    SelectCommand="SELECT [WorkExFilesID], [UID], [FileName] FROM [P_WorkExFiles] WHERE ([UID] = @UID)"
                    UpdateCommand="UPDATE [P_WorkExFiles] SET [UID] = @UID, [FileName] = @FileName WHERE [WorkExFilesID] = @WorkExFilesID">
                    <DeleteParameters>
                        <asp:Parameter Name="WorkExFilesID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="UID" Type="Int32" />
                        <asp:Parameter Name="FileName" Type="String" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:SessionParameter Name="UID" SessionField="UID" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="UID" Type="Int32" />
                        <asp:Parameter Name="FileName" Type="String" />
                        <asp:Parameter Name="WorkExFilesID" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <br />
                <br />
                <br />
                Your uploaded video file:
                <br />
                <br />
                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="3"
                    DataKeyNames="WorkExVideosID" DataSourceID="SqlDataSource2" EmptyDataText="There are no data records to display.">
                    <Columns>
                        <asp:BoundField DataField="VideoURL" HeaderText="Video URL" SortExpression="VideoURL" />
                        <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>"
                    DeleteCommand="DELETE FROM [P_WorkExVideos] WHERE [WorkExVideosID] = @WorkExVideosID"
                    InsertCommand="INSERT INTO [P_WorkExVideos] ([UID], [VideoURL]) VALUES (@UID, @VideoURL)"
                    SelectCommand="SELECT [WorkExVideosID], [UID], [VideoURL] FROM [P_WorkExVideos] WHERE ([UID] = @UID)"
                    UpdateCommand="UPDATE [P_WorkExVideos] SET [UID] = @UID, [VideoURL] = @VideoURL WHERE [WorkExVideosID] = @WorkExVideosID">
                    <DeleteParameters>
                        <asp:Parameter Name="WorkExVideosID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="UID" Type="Int32" />
                        <asp:Parameter Name="VideoURL" Type="String" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:SessionParameter Name="UID" SessionField="UID" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="UID" Type="Int32" />
                        <asp:Parameter Name="VideoURL" Type="String" />
                        <asp:Parameter Name="WorkExVideosID" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <br />
            </td>
        </tr>
        <tr>
            <td width="50%" valign="top" align="left">
                <h3>
                    <a href="PSignupProfessionalBackgrounds.aspx">Go Next Page »</a></h3>
            </td>
            <td width="50%" valign="top" align="left">
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
