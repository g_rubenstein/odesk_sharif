﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class PProfile_AccomplishmentsSkills : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


        string SPID = "";
        string SFirstName = "";
        string SMiddleInitial = "";
        string SLastName = "";
        string SPicture = "";
        string SCoverLetter = "";
        string SResume = "";
        string SObjective = "";
        string SVideoURL = "";

        SPID = Request.QueryString["UID"];

        DataTable iDataTable = new DataTable();

        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();

        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

        iSqlConnection.Open();

        SqlDataAdapter iSqlDataAdapter = new SqlDataAdapter("SELECT FirstName, MiddleInitial, LastName, Picture, CoverLetter, Resume, Objective, VideoURL FROM P_Profile WHERE  (PID = '" + SPID + "')", iSqlConnection);

        iSqlDataAdapter.Fill(iDataTable);

        if (iDataTable.Rows.Count > 0)
        {
            SFirstName = iDataTable.Rows[0][0].ToString();
            SMiddleInitial = iDataTable.Rows[0][1].ToString();
            SLastName = iDataTable.Rows[0][2].ToString();
            SPicture = iDataTable.Rows[0][3].ToString();
            SCoverLetter = iDataTable.Rows[0][4].ToString();
            SResume = iDataTable.Rows[0][5].ToString();
            SObjective = iDataTable.Rows[0][6].ToString();
            SVideoURL = iDataTable.Rows[0][7].ToString();


        }

        iSqlConnection.Close();



    }

}