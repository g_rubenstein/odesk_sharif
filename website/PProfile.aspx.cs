﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

public partial class PProfile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblInterested.Text = "";

        string QID = Request.QueryString["UID"];

        if (Session["UID"] != null) { Session["UID"].ToString(); }

        if (Session["UID"] == null)
        {
            GridView1.Visible = false;
            lblEvents.Visible = false;
        }

       


        string SPID = "";
        string SFirstName = "";
        string SMiddleInitial = "";
        string SLastName = "";
        string SPicture = "";
        string SCoverLetter = "";
        string SResume = "";
        string SObjective = "";
        string SVideoURL = "";



        DataTable iDataTable = new DataTable();

        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();

        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

        iSqlConnection.Open();

        SqlDataAdapter iSqlDataAdapter = new SqlDataAdapter("SELECT PID, FirstName, MiddleInitial, LastName, Picture, CoverLetter, Resume, Objective, VideoURL FROM P_Profile WHERE  (PID = '" + QID + "')", iSqlConnection);

        iSqlDataAdapter.Fill(iDataTable);

        if (iDataTable.Rows.Count > 0)
        {
            SPID = iDataTable.Rows[0][0].ToString();
            SFirstName = iDataTable.Rows[0][1].ToString();
            SMiddleInitial = iDataTable.Rows[0][2].ToString();
            SLastName = iDataTable.Rows[0][3].ToString();
            SPicture = iDataTable.Rows[0][4].ToString();
            SCoverLetter = iDataTable.Rows[0][5].ToString();
            SResume = iDataTable.Rows[0][6].ToString();
            SObjective = iDataTable.Rows[0][7].ToString();
            SVideoURL = iDataTable.Rows[0][8].ToString();
            SVideoURL = iDataTable.Rows[0][8].ToString();
        }

        iSqlConnection.Close();

        Name.Text = SFirstName + " " + SLastName + "'s Profile";

        Objective.Text = SObjective;


        InterviewLink.NavigateUrl = "PCalendarInvite.aspx?uid=" + QID;
        AddToNetworkLink.NavigateUrl = "PConnect.aspx?cuid=" + QID;
        PhotoLink.NavigateUrl = "PProfile_Photo.aspx?uid=" + QID;
        CoverletterLink.NavigateUrl = "PProfile_CoverLetter.aspx?uid=" + QID;
        ResumeLink.NavigateUrl = "PProfile_Resume.aspx?uid=" + QID;
        VideoLink.NavigateUrl = "pprofile_video.aspx?uid=" + QID;
        AccomplishmentsSkillsLinks.NavigateUrl = "PProfile_AccomplishmentsSkills.aspx?uid=" + QID;
        RecommendationLinks.NavigateUrl = "GProfile_Recommendation.aspx?uid=" + QID;
        ExamplarsLink.NavigateUrl = "PProfile_Exemplars.aspx?uid=" + QID;
        NetworkLink.NavigateUrl = "PProfile_Network.aspx?uid=" + QID;
        ForwardLink.NavigateUrl = "PProfile_Forward.aspx?uid=" + QID;
        ReferencesLink.NavigateUrl = "PProfile_References.aspx?uid=" + QID;



        if (SPicture != "")
            Image1.ImageUrl = "files/P/picture/" + SPicture;

        else
            Image1.ImageUrl = "images/no_picture.gif";



    }
    protected void btnInterestedLink_Click(object sender, EventArgs e)
    {

        string UID = (string)Session["UID"];
        string QID = Request.QueryString["UID"];


        string Date = DateTime.Now.ToString();
        string Subject = "I am interested knowing about your skills and experience - please contact.";
        string Msg = "Hello, I have a job opening, and I am very interested knowing more about your experience and skills for this job. Please reply this email to contact me. Thanks. ";
        string Status = "1";

        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;
        SqlConnection con = null;
        SqlCommand cmd = null;


        String sQuery = "INSERT INTO G_InMail (Date, Subject, Msg, Status, mailto, mailfrom) VALUES ('" + Date + "','" + Subject + "','" + Msg + "','" + Status + "','" + QID + "','" + UID + "')";




        try
        {
            con = new SqlConnection(conString);
            cmd = new SqlCommand(sQuery, con);
            con.Open();
            cmd.ExecuteNonQuery();

        }

        catch
        {

        }

        finally
        {
            con.Close();
            lblInterested.Text = "Thank you, your inquiry of interest has been sent!";
        }






    }
    //protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        DataRow row = ((DataRowView)e.Row.DataItem).Row;
    //        e.Row.Cells[1].Text = GetUserData(e.Row.Cells[1].Text);
          
    //    }
    //}



    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblFrom = (Label)e.Row.FindControl("Label1");
            //DataRow row = ((DataRowView)e.Row.DataItem).Row;
            lblFrom.Text = GetUserData(lblFrom.Text);
        }
    }

    //private string GetUserData(string uid)
    //{
    //    string userinfo = string.Empty;

    //    DataTable dtCustomerInfo = new DataTable();
    //    SqlDataAdapter daCustomerInfo = new SqlDataAdapter();
    //    String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;

    //    SqlConnection con = null;
    //    string commandStr = "";

    //    commandStr = "SELECT Firstname, LastName FROM P_Profile   WHERE PID = " + Convert.ToInt32(uid);


    //    try
    //    {


    //        con = new SqlConnection(conString);
    //        SqlCommand myCommand = new SqlCommand(commandStr, con);
    //        daCustomerInfo.SelectCommand = myCommand;
    //        daCustomerInfo.Fill(dtCustomerInfo);
    //        if(dtCustomerInfo.Rows.Count>0)
    //        userinfo = dtCustomerInfo.Rows[0]["FirstName"].ToString() + " " + dtCustomerInfo.Rows[0]["LastName"].ToString();

    //        con.Close();

    //    }
    //    catch (Exception ex)
    //    {
    //        throw (ex);
    //    }
    //    finally
    //    {

    //    }
    //    return userinfo;
    //}




    private string GetUserData(string uid)
    {
        string userinfo = string.Empty;
        string userType = GetUserType(uid);
        DataTable dtCustomerInfo = new DataTable();
        SqlDataAdapter daCustomerInfo = new SqlDataAdapter();
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;

        SqlConnection con = null;
        string commandStr = "";
        if (userType == "A")
        {
            commandStr = "SELECT Companyname  FROM A_Profile WHERE  AID = " + Convert.ToInt32(uid);
        }
        if (userType == "B")
        {
            commandStr = "SELECT  Companyname FROM B_Profile  WHERE BID = " + Convert.ToInt32(uid);
        }
        if (userType == "P")
        {
            commandStr = "SELECT Firstname, LastName FROM P_Profile   WHERE PID = " + Convert.ToInt32(uid);
        }
        if (userType == "S")
        {
            commandStr = "SELECT Firstname, LastName FROM S_Profile WHERE SID = " + Convert.ToInt32(uid);
        }

        try
        {
            if (userType != "")
            {
                con = new SqlConnection(conString);
                SqlCommand myCommand = new SqlCommand(commandStr, con);
                daCustomerInfo.SelectCommand = myCommand;
                daCustomerInfo.Fill(dtCustomerInfo);
                if ((userType == "S") || (userType == "P"))
                    userinfo = dtCustomerInfo.Rows[0]["FirstName"].ToString() + " " + dtCustomerInfo.Rows[0]["LastName"].ToString();
                else
                    userinfo = dtCustomerInfo.Rows[0]["Companyname"].ToString();
                con.Close();
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }
        finally
        {

        }
        return userinfo;
    }

    private string GetUserType(string uid)
    {
        string userType = "";
        DataTable dtCustomerInfo = new DataTable();
        SqlDataAdapter daCustomerInfo = new SqlDataAdapter();
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;

        SqlConnection con = null;
        string commandStr = "";

        commandStr = "SELECT type FROM G_profiles WHERE type is not null and  UID=" + Convert.ToInt32(uid);

        try
        {
            con = new SqlConnection(conString);
            SqlCommand myCommand = new SqlCommand(commandStr, con);
            daCustomerInfo.SelectCommand = myCommand;
            daCustomerInfo.Fill(dtCustomerInfo);
            if (dtCustomerInfo.Rows.Count > 0)
            {
                if (dtCustomerInfo.Rows[0]["Type"] != DBNull.Value)
                    userType = Convert.ToString(dtCustomerInfo.Rows[0]["Type"]);
                if (userType == "1")
                    userType = "P";
                if (userType == "2")
                    userType = "B";
                if (userType == "3")
                    userType = "S";
                if (userType == "4")
                    userType = "A";
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }
        finally
        {
            con.Close();
        }
        return userType;
    }


    //public String FrmText()
    //{
    //    return "http://ysharp.com";
    //}






}