﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public partial class PSearch : System.Web.UI.Page
{
    private string nearrestZip = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        

        btnSearch_Click(btnSearch, new EventArgs());


        if (!IsPostBack)
        {
            bindField();
            bindSkills();
            bindProfession();
        }
    }

    private void bindField()
    {

        DataTable dt = new DataTable();
        string connectionStr = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();

        System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connectionStr);

        con.Open();
        string strQuery = "SELECT DISTINCT Field from P_ProfessionalBackgroundsSelections";
        SqlCommand cmd = new SqlCommand(strQuery, con);
        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            da.Fill(dt);
        ddlfield.DataSource = dt;
        ddlfield.DataBind();
        ddlfield.Items.Insert(0, "--Select--");
        con.Close();
    }
    private void bindProfession()
    {

        DataTable dt = new DataTable();
        string connectionStr = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();

        System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connectionStr);

        con.Open();
        string strQuery = "SELECT DISTINCT Profession from P_ProfessionalBackgroundsSelections";
        SqlCommand cmd = new SqlCommand(strQuery, con);
        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            da.Fill(dt);
        ddlPrfoffesion.DataSource = dt;
        ddlPrfoffesion.DataBind();
        ddlPrfoffesion.Items.Insert(0, "--Select--");
        con.Close();
    }
    private void bindSkills()
    {

        DataTable dt = new DataTable();
        string connectionStr = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();

        System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connectionStr);

        con.Open();
        string strQuery = "SELECT DISTINCT Skills from P_ProfessionalBackgroundsSelections";
        SqlCommand cmd = new SqlCommand(strQuery, con);
        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            da.Fill(dt);
        ddlSkills.DataSource = dt;
        ddlSkills.DataBind();
        ddlSkills.Items.Insert(0, "--Select--");
        con.Close();
    }
    public String FrmImg(String str)
    {
        if (str != "")
            return ("files/A/picture/" + str);
        else
            return "images/no_picture_small.gif";


    }
    public string GetInitialCoordinates(string zip)
    {
        //This subroutine requires a Label control named lblStatus

        //Prepare to connect to db and execute stored procedure
        string connectionStr = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        SqlConnection objConn = new SqlConnection(connectionStr);
        SqlCommand objCmd = new SqlCommand("sp_get_zip_code", objConn);
        objCmd.CommandType = CommandType.StoredProcedure;

        //we need to supply the ZIP code as an input parameter to our stored procedure
        objCmd.Parameters.Add(new SqlParameter("zipcode", SqlDbType.Char, 5));
        objCmd.Parameters["zipcode"].Value = zip;

        //sglMinLat = south, sglMaxLat = north, sglMinLon = west, sglMaxLon = east
        double sglMinLat = 0;
        double sglMaxLat = 0;
        double sglMinLon = 0;
        double sglMaxLon = 0;

        try
        {
            //open connection
            objConn.Open();
            //put results into datareader
            SqlDataReader objReader = default(SqlDataReader);
            objReader = objCmd.ExecuteReader();
            if (objReader.HasRows)
            {
                //if starting point found, calculate box points
                objReader.Read();
                double radious = Convert.ToDouble(ddlRadius.SelectedValue);
                double radiousE = 3959;
                double lat = Convert.ToDouble(objReader["latitude"]);
                double lon = Convert.ToDouble(objReader["longitude"]);
                sglMaxLat = lat + rad2deg(radious / radiousE);
                sglMinLat = lat - rad2deg(radious / radiousE);

                sglMaxLon = lon + rad2deg(radious / radiousE / Math.Cos(deg2rad(lat)));
                sglMinLon = lon - rad2deg(radious / radiousE / Math.Cos(deg2rad(lat)));

                lat = deg2rad(lat);
                lon = deg2rad(lon);
                //populate gridview
                nearrestZip = PopulateGridView(sglMinLat, sglMaxLat, sglMinLon, sglMaxLon, lat, lon);
            }
            else
            {
                //starting point not found
                // lblStatus.Text = "Error retrieving initial ZIP Code coordinates: No record found for " + tbZip.Text + ".";
            }
            objConn.Close();
            objCmd.Dispose();
            objConn.Dispose();
        }
        catch (Exception ex)
        {
            //technical problem running the query
            // lblStatus.Text = "Error executing database query for initial coordinates: " + ex.Message;
        }
        return nearrestZip;
    }
    private double deg2rad(double deg)
    {
        return (deg * Math.PI / 180.0);
    }
    private double rad2deg(double rad)
    {
        return (rad / Math.PI * 180.0);
    }
    public double CalculateLongitudeCoordinate(double sglLon1, double sglLat1, double sglLat2, int intRadius, int intBearing, int intDistance)
    {
        return sglLon1 + Math.Atan2(Math.Sin(intBearing) * Math.Sin(intDistance / intRadius) * Math.Cos(sglLat1), Math.Cos(intDistance / intRadius) - Math.Sin(sglLat1) * Math.Sin(sglLat2));
    }

    public double CalculateLatitudeCoordinate(double sglLat1, int intRadius, int intBearing, int intDistance)
    {
        return Math.Asin(Math.Sin(sglLat1) * Math.Cos(intDistance / intRadius) + Math.Cos(sglLat1) * Math.Sin(intDistance / intRadius) * Math.Cos(intBearing));
    }
    public string PopulateGridView(double sglMinLat, double sglMaxLat, double sglMinLon, double sglMaxLon, double sglStartLat, double sglStartLon)
    {
        string nearestZip = string.Empty; DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;

        SqlConnection con = null;
        try
        {
            con = new SqlConnection(conString);
            cmd = new SqlCommand("sp_get_zips_in_radius", con);
            cmd.Parameters.Add(new SqlParameter("@minlat", sglMinLat));
            cmd.Parameters.Add(new SqlParameter("@maxlat", sglMaxLat));
            cmd.Parameters.Add(new SqlParameter("@minlon", sglMinLon));
            cmd.Parameters.Add(new SqlParameter("@maxlon", sglMaxLon));
            cmd.Parameters.Add(new SqlParameter("@startlat", sglStartLat));
            cmd.Parameters.Add(new SqlParameter("@startlon", sglStartLon));
            cmd.Parameters.Add(new SqlParameter("@radius", "3959"));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dt);


            for (int i = 0; i < dt.Rows.Count - 1; i++)
            {
                nearestZip = nearestZip + "," + dt.Rows[i]["zipcode"].ToString();
            }
            nearestZip = nearestZip.Remove(0, 1);
            //  BindSearchBasedUponZip(nearestZip);
        }
        catch (Exception x)
        {

        }
        finally
        {
            cmd.Dispose();
            con.Close();
        }
        return nearestZip;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }

    private void BindSearch()
    {

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter();

        string connectionStr = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        System.Data.SqlClient.SqlCommand myCommand = new SqlCommand();
        System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(connectionStr);
        try
        {
            connection.Open();
            string strSQL = "SELECT DISTINCT PID, FirstName, LastName,Picture FROM P_Profile inner join G_profiles on P_profile.PID = G_profiles.UID Inner join P_ProfessionalBackgroundsSelections on P_ProfessionalBackgroundsSelections.UID = P_profile.PID Where status=1";//
            if (txtName.Text != "")
                strSQL = strSQL + "and (FirstName LIKE " + "'%" + txtName.Text + "%'" + " OR LastName LIKE " + "'%" + txtName.Text + "%')";
            if (txtCity.Text != "")
                strSQL = strSQL + "and City LIKE " + "'%" + txtCity.Text + "%'";
            if (txtZip.Text != "")
            {
                string Zip = GetInitialCoordinates(txtZip.Text);
                strSQL = strSQL + "and  Zip in ( " + Zip + " )";
            }
            if (ddlState.SelectedValue != "--Select--")
                strSQL = strSQL + "and State='" + ddlState.SelectedValue + "' ";
            if (ddlPrfoffesion.SelectedValue != "--Select--")
                strSQL = strSQL + "and P_ProfessionalBackgroundsSelections.Profession='" + ddlPrfoffesion.SelectedValue + "' ";
            if (ddlSkills.SelectedValue != "--Select--")
                strSQL = strSQL + "and P_ProfessionalBackgroundsSelections.Skills='" + ddlSkills.SelectedValue + "' ";
            if (ddlfield.SelectedValue != "--Select--")
                strSQL = strSQL + "and P_ProfessionalBackgroundsSelections.Field='" + ddlfield.SelectedValue + "' ";
            myCommand = new System.Data.SqlClient.SqlCommand(strSQL, connection);
            da.SelectCommand = myCommand;
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GV.Visible = true;
                GV.DataSource = dt;
                GV.DataBind();
                lblMessage.Visible = false;
            }
            else
            {
                GV.Visible = false;
                lblMessage.Visible = true;
                // lblMessage.Text = "There is no any users within " + ddlDistance.SelectedItem.Value + " miles. of " + tbZip.Text;
            }
        }
        catch (Exception x)
        {

        }
        finally
        {
            myCommand.Dispose();
            connection.Close();
        }
        connection.Close();
    }
    protected void btnSearch_Click1(object sender, EventArgs e)
    {
        BindSearch();
    }
}