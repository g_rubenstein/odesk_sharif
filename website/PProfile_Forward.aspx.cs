﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class PProfile_Forward : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            btnForward.Visible = false;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {


        string connectionStr = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();

        System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(connectionStr);

        connection.Open();
        string commandStr = "";
        //string commandStr = "SELECT PID, FirstName, LastName,  Picture FROM P_Profile WHERE FirstName LIKE " + "'%" + srch + "%'" + " OR LastName LIKE " + "'%" + srch + "%'";
        // SQL For P Profile
        commandStr = "SELECT PID , FirstName, LastName, Picture FROM P_Profile inner join G_Profiles on P_Profile.PID= G_Profiles.uid WHERE G_Profiles.status=1";
        if (txtName.Text != "")
            commandStr = commandStr + "and FirstName LIKE " + "'%" + txtName.Text + "%'" + " OR LastName LIKE " + "'%" + txtName.Text + "%'";
        if (txtCity.Text != "")
            commandStr = commandStr + "and city LIKE " + "'%" + txtCity.Text + "%'";

        if (txtZip.Text != "")
        {
            string Zip = GetInitialCoordinates(txtZip.Text);
            commandStr = commandStr + "and  Zip in ( " + Zip + " )";

        }
        if (ddlState.SelectedValue != "--Select--")
            commandStr = commandStr + "and State='" + ddlState.SelectedValue + "'";
        // SQL For A profile
        commandStr = commandStr + "union all select AID as PID,CompanyName as Firstname,'' as Lastname, Logo as Picture from A_Profile inner join G_Profiles on A_Profile.AID= G_Profiles.uid WHERE G_Profiles.status=1";
        if (txtName.Text != "")
            commandStr = commandStr + "and CompanyName LIKE " + "'%" + txtName.Text + "%'";
        if (txtCity.Text != "")
            commandStr = commandStr + "and city LIKE " + "'%" + txtCity.Text + "%'";
        if (txtZip.Text != "")
        {
            string Zip = GetInitialCoordinates(txtZip.Text);
            commandStr = commandStr + "and  Zip in ( " + Zip + " )";
        }
        if (ddlState.SelectedValue != "--Select--")
            commandStr = commandStr + "and State='" + ddlState.SelectedValue + "'";
        // SQL For B Profile
        commandStr = commandStr + "union all select BID as PID,CompanyName as Firstname,'' as Lastname, logo as Picture from B_Profile inner join G_Profiles on B_Profile.BID= G_Profiles.uid WHERE G_Profiles.status=1";
        if (txtName.Text != "")
            commandStr = commandStr + "and CompanyName LIKE " + "'%" + txtName.Text + "%'";
        if (txtCity.Text != "")
            commandStr = commandStr + "and city LIKE " + "'%" + txtCity.Text + "%'";
        if (txtZip.Text != "")
        {
            string Zip = GetInitialCoordinates(txtZip.Text);
            commandStr = commandStr + "and  Zip in ( " + Zip + " )";
        }
        if (ddlState.SelectedValue != "--Select--")
            commandStr = commandStr + "and State='" + ddlState.SelectedValue + "'";
        // SQL For S Profile
        commandStr = commandStr + " union all select SID AS PID ,Firstname,Lastname,Picture  from S_Profile inner join G_Profiles on S_Profile.SID= G_Profiles.uid WHERE   G_Profiles.status=1 ";
        if (txtName.Text != "")
            commandStr = commandStr + "and FirstName LIKE " + "'%" + txtName.Text + "%'" + " OR LastName LIKE " + "'%" + txtName.Text + "%'";
        if (txtCity.Text != "")
            commandStr = commandStr + "and city LIKE " + "'%" + txtCity.Text + "%'";
        if (txtZip.Text != "")
        {
            string Zip = GetInitialCoordinates(txtZip.Text);
            commandStr = commandStr + "and  Zip in ( " + Zip + " )";
        }
        if (ddlState.SelectedValue != "--Select--")
            commandStr = commandStr + "and State='" + ddlState.SelectedValue + "'";
        System.Data.SqlClient.SqlCommand myCommand = new System.Data.SqlClient.SqlCommand(commandStr, connection);

        System.Data.SqlClient.SqlDataReader myReader = myCommand.ExecuteReader();





        GV.DataSource = myReader;

        GV.DataBind();

        myReader.Close();

        string retGV = Convert.ToString(myCommand.ExecuteScalar());

        if (retGV == "")
        {
            Label3.Text = "Your search result returned no value. Please search again";
            btnForward.Visible = false;

        }
        else
        {
            Label3.Visible = false;

        }

        connection.Close();

    }
    public string GetInitialCoordinates(string zip)
    {
        //This subroutine requires a Label control named lblStatus
        string nearrestZip = "";
        //Prepare to connect to db and execute stored procedure
        string connectionStr = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        SqlConnection objConn = new SqlConnection(connectionStr);
        SqlCommand objCmd = new SqlCommand("sp_get_zip_code", objConn);
        objCmd.CommandType = CommandType.StoredProcedure;

        //we need to supply the ZIP code as an input parameter to our stored procedure
        objCmd.Parameters.Add(new SqlParameter("zipcode", SqlDbType.Char, 5));
        objCmd.Parameters["zipcode"].Value = zip;

        //sglMinLat = south, sglMaxLat = north, sglMinLon = west, sglMaxLon = east
        double sglMinLat = 0;
        double sglMaxLat = 0;
        double sglMinLon = 0;
        double sglMaxLon = 0;

        try
        {
            //open connection
            objConn.Open();
            //put results into datareader
            SqlDataReader objReader = default(SqlDataReader);
            objReader = objCmd.ExecuteReader();
            if (objReader.HasRows)
            {
                //if starting point found, calculate box points
                objReader.Read();
                double radious = Convert.ToDouble(ddlRadius.SelectedValue);
                double radiousE = 3959;
                double lat = Convert.ToDouble(objReader["latitude"]);
                double lon = Convert.ToDouble(objReader["longitude"]);
                sglMaxLat = lat + rad2deg(radious / radiousE);
                sglMinLat = lat - rad2deg(radious / radiousE);

                sglMaxLon = lon + rad2deg(radious / radiousE / Math.Cos(deg2rad(lat)));
                sglMinLon = lon - rad2deg(radious / radiousE / Math.Cos(deg2rad(lat)));

                lat = deg2rad(lat);
                lon = deg2rad(lon);
                //populate gridview
                nearrestZip = PopulateGridView(sglMinLat, sglMaxLat, sglMinLon, sglMaxLon, lat, lon);
            }
            else
            {
                //starting point not found
                // lblStatus.Text = "Error retrieving initial ZIP Code coordinates: No record found for " + tbZip.Text + ".";
            }
            objConn.Close();
            objCmd.Dispose();
            objConn.Dispose();
        }
        catch (Exception ex)
        {
            //technical problem running the query
            // lblStatus.Text = "Error executing database query for initial coordinates: " + ex.Message;
        }
        return nearrestZip;
    }
    private double deg2rad(double deg)
    {
        return (deg * Math.PI / 180.0);
    }
    private double rad2deg(double rad)
    {
        return (rad / Math.PI * 180.0);
    }
    public double CalculateLongitudeCoordinate(double sglLon1, double sglLat1, double sglLat2, int intRadius, int intBearing, int intDistance)
    {
        return sglLon1 + Math.Atan2(Math.Sin(intBearing) * Math.Sin(intDistance / intRadius) * Math.Cos(sglLat1), Math.Cos(intDistance / intRadius) - Math.Sin(sglLat1) * Math.Sin(sglLat2));
    }

    public double CalculateLatitudeCoordinate(double sglLat1, int intRadius, int intBearing, int intDistance)
    {
        return Math.Asin(Math.Sin(sglLat1) * Math.Cos(intDistance / intRadius) + Math.Cos(sglLat1) * Math.Sin(intDistance / intRadius) * Math.Cos(intBearing));
    }
    public string PopulateGridView(double sglMinLat, double sglMaxLat, double sglMinLon, double sglMaxLon, double sglStartLat, double sglStartLon)
    {
        string nearestZip = string.Empty; DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;

        SqlConnection con = null;
        try
        {
            con = new SqlConnection(conString);
            cmd = new SqlCommand("sp_get_zips_in_radius", con);
            cmd.Parameters.Add(new SqlParameter("@minlat", sglMinLat));
            cmd.Parameters.Add(new SqlParameter("@maxlat", sglMaxLat));
            cmd.Parameters.Add(new SqlParameter("@minlon", sglMinLon));
            cmd.Parameters.Add(new SqlParameter("@maxlon", sglMaxLon));
            cmd.Parameters.Add(new SqlParameter("@startlat", sglStartLat));
            cmd.Parameters.Add(new SqlParameter("@startlon", sglStartLon));
            cmd.Parameters.Add(new SqlParameter("@radius", "3959"));
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dt);


            for (int i = 0; i < dt.Rows.Count - 1; i++)
            {
                nearestZip = nearestZip + "," + dt.Rows[i]["zipcode"].ToString();
            }
            nearestZip = nearestZip.Remove(0, 1);
            //  BindSearchBasedUponZip(nearestZip);
        }
        catch (Exception x)
        {

        }
        finally
        {
            cmd.Dispose();
            con.Close();
        }
        return nearestZip;
    }

    protected void GV_SelectedIndexChanged(object sender, EventArgs e)
    {
        btnForward.Visible = true;
        GridViewRow row = GV.SelectedRow;

        string fname = row.Cells[0].Text;
        string lname = row.Cells[1].Text;

        btnForward.Text = "Click here to forward this profile to " + fname + " " + lname;

        Label2.Text = row.Cells[2].Text;
        GV.Visible = false;
    }

    protected void btnForward_Click(object sender, EventArgs e)
    {
        string UID = (string)Session["UID"];
        string QID = Request.QueryString["UID"];


        string Date = DateTime.Now.ToString();
        string Subject = "Forwarded Profile!";
        string Msg = "Hello, I was interested about this profile, please take a look by <a href=http://st.ysharp.com/PProfile.aspx?UID=" + QID + "> clicking here </a>";
        string Status = "1";


        string mailto = Label2.Text;
        string mailfrom = UID;
        string mailabout = QID;

        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;
        SqlConnection con = null;
        SqlCommand cmd = null;


        String sQuery = "INSERT INTO G_InMail (Date, Subject, Msg, Status, mailto, mailfrom) VALUES ('" + Date + "','" + Subject + "','" + Msg + "','" + Status + "','" + Label2.Text + "','" + UID + "')";




        try
        {
            con = new SqlConnection(conString);
            cmd = new SqlCommand(sQuery, con);
            con.Open();
            cmd.ExecuteNonQuery();

        }

        catch
        {

        }

        finally
        {
            con.Close();
            Label1.Text = "Your message has been sent!";
            GV.Visible = false;
            btnForward.Visible = false;
        }
    }


    public String FrmImg(String str)
    {
        if (str != "")
            return ("files/pictures/" + str);

        else
            return "images/no_picture_small.gif";


    }


    protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            String PID = e.Row.Cells[0].Text.ToString();

            Image ddl = (Image)e.Row.FindControl("Image1");
            ddl.ImageUrl = GetUserData(PID, ddl.ImageUrl);
        }
    }
    private string GetUserData(string uid, string URL)
    {
        string userinfo = "";
        string userType = GetUserType(uid);

        if (userType == "A")
        {
            userinfo = URL.Replace("pictures", "A/logo");
        }
        if (userType == "B")
        {
            userinfo = URL.Replace("pictures", "B/logo");
        }
        if (userType == "P")
        {
            userinfo = URL.Replace("pictures", "P/picture");
        }
        if (userType == "S")
        {
            userinfo = URL.Replace("pictures", "S/picture");
        }


        return userinfo;
    }
    private string GetUserType(string uid)
    {
        string userType = "";
        DataTable dtCustomerInfo = new DataTable();
        SqlDataAdapter daCustomerInfo = new SqlDataAdapter();
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;

        SqlConnection con = null;
        string commandStr = "";

        commandStr = "SELECT type FROM G_profiles WHERE type is not null and  UID=" + Convert.ToInt32(uid);

        try
        {
            con = new SqlConnection(conString);
            SqlCommand myCommand = new SqlCommand(commandStr, con);
            daCustomerInfo.SelectCommand = myCommand;
            daCustomerInfo.Fill(dtCustomerInfo);
            if (dtCustomerInfo.Rows.Count > 0)
            {
                if (dtCustomerInfo.Rows[0]["Type"] != DBNull.Value)
                    userType = Convert.ToString(dtCustomerInfo.Rows[0]["Type"]);
                if (userType == "1")
                    userType = "P";
                if (userType == "2")
                    userType = "B";
                if (userType == "3")
                    userType = "S";
                if (userType == "4")
                    userType = "A";
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }
        finally
        {
            con.Close();
        }
        return userType;
    }
}