﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpg.master" AutoEventWireup="true" CodeFile="InMail.aspx.cs" Inherits="InMail" %>
<%@ Register src="UCInMailNav.ascx" tagname="UCInMailNav" tagprefix="Nav" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <br />
    <br />
<center>
<nav:ucinmailnav ID="UCInMailNav1" runat="server" />  
</center>
    <br />
    <br />
    <br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="InMailID" DataSourceID="SqlDataSource1" 
        EmptyDataText="There is no email to display" Width="100%" 
        AllowPaging="True" AllowSorting="True" CellPadding="10" PageSize="50" 
        BackColor="#F2F2F2" onrowdatabound="GridView1_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="Subject" SortExpression="Subject">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Subject") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    
                    <asp:HyperLink ID="HyperLink1" runat="server" 
                        NavigateUrl='<%# Eval("InMailID", "InMailRead.aspx?InMailID={0}") %>' 
                        Text="">
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Subject") %>'></asp:Label>
                        
                        </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Date" HeaderText="Date" 
                SortExpression="Date" >
            </asp:BoundField>
            <asp:BoundField DataField="mailfrom" HeaderText="Mail From" 
                SortExpression="mailfrom" />
            <asp:BoundField DataField="Status" HeaderText="Status" 
                SortExpression="Status" >
             
            <HeaderStyle CssClass="hiddencol" />
            <ItemStyle CssClass="hiddencol" />
            </asp:BoundField>
             
        </Columns>
        <HeaderStyle BackColor="Silver" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
        DeleteCommand="DELETE FROM [G_InMail] WHERE [InMailID] = @InMailID" 
        InsertCommand="INSERT INTO [G_InMail] ([Date], [Subject], [Msg], [Status], [mailto], [mailfrom]) VALUES (@Date, @Subject, @Msg, @Status, @mailto, @mailfrom)" 
        SelectCommand="SELECT [InMailID], [Date], [Subject], [Msg], [Status], [mailto], [mailfrom] FROM [G_InMail] WHERE ([mailto] = @mailto)  ORDER BY [Date] DESC" 
        UpdateCommand="UPDATE [G_InMail] SET [Date] = @Date, [Subject] = @Subject, [Msg] = @Msg, [Status] = @Status, [mailto] = @mailto, [mailfrom] = @mailfrom WHERE [InMailID] = @InMailID">
        <DeleteParameters>
            <asp:Parameter Name="InMailID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Subject" Type="String" />
            <asp:Parameter Name="Msg" Type="String" />
            <asp:Parameter Name="Status" Type="Int32" />
            <asp:Parameter Name="mailto" Type="Int32" />
            <asp:Parameter Name="mailfrom" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="mailto" SessionField="UID" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Subject" Type="String" />
            <asp:Parameter Name="Msg" Type="String" />
            <asp:Parameter Name="Status" Type="Int32" />
            <asp:Parameter Name="mailto" Type="Int32" />
            <asp:Parameter Name="mailfrom" Type="Int32" />
            <asp:Parameter Name="InMailID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>



</asp:Content>

