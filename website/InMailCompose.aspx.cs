﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class InMailCompose : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


        string TOPID = Request.QueryString["topid"];
        if (TOPID == null)
        {

            DetailsView1.Visible = false;
        }



    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string srch = txtSearch.Text;

        string connectionStr = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();

        System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(connectionStr);

        connection.Open();

        //string commandStr = "SELECT PID, FirstName, LastName, Picture FROM P_Profile WHERE FirstName LIKE " + "'%" + srch + "%'" + " OR LastName LIKE " + "'%" + srch + "%'";
        string commandStr = "SELECT PID , FirstName, LastName, Picture FROM P_Profile inner join G_Profiles on P_Profile.PID= G_Profiles.uid WHERE (FirstName LIKE " + "'%" + srch + "%'" + " OR LastName LIKE " + "'%" + srch + "%') and  G_Profiles.status=1" +
       " union all select AID as PID,CompanyName as Firstname,'' as Lastname, Logo as Picture from A_Profile inner join G_Profiles on A_Profile.AID= G_Profiles.uid  WHERE  CompanyName LIKE " + "'%" + srch + "%' and  G_Profiles.status=1" +
       " union all select BID as PID,CompanyName as Firstname,'' as Lastname, logo as Picture from B_Profile inner join G_Profiles on B_Profile.BID= G_Profiles.uid WHERE  CompanyName LIKE " + "'%" + srch + "%'and  G_Profiles.status=1" +
       " union all select SID AS PID ,Firstname,Lastname,Picture  from S_Profile inner join G_Profiles on S_Profile.SID= G_Profiles.uid WHERE   (FirstName LIKE " + "'%" + srch + "%'" + " OR LastName LIKE " + "'%" + srch + "%') and  G_Profiles.status=1 ";
        System.Data.SqlClient.SqlCommand myCommand = new System.Data.SqlClient.SqlCommand(commandStr, connection);

        System.Data.SqlClient.SqlDataReader myReader = myCommand.ExecuteReader();

        GV.DataSource = myReader;

        GV.DataBind();

        myReader.Close();

        connection.Close();

        
    }
    protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            String PID = e.Row.Cells[0].Text.ToString();

            Image ddl = (Image)e.Row.FindControl("Image1");
            ddl.ImageUrl = GetUserData(PID, ddl.ImageUrl);
        }
    }
    private string GetUserData(string uid,string URL)
    {
        string userinfo = "";
        string userType = GetUserType(uid);
       
        if (userType == "A")
        {
            userinfo = URL.Replace("pictures", "A/logo");
        }
        if (userType == "B")
        {
            userinfo = URL.Replace("pictures", "B/logo");
        }
        if (userType == "P")
        {
            userinfo = URL.Replace("pictures", "P/picture");
        }
        if (userType == "S")
        {
            userinfo = URL.Replace("pictures", "S/picture");
        }

       
        return userinfo;
    }
    private string GetUserType(string uid)
    {
        string userType = "";
        DataTable dtCustomerInfo = new DataTable();
        SqlDataAdapter daCustomerInfo = new SqlDataAdapter();
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;

        SqlConnection con = null;
        string commandStr = "";

        commandStr = "SELECT type FROM G_profiles WHERE type is not null and  UID=" + Convert.ToInt32(uid);

        try
        {
            con = new SqlConnection(conString);
            SqlCommand myCommand = new SqlCommand(commandStr, con);
            daCustomerInfo.SelectCommand = myCommand;
            daCustomerInfo.Fill(dtCustomerInfo);
            if (dtCustomerInfo.Rows.Count > 0)
            {
                if (dtCustomerInfo.Rows[0]["Type"] != DBNull.Value)
                    userType = Convert.ToString(dtCustomerInfo.Rows[0]["Type"]);
                if (userType == "1")
                    userType = "P";
                if (userType == "2")
                    userType = "B";
                if (userType == "3")
                    userType = "S";
                if (userType == "4")
                    userType = "A";
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }
        finally
        {
            con.Close();
        }
        return userType;
    }


    public String FrmImg(String str)
    {
        if (str != "")
            return ("files/pictures/" + str);
        else
            return "images/no_picture_small.gif";


    }

}