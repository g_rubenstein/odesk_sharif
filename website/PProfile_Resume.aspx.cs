﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class PProfile_Resume : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string VBID = Request.QueryString["UID"];
        ImageButton1.Visible = false;

        DataTable iDataTable = new DataTable();
        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);
        iSqlConnection.Open();
        SqlDataAdapter iSqlDataAdapter = new SqlDataAdapter("select ResumeU from p_profile where  (pid = '" + VBID + "')", iSqlConnection);
        iSqlDataAdapter.Fill(iDataTable);

        if (iDataTable.Rows[0][0].ToString() != "")
        {
            ImageButton1.ImageUrl = "images/download_resume.gif";
            ImageButton1.Visible = true;
        }
        else
            ImageButton1.Visible = false;

        iSqlConnection.Close();       

    }


    public String FrmText(String str)
    {
        return str.Replace("\r", "<br>");
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        string VBID = Request.QueryString["UID"];
        string Resumes = "";

        DataTable iDataTable = new DataTable();
        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);
        iSqlConnection.Open();
        SqlDataAdapter iSqlDataAdapter = new SqlDataAdapter("select ResumeU from p_profile where  (pid = '" + VBID + "')", iSqlConnection);
        iSqlDataAdapter.Fill(iDataTable);
        if (iDataTable.Rows.Count > 0)
        {
            Resumes = iDataTable.Rows[0][0].ToString();
        }

        iSqlConnection.Close();

        string url = "files/resumes/" + Resumes;

        Response.Redirect(url);
    }
}