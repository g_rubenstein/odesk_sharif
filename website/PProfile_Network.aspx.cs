﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class PProfile_Network : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


        string SPID = "";

        SPID = Request.QueryString["UID"];

        DataTable iDataTable = new DataTable();

        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();

        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

        iSqlConnection.Open();

        string commandStr = "select DISTINCT P_Profile.PID, P_Profile.FirstName, P_Profile.LastName, P_Profile.Picture from P_Profile, G_Connect where (P_Profile.PID = G_Connect.UID) AND (G_Connect.CUID ='" + SPID + "')";


        System.Data.SqlClient.SqlCommand myCommand = new System.Data.SqlClient.SqlCommand(commandStr, iSqlConnection);

        System.Data.SqlClient.SqlDataReader myReader = myCommand.ExecuteReader();

        GVNetwork.DataSource = myReader;

        GVNetwork.DataBind();

        myReader.Close();

        iSqlConnection.Close();



    }


    public String FrmImg(String str)
    {
        if (str != "")
            return ("files/P/picture/" + str);

        else
            return "images/no_picture_small.gif";


    }


}