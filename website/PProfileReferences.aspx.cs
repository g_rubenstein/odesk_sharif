﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class PProfileReferences : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string SPID = "";
        string SFirstName = "";

        if (Session["UID"] != null)
        {
            SPID = Session["UID"].ToString();
        }
        else
            Response.Redirect("SessionTimeOut.aspx");

        DataTable iDataTable = new DataTable();
       
        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        
        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

        iSqlConnection.Open(); 
        
        SqlDataAdapter iSqlDataAdapter = new SqlDataAdapter("SELECT FirstName FROM P_Profile WHERE (PID = '" + SPID + "')", iSqlConnection);
       
        iSqlDataAdapter.Fill(iDataTable);
       
        if (iDataTable.Rows.Count > 0)
        {
            SFirstName = iDataTable.Rows[0][0].ToString();
        }



        Label1.Text = "Hello " + SFirstName + ", please add some references to your profile.";





            string commandStr = "SELECT COUNT(UID) FROM P_References WHERE UID =" + SPID;
            System.Data.SqlClient.SqlCommand myCommand = new System.Data.SqlClient.SqlCommand(commandStr, iSqlConnection);
            System.Data.SqlClient.SqlDataReader myReader = myCommand.ExecuteReader();
            myReader.Close();

            string count = Convert.ToString(myCommand.ExecuteScalar());


            int scount = Convert.ToInt32(count); 



            if (scount >= 6)
            {
                btnSubmit.Enabled = false;
                Label2.Text = "You already have added maximum 6 references. <br>Plesae delete some existing references to add more.";
            }
            else
                btnSubmit.Enabled = true;
            

                    iSqlConnection.Close();
        }









    public bool AddUserToDB(string UID, string Name, string Title, string Company)
    {
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;
        SqlConnection con = null;
        SqlCommand cmd = null;

        String sQuery = "INSERT INTO P_References (UID, Name, Title, Company) VALUES ('" + UID + "','" + Name + "','" + Title + "','" + Company + "')";

        try
        {
            con = new SqlConnection(conString);
            cmd = new SqlCommand(sQuery, con);
            con.Open();
            cmd.ExecuteNonQuery();

        }

        catch
        {

        }

        finally
        {
            con.Close();
        }

        return true;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        DataTable iDataTable = new DataTable();
        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

        iSqlConnection.Open();

        string UID = "";

        if (Session["UID"] != null)
        {
            UID = Session["UID"].ToString();
        }


            string Name = txtName.Text;
            string Title = txtTitle.Text;
            string Company = txtCompany.Text;

            AddUserToDB(UID, Name, Title, Company);


            Response.Redirect("PSignupReferences.aspx");




    }


    protected void GridView1_RowDeleted(Object sender, GridViewDeletedEventArgs e)
    {

        // Display whether the delete operation succeeded.
        if (e.Exception == null)
        {
            Response.Redirect("PSignupReferences.aspx");
            
        }
        else
        {
            Label2.Text = "An error occurred while attempting to delete the row.";
            e.ExceptionHandled = true;
        }

    }







}