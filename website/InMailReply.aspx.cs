﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class InMailReply : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string UID = (string)Session["UID"];
        if (UID == null)
        {
            Response.Redirect("SessionTimeOut.aspx");
        }

        string TOPID = Request.QueryString["topid"];
        if (TOPID == null)
        {

            DetailsView1.Visible = false;
        }

    }
}