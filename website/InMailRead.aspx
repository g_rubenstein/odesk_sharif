﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpg.master" AutoEventWireup="true" CodeFile="InMailRead.aspx.cs" Inherits="InMailRead" %>
<%@ Register src="UCInMailNav.ascx" tagname="UCInMailNav" tagprefix="Nav" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <br />
    <br />
    <br />
<center>
<nav:ucinmailnav ID="UCInMailNav1" runat="server" />  
</center>
    <br />
    <br />
    <br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="InMailID" DataSourceID="SqlDataSource1" 
        EmptyDataText="There are no data records to display." 
        EnableModelValidation="True" BorderWidth="0px" ShowHeader="False" 
        onrowdatabound="GridView1_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="Msg" SortExpression="Msg">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Msg") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    
                    
                   <h1>Mail from User: <asp:Label ID="Label4" runat="server" Text='<%# Bind("mailfrom") %>'></asp:Label></h1>
                    
                    <asp:HyperLink ID="HyperLink1" runat="server" 
                        NavigateUrl='<%# Eval("mailfrom", "InMailReply.aspx?topid={0}") %>' 
                        Text="Reply"></asp:HyperLink>
                    <br />
                    <br />
                    
                    
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Subject") %>' Font-Bold="True"></asp:Label><br />
                    <i>Received at <asp:Label ID="Label1" runat="server" Text='<%# Bind("Date") %>'></asp:Label></i>
                   <hr />
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Msg") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
        DeleteCommand="DELETE FROM [G_InMail] WHERE [InMailID] = @InMailID" 
        InsertCommand="INSERT INTO [G_InMail] ([Date], [Subject], [Msg], [Status], [mailto], [mailfrom]) VALUES (@Date, @Subject, @Msg, @Status, @mailto, @mailfrom)" 
        SelectCommand="SELECT [InMailID], [Date], [Subject], [Msg], [Status], [mailto], [mailfrom] FROM [G_InMail] WHERE (([InMailID] = @InMailID) AND ([mailto] = @mailto))" 
        UpdateCommand="UPDATE [G_InMail] SET [Date] = @Date, [Subject] = @Subject, [Msg] = @Msg, [Status] = @Status, [mailto] = @mailto, [mailfrom] = @mailfrom WHERE [InMailID] = @InMailID">
        <DeleteParameters>
            <asp:Parameter Name="InMailID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Subject" Type="String" />
            <asp:Parameter Name="Msg" Type="String" />
            <asp:Parameter Name="Status" Type="Int32" />
            <asp:Parameter Name="mailto" Type="Int32" />
            <asp:Parameter Name="mailfrom" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="InMailID" QueryStringField="InMailID" 
                Type="Int32" />
            <asp:SessionParameter Name="mailto" SessionField="UID" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Subject" Type="String" />
            <asp:Parameter Name="Msg" Type="String" />
            <asp:Parameter Name="Status" Type="Int32" />
            <asp:Parameter Name="mailto" Type="Int32" />
            <asp:Parameter Name="mailfrom" Type="Int32" />
            <asp:Parameter Name="InMailID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>

