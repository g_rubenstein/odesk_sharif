﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpNoCSS.master" AutoEventWireup="true" CodeFile="PProfile_AccomplishmentsSkills.aspx.cs" Inherits="PProfile_AccomplishmentsSkills" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1>My Accomplishments</h1>
    <br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="AccomplishmentsID" DataSourceID="SqlDataSource1" 
        EmptyDataText="There are no data records to display." BorderWidth="0px" 
        ShowHeader="False">
        <Columns>
            <asp:BoundField DataField="Accomplishments" HeaderText="Accomplishments" 
                SortExpression="Accomplishments" >
            <ItemStyle BorderWidth="0px" />
            </asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
        DeleteCommand="DELETE FROM [P_Accomplishments] WHERE [AccomplishmentsID] = @AccomplishmentsID" 
        InsertCommand="INSERT INTO [P_Accomplishments] ([UID], [Accomplishments]) VALUES (@UID, @Accomplishments)" 
        SelectCommand="SELECT [AccomplishmentsID], [UID], [Accomplishments] FROM [P_Accomplishments] WHERE ([UID] = @UID)" 
        UpdateCommand="UPDATE [P_Accomplishments] SET [UID] = @UID, [Accomplishments] = @Accomplishments WHERE [AccomplishmentsID] = @AccomplishmentsID">
        <DeleteParameters>
            <asp:Parameter Name="AccomplishmentsID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="UID" Type="Int32" />
            <asp:Parameter Name="Accomplishments" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="UID" QueryStringField="UID" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="UID" Type="Int32" />
            <asp:Parameter Name="Accomplishments" Type="String" />
            <asp:Parameter Name="AccomplishmentsID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    <br />
    <br />

     <h1>My Skills</h1>
         <br />
    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="SkillsID" DataSourceID="SqlDataSource2" 
        EmptyDataText="There are no data records to display." BorderWidth="0px" 
        ShowHeader="False">
        <Columns>
            <asp:BoundField DataField="Skills" HeaderText="Skills" 
                SortExpression="Skills" >
            <ItemStyle BorderWidth="0px" />
            </asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
        DeleteCommand="DELETE FROM [P_Skills] WHERE [SkillsID] = @SkillsID" 
        InsertCommand="INSERT INTO [P_Skills] ([UID], [Skills]) VALUES (@UID, @Skills)" 
        SelectCommand="SELECT [SkillsID], [UID], [Skills] FROM [P_Skills] WHERE ([UID] = @UID)" 
        UpdateCommand="UPDATE [P_Skills] SET [UID] = @UID, [Skills] = @Skills WHERE [SkillsID] = @SkillsID">
        <DeleteParameters>
            <asp:Parameter Name="SkillsID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="UID" Type="Int32" />
            <asp:Parameter Name="Skills" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="UID" QueryStringField="UID" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="UID" Type="Int32" />
            <asp:Parameter Name="Skills" Type="String" />
            <asp:Parameter Name="SkillsID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
&nbsp;



</asp:Content>

