﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpp.master" AutoEventWireup="true" CodeFile="PCalendar.aspx.cs" Inherits="PCalendar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<br /><br />
<h1>Manage Calendar and Events</h1>
<br /><br />



    <br />

    <h2>PENDING EVENTS:</h2>
    <br />
    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="CalendarID" DataSourceID="SqlDataSource2" 
        EmptyDataText="There are no data records to display." CellPadding="5" 
        BackColor="White">
        <Columns>
            <asp:BoundField DataField="CalendarID" HeaderText="CalendarID" ReadOnly="True" 
                SortExpression="CalendarID" >
            <HeaderStyle CssClass="hiddencol" />
            <ItemStyle CssClass="hiddencol" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Organizer" SortExpression="Organizer">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Organizer") %>' 
                        Visible="False"></asp:TextBox>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Organizer") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Organizer") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Participant" HeaderText="Participant" 
                SortExpression="Participant" >
            <HeaderStyle CssClass="hiddencol" />
            <ItemStyle CssClass="hiddencol" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Date" SortExpression="Date">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Date") %>' 
                        Visible="False"></asp:TextBox>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Details" SortExpression="Details">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Details") %>' 
                        Visible="False"></asp:TextBox>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("Details") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("Details") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField SortExpression="Status">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Status") %>' 
                        Visible="False"></asp:TextBox>
                    <asp:DropDownList ID="DropDownList1" runat="server" 
                        SelectedValue='<%# Bind("Status") %>'>
                        <asp:ListItem Value="0">Pending</asp:ListItem>
                        <asp:ListItem Value="1">Approved</asp:ListItem>
                        <asp:ListItem Value="2">Deny</asp:ListItem>
                    </asp:DropDownList>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Status") %>' 
                        Visible="False"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowEditButton="True" EditText="Approve Event" />
        </Columns>
        <HeaderStyle BackColor="#CCCCCC" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
        DeleteCommand="DELETE FROM [G_Calendar] WHERE [CalendarID] = @CalendarID" 
        InsertCommand="INSERT INTO [G_Calendar] ([Organizer], [Participant], [Date], [Details], [Status]) VALUES (@Organizer, @Participant, @Date, @Details, @Status)" 
        SelectCommand="SELECT [CalendarID], [Organizer], [Participant], [Date], [Details], [Status] FROM [G_Calendar] WHERE (([Participant] = @Participant) AND ([Status] = @Status) AND [Date] > getdate() )" 
        UpdateCommand="UPDATE [G_Calendar] SET [Organizer] = @Organizer, [Participant] = @Participant, [Date] = @Date, [Details] = @Details, [Status] = @Status WHERE [CalendarID] = @CalendarID">
        <DeleteParameters>
            <asp:Parameter Name="CalendarID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Organizer" Type="Int32" />
            <asp:Parameter Name="Participant" Type="Int32" />
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Details" Type="String" />
            <asp:Parameter Name="Status" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="Participant" SessionField="UID" Type="Int32" />
            <asp:Parameter DefaultValue="0" Name="Status" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Organizer" Type="Int32" />
            <asp:Parameter Name="Participant" Type="Int32" />
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Details" Type="String" />
            <asp:Parameter Name="Status" Type="Int32" />
            <asp:Parameter Name="CalendarID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    <br />
    <br />
    <h2>ALL EVENTS:</h2><br />

    <i>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="CalendarID" DataSourceID="SqlDataSource1" 
        EmptyDataText="There are no data records to display." ForeColor="#666666" 
        AllowSorting="True" CellPadding="5" BorderStyle="Dashed" BorderWidth="3px">
        <Columns>
            <asp:BoundField DataField="CalendarID" HeaderText="CalendarID" ReadOnly="True" 
                SortExpression="CalendarID" />
            <asp:BoundField DataField="Organizer" HeaderText="Organizer" 
                SortExpression="Organizer" />
            <asp:BoundField DataField="Participant" HeaderText="Participant" 
                SortExpression="Participant" />
            <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" />
            <asp:TemplateField HeaderText="Details" SortExpression="Details">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Details") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Details") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Status" HeaderText="Status" 
                SortExpression="Status" >
            <HeaderStyle CssClass="hiddencol" />
            <ItemStyle CssClass="hiddencol" />
            </asp:BoundField>
        </Columns>
        <HeaderStyle BackColor="#CCCCCC" />
    </asp:GridView>
    </i>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
        DeleteCommand="DELETE FROM [G_Calendar] WHERE [CalendarID] = @CalendarID" 
        InsertCommand="INSERT INTO [G_Calendar] ([Organizer], [Participant], [Date], [Details], [Status]) VALUES (@Organizer, @Participant, @Date, @Details, @Status)" 
        SelectCommand="SELECT [CalendarID], [Organizer], [Participant], [Date], [Details], [Status] FROM [G_Calendar] WHERE ([Participant] = @Participant) ORDER BY [Date] DESC" 
        UpdateCommand="UPDATE [G_Calendar] SET [Organizer] = @Organizer, [Participant] = @Participant, [Date] = @Date, [Details] = @Details, [Status] = @Status WHERE [CalendarID] = @CalendarID">
        <DeleteParameters>
            <asp:Parameter Name="CalendarID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Organizer" Type="Int32" />
            <asp:Parameter Name="Participant" Type="Int32" />
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Details" Type="String" />
            <asp:Parameter Name="Status" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="Participant" SessionField="UID" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Organizer" Type="Int32" />
            <asp:Parameter Name="Participant" Type="Int32" />
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Details" Type="String" />
            <asp:Parameter Name="Status" Type="Int32" />
            <asp:Parameter Name="CalendarID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>

