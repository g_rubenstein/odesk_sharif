﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpNoCSS.master" AutoEventWireup="true" CodeFile="PProfile_References.aspx.cs" Inherits="PProfile_References" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">




<h1>My References</h1>

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="ReferenceID" DataSourceID="SqlDataSource1" 
        EmptyDataText="There are no data records to display." BorderWidth="0px" 
        ShowHeader="False">
        <Columns>
            <asp:TemplateField HeaderText="Name" SortExpression="Name">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <br />
                    Name:
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                    <br />
                    Company:
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Company") %>'></asp:Label>
                    <br />
                    Title:
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                    <br />
                    <br />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
        DeleteCommand="DELETE FROM [P_References] WHERE [ReferenceID] = @ReferenceID" 
        InsertCommand="INSERT INTO [P_References] ([UID], [Name], [Company], [Title], [Field1]) VALUES (@UID, @Name, @Company, @Title, @Field1)" 
        ProviderName="<%$ ConnectionStrings:stdbConnectionString.ProviderName %>" 
        SelectCommand="SELECT [ReferenceID], [UID], [Name], [Company], [Title], [Field1] FROM [P_References] WHERE ([UID] = @UID)" 
        UpdateCommand="UPDATE [P_References] SET [UID] = @UID, [Name] = @Name, [Company] = @Company, [Title] = @Title, [Field1] = @Field1 WHERE [ReferenceID] = @ReferenceID">
        <DeleteParameters>
            <asp:Parameter Name="ReferenceID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="UID" Type="Int32" />
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Company" Type="String" />
            <asp:Parameter Name="Title" Type="String" />
            <asp:Parameter Name="Field1" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="UID" QueryStringField="UID" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="UID" Type="Int32" />
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Company" Type="String" />
            <asp:Parameter Name="Title" Type="String" />
            <asp:Parameter Name="Field1" Type="String" />
            <asp:Parameter Name="ReferenceID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>



</asp:Content>

