﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpOut.master" AutoEventWireup="true"
    CodeFile="PSignupMembership.aspx.cs" Inherits="PSignupMembership" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  
    <br />
    <br />
    <br />
    <table border="0" cellpadding="50" cellspacing="0" width="100%">
        <tr>
            <td width="50%" valign="top" align="left">
               <table border="1" cellpadding="10" cellspacing="0" width="600" bordercolor="#256EFF">
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"></td>
    <td width="25%" bgcolor="#C0C0C0" valign="top" align="center"><b><font size="4">SILVER</font></b></td>
    <td width="25%" bgcolor="#DA9100" valign="top" align="center"><b><font size="4">GOLD</font></b></td>
    <td width="25%" bgcolor="#E5E4E2" valign="top" align="center"><b><font size="4">PLATINUM</font></b></td>
  </tr>
  <tr>
      <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Companies Can Browse You</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Companies
      Can Interview You?</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        &nbsp;</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Unlimited Networking with professionals</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>50 limited Searches</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>5 limited Inmails</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Pre-Interview Video&nbsp;
      Capabilities</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Cover letter</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Resume</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Accomplishment and Skills</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Recommendations/Referrals</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>You Can Browse Companies</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">&nbsp;</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Key Words Will Automatically Link You With Companies</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">&nbsp;</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Email Capabilities&nbsp;<br>
      (Inmail 25)</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">&nbsp;</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>100 Searches</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">&nbsp;</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Apply for Jobs</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">&nbsp;</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Portfolio</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">&nbsp;</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Chat</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">&nbsp;</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        Yes</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>

  </tr>



  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Cover Letter 
        Assistance</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Not available</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" />
        Yes, additional $5</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Resume Assistance</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Not available</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        <asp:CheckBox ID="CheckBox2" runat="server"  AutoPostBack="True" />
        Yes, additional $5</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Pre-Interview Video 
        Assistance</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Not available</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        <asp:CheckBox ID="CheckBox3" runat="server"  AutoPostBack="True"/>
        Yes, additional $5</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Who’s Viewed You</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Not available</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">Not available</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Unlimited Searches</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Not available</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">Not available</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Email Capabilities 
        (Inmail 50)</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Not available</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">Not available</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">Yes</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Job Verification</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Not available</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        <asp:CheckBox ID="CheckBox4" runat="server"  AutoPostBack="True"/>
        Yes, additional $10</td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">
        <asp:CheckBox ID="CheckBox5" runat="server"  AutoPostBack="True"/>
        Yes, additional $10</td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b>Pricing</b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">Free</td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True">
            <asp:ListItem Value="14.99">$14.99/month</asp:ListItem>
            <asp:ListItem Selected="True" Value="144.00">$144/annually</asp:ListItem>
        </asp:DropDownList>
      </td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">
        <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True">
            <asp:ListItem Value="24.99">$24.99/month</asp:ListItem>
            <asp:ListItem Value="240" Selected="True">$240/annually</asp:ListItem>
        </asp:DropDownList>
      </td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"><b><font size="4">Due Today</font></b></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2"><font size="4">$0.00</font></td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        <font size="4">$<asp:Label ID="Label4" runat="server" Font-Size="Medium"></asp:Label></font>
      </td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">
        <font size="4">$<asp:Label ID="Label5" runat="server" Font-Size="Medium"></asp:Label></font>
      </td>
  </tr>
  <tr>
    <td width="25%" valign="top" align="center" bgcolor="#B7EDFF"></td>
    <td width="25%" valign="top" align="center" bgcolor="#E5E4E2">
        <asp:ImageButton ID="btnSilver" runat="server" 
            ImageUrl="images/signup-silver.gif" onclick="btnSilver_Click" />
        </td>
    <td width="25%" valign="top" align="center" bgcolor="#FFF9EA">
        <asp:ImageButton ID="btnGold" runat="server"  
            ImageUrl="images/signup-gold.gif" onclick="btnGold_Click"/>
        </td>
    <td width="25%" valign="top" align="center" bgcolor="#EFEEED">
        <asp:ImageButton ID="btnPlatinum" runat="server"  
            ImageUrl="images/signup-platinum.gif" onclick="btnPlatinum_Click"/>
        </td>
  </tr>
</table>
            </td>
            <td width="50%" valign="top" align="left">



<%--                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                &nbsp;<asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" />
                <br />
                <br />
                <asp:GridView ID="GV" runat="server" AutoGenerateColumns="False" EnableModelValidation="True"
                    CellPadding="5" Width="418px">
                    <Columns>
                        <asp:BoundField DataField="FirstName" />
                        <asp:BoundField DataField="LastName" />
                        <asp:BoundField DataField="PID" SortExpression="PID">
                            <HeaderStyle CssClass="hiddencol" />
                            <ItemStyle CssClass="hiddencol" />
                        </asp:BoundField>
                        <asp:CommandField ShowSelectButton="True" />
                    </Columns>
                </asp:GridView>
                <asp:Label ID="Label3" runat="server"></asp:Label>
                <br />
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="#666666"></asp:Label>
                <br />
                <asp:Label ID="Label2" runat="server" Visible="False"></asp:Label>
                <br />
                <p>
                    <asp:SqlDataSource ID="SqlDataSource_Referral" runat="server" ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>"
                        SelectCommand="SELECT [PID], [FirstName] FROM [P_Profile]"></asp:SqlDataSource>
                    <asp:Button ID="btnReferral" runat="server" OnClick="btnReferral_Click" Text="Add Referrar"
                        Font-Bold="True" Height="38px" />
                </p>--%>




                <br /><br />

                        <div class="button">
    <asp:HyperLink ID="ReferencesLink"  class='iframe'
            runat="server">Add Referral</asp:HyperLink>
    </div>











            </td>
        </tr>
      <%--  <tr>
            <td valign="top" align="center" colspan="2" style="width: 100%">
                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="CLICK HERE TO COMPLETE SIGNUP"
                    Visible="false" Width="312px" Height="50px" />
                <asp:Button ID="btnClick" runat="server" Text="CLICK HERE TO COMPLETE SIGNUP" Width="312px"
                    Height="50px" OnClick="btnClick_Click" />
            </td>
        </tr>--%>
    </table>
    </p>
    <p>
        &nbsp;</p>
</asp:Content>
