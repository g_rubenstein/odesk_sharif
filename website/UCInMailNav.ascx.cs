﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UCInMailNav : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string UID = (string)Session["UID"];
        MailInBoxLink.NavigateUrl = "InMail.aspx?uid=" + UID;
        MailComposeLink.NavigateUrl = "InMailCompose.aspx?uid=" + UID;
        MailSentLink.NavigateUrl = "InMailSent.aspx?uid=" + UID;
    }
}