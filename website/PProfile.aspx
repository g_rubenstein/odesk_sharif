﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpp.master" AutoEventWireup="true" CodeFile="PProfile.aspx.cs" Inherits="PProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">







<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td width="75%" colspan="3" class="style1" valign="top" align="left">
    <h1>
    <asp:Label ID="Name" runat="server"></asp:Label>
    </h1>
        <br />


        <br />
        <h3>
            <asp:Label ID="lblEvents" runat="server" Text="Upcoming Events:"></asp:Label>
        </h3>


        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="CalendarID" DataSourceID="SqlDataSource1" 
            EmptyDataText="There are no event or calendar records to display." 
            BorderWidth="1px" BorderColor="White" CellPadding="5" Width="558px" 
            onrowdatabound="GridView1_RowDataBound" 
            >
            <Columns>
                <asp:BoundField DataField="Date" HeaderText="Interview Date" 
                    SortExpression="Date" />
                <asp:TemplateField HeaderText="Interviewer" SortExpression="Organizer">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Organizer") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Organizer") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Details" HeaderText="Details" 
                    SortExpression="Details" />
            </Columns>
            <HeaderStyle BackColor="#CCCCCC" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
            DeleteCommand="DELETE FROM [G_Calendar] WHERE [CalendarID] = @CalendarID" 
            InsertCommand="INSERT INTO [G_Calendar] ([Organizer], [Participant], [Date], [Details], [Status]) VALUES (@Organizer, @Participant, @Date, @Details, @Status)" 
            SelectCommand="SELECT [CalendarID], [Organizer], [Participant], [Date], [Details], [Status] FROM [G_Calendar] WHERE ([Participant] = @Participant) AND ([Date] >= getdate())  AND ([Status] = 1)" 
            UpdateCommand="UPDATE [G_Calendar] SET [Organizer] = @Organizer, [Participant] = @Participant, [Date] = @Date, [Details] = @Details, [Status] = @Status WHERE [CalendarID] = @CalendarID">
            <DeleteParameters>
                <asp:Parameter Name="CalendarID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Organizer" Type="Int32" />
                <asp:Parameter Name="Participant" Type="Int32" />
                <asp:Parameter Name="Date" Type="DateTime" />
                <asp:Parameter Name="Details" Type="String" />
                <asp:Parameter Name="Status" Type="Int32" />
            </InsertParameters>
            <SelectParameters>
                <asp:SessionParameter Name="Participant" SessionField="UID" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="Organizer" Type="Int32" />
                <asp:Parameter Name="Participant" Type="Int32" />
                <asp:Parameter Name="Date" Type="DateTime" />
                <asp:Parameter Name="Details" Type="String" />
                <asp:Parameter Name="Status" Type="Int32" />
                <asp:Parameter Name="CalendarID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>




        <br />
    </td>
    <td width="25%" class="style1" valign="top" align="left">&nbsp;</td>
  </tr>
  <tr>
    <td width="75%" colspan="3" valign="top" align="left">
    <h2>Objective</h2>
        <asp:Label ID="Objective" runat="server"></asp:Label>
        <br />
        <br />
        <br />
      </td>
    <td width="25%" valign="top" align="right">
    
    <div style="padding-right: 35px;">
    
    <asp:HyperLink ID="ReferencesLink"  class='iframe'
            runat="server"><img src="images/references.gif"  border="0" /></asp:HyperLink>
    

    </div>


      </td>
  </tr>
  <tr>
    <td colspan="4">

        

<div class="block">





<%--    <asp:HyperLink  class='iframe' ID="PhotoLink" runat="server">
    <iframe id="contact" src="http://ysharp.com" width="240" height="220" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" style="border: #000000 0px solid;"></iframe>
    <img src="images/zoom.png" height=20px  border=0 style="padding: 5px; "/></asp:HyperLink>--%>



    <asp:HyperLink  class='iframe' ID="PhotoLink" runat="server">
        <asp:Image ID="Image1" runat="server"  width="255" height="250" border=0 />
        <%--<img src="images/zoom.png" height=20px  border=0 style="padding: 5px; "/>--%>
    </asp:HyperLink>

    <asp:HyperLink  class='iframe'  ID="CoverletterLink" runat="server"><img src="images/Coverletter.gif"  width="255" height="250" border=0 /></asp:HyperLink>
    <asp:HyperLink  class='iframe' ID="ResumeLink" runat="server"><img src="images/Resume.gif"  width="255" height="250" border=0 /></asp:HyperLink>
    <asp:HyperLink class='iframe' ID="VideoLink" runat="server">
    <img src="images/video.gif"  width="255" height="250" border=0 />
    </asp:HyperLink>
    <asp:HyperLink class='iframe' ID="RecommendationLinks" runat="server"><img src="images/recommendation.gif"  width="255" height="250" border=0 /></asp:HyperLink>
    <asp:HyperLink class='iframe' ID="AccomplishmentsSkillsLinks" runat="server"><img src="images/accomplishment_ski.gif"  width="255" height="250" border=0 /></asp:HyperLink>
    <asp:HyperLink  class='iframe' ID="ExamplarsLink" runat="server"><img src="images/examplars.gif"  width="255" height="250" border=0 /></asp:HyperLink>
    <asp:HyperLink  class='iframe' ID="NetworkLink" runat="server"><img src="images/network.gif"  width="255" height="250" border=0 />
    
   <div style="position: absolute; top: 355px; right: 91px;">
    
            <asp:DataList ID="DataList1" runat="server" DataSourceID="SqlDataSource2" 
            RepeatDirection="Horizontal">
            <ItemTemplate>
            
            
                <asp:Image ID="Image1" runat="server" Height="60px" 
                    ImageUrl='<%# "files/P/picture/" + Eval("Picture") %>' Width="50px" />
            </ItemTemplate>
        </asp:DataList>    
        
   <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
            SelectCommand="select top 3 P_Profile.PID, G_Connect.UID, P_Profile.Picture from P_Profile, G_Connect where (P_Profile.PID = G_Connect.UID) AND (G_Connect.CUID = @PID) AND (P_Profile.Picture != '')">
            <SelectParameters>
                <asp:QueryStringParameter Name="PID" QueryStringField="UID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>     
    
     
</div>
    
    </asp:HyperLink>
</div>
    

     
        
        
     </td>
  </tr>
  

</table>




<div class="bar">
<asp:HyperLink ID="InterviewLink" class='iframe' runat="server"><img src="images/interview.gif" border="0" /></asp:HyperLink>
<asp:LinkButton ID="btnInterestedLink" runat="server"  onclick="btnInterestedLink_Click"><img src="images/interested.gif"  border="0" /></asp:LinkButton>
<asp:HyperLink class='iframe' ID="ForwardLink" runat="server"><img src="images/forward.gif"  border="0" /></asp:HyperLink>
<asp:HyperLink class='iframe' ID="AddToNetworkLink" runat="server"><img src="images/add_to_network.gif"  border="0" /></asp:HyperLink>
</div>
<br /><br /><br /><br />

<div style="text-align: center">
<asp:Label ID="lblInterested" runat="server" Font-Bold="True" Font-Size="Large" 
        ForeColor="#CC3300"></asp:Label>
</div>

</asp:Content>

