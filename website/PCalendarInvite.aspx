﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpNoCSS.master" AutoEventWireup="true" CodeFile="PCalendarInvite.aspx.cs" Inherits="PCalendarInvite" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<h1>Invite to an interview session</h1>

    <br />
    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" 
        DataKeyNames="CalendarID" DataSourceID="SqlDataSource1" 
        DefaultMode="Insert" Height="50px" Width="688px" BorderWidth="0px" 
        CellPadding="5">
        <Fields>
            <asp:BoundField DataField="CalendarID" HeaderText="CalendarID" 
                InsertVisible="False" ReadOnly="True" SortExpression="CalendarID" >
            <HeaderStyle CssClass="hiddencol" />
            <ItemStyle CssClass="hiddencol" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Organizer" SortExpression="Organizer">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Organizer") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Organizer") %>'   Value='<%# (string)Session["UID"] %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Organizer") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="hiddencol" />
                <ItemStyle CssClass="hiddencol" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Participant" SortExpression="Participant">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Participant") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Participant") %>'  Value='<%# Request.QueryString["uid"] %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Participant") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="hiddencol" />
                <ItemStyle CssClass="hiddencol" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Interview Date" SortExpression="Date">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Date") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Date") %>'    Value='<%# DateTime.Now.ToString() %>'></asp:TextBox>                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle BorderWidth="0px" />
                <ItemStyle BorderWidth="0px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Details" SortExpression="Details">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Details") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Details") %>' 
                        Height="120px" TextMode="MultiLine" Width="522px"></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("Details") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle BorderWidth="0px" />
                <ItemStyle BorderWidth="0px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status" SortExpression="Status">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Status") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Status") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="hiddencol" />
                <ItemStyle CssClass="hiddencol" />
            </asp:TemplateField>
            <asp:CommandField ShowInsertButton="True" ButtonType="Image" 
                CancelImageUrl="~/images/cancel.gif" InsertImageUrl="~/images/submit.gif" >
            <HeaderStyle BorderWidth="0px" />
            <ItemStyle BorderWidth="0px" />
            </asp:CommandField>
        </Fields>
    </asp:DetailsView>
    <br />
    <br />
    <br />

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="CalendarID" DataSourceID="SqlDataSource1" 
        EmptyDataText="There are no data records to display." CellPadding="5">
        <Columns>
            <asp:BoundField DataField="Date" HeaderText="Interview Date" 
                SortExpression="Date" />
            <asp:BoundField DataField="Details" HeaderText="Interview Details" 
                SortExpression="Details" />
        </Columns>
        <HeaderStyle BackColor="#CCCCCC" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
        DeleteCommand="DELETE FROM [G_Calendar] WHERE [CalendarID] = @CalendarID" 
        InsertCommand="INSERT INTO [G_Calendar] ([Organizer], [Participant], [Date], [Details], [Status]) VALUES (@Organizer, @Participant, @Date, @Details, 0)" 
        ProviderName="<%$ ConnectionStrings:stdbConnectionString.ProviderName %>" 
        SelectCommand="SELECT [CalendarID], [Organizer], [Participant], [Date], [Details], [Status] FROM [G_Calendar] WHERE (([Organizer] = @Organizer) AND ([Participant] = @Participant))" 
        UpdateCommand="UPDATE [G_Calendar] SET [Organizer] = @Organizer, [Participant] = @Participant, [Date] = @Date, [Details] = @Details, [Status] = @Status WHERE [CalendarID] = @CalendarID">
        <DeleteParameters>
            <asp:Parameter Name="CalendarID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Organizer" Type="Int32" />
            <asp:Parameter Name="Participant" Type="Int32" />
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Details" Type="String" />
            <asp:Parameter Name="Status" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="Organizer" SessionField="UID" Type="Int32" />
            <asp:QueryStringParameter Name="Participant" QueryStringField="UID" 
                Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Organizer" Type="Int32" />
            <asp:Parameter Name="Participant" Type="Int32" />
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Details" Type="String" />
            <asp:Parameter Name="Status" Type="Int32" />
            <asp:Parameter Name="CalendarID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <br />
    <br />
    <br />
</asp:Content>

