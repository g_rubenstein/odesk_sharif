﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpNoCSS.master" AutoEventWireup="true" CodeFile="PProfile_CoverLetter.aspx.cs" Inherits="PProfile_CoverLetter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1>Cover Letter</h1>

    <br />
    <br />
    <asp:ImageButton ID="ImageButton1" runat="server" 
        onclick="ImageButton1_Click" />
    <br /><br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataSourceID="SqlDataSource1" 
        EmptyDataText="There are no data records to display." ShowHeader="False" 
        BorderWidth="0px">
        <Columns>
            <asp:TemplateField HeaderText="CoverLetter" SortExpression="CoverLetter">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CoverLetter") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# FrmText(Eval("CoverLetter").ToString()) %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle BorderWidth="0px" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
        SelectCommand="SELECT [CoverLetter] FROM [P_Profile] WHERE ([PID] = @PID)">
        <SelectParameters>
            <asp:QueryStringParameter Name="PID" QueryStringField="UID" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br /><br />



</asp:Content>

