﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpp.master" ValidateRequest="false"  AutoEventWireup="true" CodeFile="PProfileUpdate.aspx.cs" Inherits="PProfileUpdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<br /><br />
<h1>Update Profile</h1>
<br /><br />


<table border="0" cellpadding="10" cellspacing="0">
  <tr>
    <td valign="top" align="left">

    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" 
        DataKeyNames="PID" DataSourceID="SqlDataSource1" DefaultMode="Edit" 
        Height="50px" Width="654px" CellPadding="5">
        <Fields>
            <asp:BoundField DataField="PID" HeaderText="PID" ReadOnly="True" 
                SortExpression="PID" />
            <asp:BoundField DataField="FirstName" HeaderText="First Name" 
                SortExpression="FirstName" />
            <asp:BoundField DataField="MiddleInitial" HeaderText="Middle Initial" 
                SortExpression="MiddleInitial" />
            <asp:BoundField DataField="LastName" HeaderText="Last Name" 
                SortExpression="LastName" />
            <asp:BoundField DataField="Gender" HeaderText="Gender" 
                SortExpression="Gender" />
            <asp:BoundField DataField="DOB" HeaderText="DOB" SortExpression="DOB" />
            <asp:BoundField DataField="Address" HeaderText="Address" 
                SortExpression="Address" />
            <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
            <asp:BoundField DataField="State" HeaderText="State" SortExpression="State" />
            <asp:BoundField DataField="Zip" HeaderText="Zip" SortExpression="Zip" />
            <asp:BoundField DataField="Phone1" HeaderText="Phone1" 
                SortExpression="Phone1" />
            <asp:BoundField DataField="Phone2" HeaderText="Phone2" 
                SortExpression="Phone2" />
            <asp:BoundField DataField="Profession" HeaderText="Profession" 
                SortExpression="Profession" />
            <asp:BoundField DataField="ConfirmCode" HeaderText="ConfirmCode" 
                SortExpression="ConfirmCode" >
            <HeaderStyle CssClass="hiddencol" />
            <ItemStyle CssClass="hiddencol" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Picture" SortExpression="Picture">
                <EditItemTemplate>
                   <%-- <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Picture") %>'></asp:TextBox>--%>


                    <asp:Label ID='XXX' runat="server" Text='<%# Bind("Picture") %>'>
                     </asp:Label>
                     <asp:FileUpload ID="FFF" runat="server" />
                     <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Upload" /> 

                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Picture") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("Picture") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Cover Letter" SortExpression="CoverLetter">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Height="150px" 
                        Text='<%# Bind("CoverLetter") %>' TextMode="MultiLine" Width="500px"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CoverLetter") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("CoverLetter") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Resume" SortExpression="Resume">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Resume") %>' 
                        Height="150px" TextMode="MultiLine" Width="500px"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Resume") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Resume") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Objective" SortExpression="Objective">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Height="100px" 
                        Text='<%# Bind("Objective") %>' TextMode="MultiLine" Width="500px"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Objective") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("Objective") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Video URL" SortExpression="VideoURL">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Height="100px" 
                        Text='<%# Bind("VideoURL") %>' TextMode="MultiLine" Width="500px"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("VideoURL") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("VideoURL") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Cover Letter Upload" 
                SortExpression="CoverLetterU">
                <EditItemTemplate>
                    <%--<asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("CoverLetterU") %>'></asp:TextBox>--%>

                                        <asp:Label ID='CCC' runat="server" Text='<%# Bind("CoverLetterU") %>'>
                     </asp:Label>
                     <asp:FileUpload ID="DDD" runat="server" />
                     <asp:Button ID="Button2" runat="server" onclick="Button2_Click" Text="Upload" /> 


                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("CoverLetterU") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("CoverLetterU") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Resume Upload" SortExpression="ResumeU">
                <EditItemTemplate>
                    <%--<asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("ResumeU") %>'></asp:TextBox>--%>

                     <asp:Label ID='RRR' runat="server" Text='<%# Bind("CoverLetterU") %>'>
                     </asp:Label>
                     <asp:FileUpload ID="SSS" runat="server" />
                     <asp:Button ID="Button3" runat="server" onclick="Button3_Click" Text="Upload" /> 


                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("ResumeU") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("ResumeU") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowEditButton="True" ButtonType="Image" 
                CancelImageUrl="~/images/cancel.gif" UpdateImageUrl="~/images/update.gif" />
        </Fields>
    </asp:DetailsView>

        <br />

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
        DeleteCommand="DELETE FROM [P_Profile] WHERE [PID] = @PID" 
        InsertCommand="INSERT INTO [P_Profile] ([PID], [FirstName], [MiddleInitial], [LastName], [Gender], [DOB], [Address], [City], [State], [Zip], [Phone1], [Phone2], [Profession], [ConfirmCode], [Picture], [CoverLetter], [Resume], [Objective], [VideoURL], [CoverLetterU], [ResumeU], [Field3]) VALUES (@PID, @FirstName, @MiddleInitial, @LastName, @Gender, @DOB, @Address, @City, @State, @Zip, @Phone1, @Phone2, @Profession, @ConfirmCode, @Picture, @CoverLetter, @Resume, @Objective, @VideoURL, @CoverLetterU, @ResumeU, @Field3)" 
        ProviderName="<%$ ConnectionStrings:stdbConnectionString.ProviderName %>" 
        SelectCommand="SELECT [PID], [FirstName], [MiddleInitial], [LastName], [Gender], [DOB], [Address], [City], [State], [Zip], [Phone1], [Phone2], [Profession], [ConfirmCode], [Picture], [CoverLetter], [Resume], [Objective], [VideoURL], [CoverLetterU], [ResumeU], [Field3] FROM [P_Profile] WHERE ([PID] = @PID)" 
        UpdateCommand="UPDATE [P_Profile] SET [FirstName] = @FirstName, [MiddleInitial] = @MiddleInitial, [LastName] = @LastName, [Gender] = @Gender, [DOB] = @DOB, [Address] = @Address, [City] = @City, [State] = @State, [Zip] = @Zip, [Phone1] = @Phone1, [Phone2] = @Phone2, [Profession] = @Profession, [ConfirmCode] = @ConfirmCode, [Picture] = @Picture, [CoverLetter] = @CoverLetter, [Resume] = @Resume, [Objective] = @Objective, [VideoURL] = @VideoURL, [CoverLetterU] = @CoverLetterU, [ResumeU] = @ResumeU, [Field3] = @Field3 WHERE [PID] = @PID">
        <DeleteParameters>
            <asp:Parameter Name="PID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="PID" Type="Int32" />
            <asp:Parameter Name="FirstName" Type="String" />
            <asp:Parameter Name="MiddleInitial" Type="String" />
            <asp:Parameter Name="LastName" Type="String" />
            <asp:Parameter Name="Gender" Type="String" />
            <asp:Parameter DbType="Date" Name="DOB" />
            <asp:Parameter Name="Address" Type="String" />
            <asp:Parameter Name="City" Type="String" />
            <asp:Parameter Name="State" Type="String" />
            <asp:Parameter Name="Zip" Type="Int32" />
            <asp:Parameter Name="Phone1" Type="String" />
            <asp:Parameter Name="Phone2" Type="String" />
            <asp:Parameter Name="Profession" Type="String" />
            <asp:Parameter Name="ConfirmCode" Type="String" />
            <asp:Parameter Name="Picture" Type="String" />
            <asp:Parameter Name="CoverLetter" Type="String" />
            <asp:Parameter Name="Resume" Type="String" />
            <asp:Parameter Name="Objective" Type="String" />
            <asp:Parameter Name="VideoURL" Type="String" />
            <asp:Parameter Name="CoverLetterU" Type="String" />
            <asp:Parameter Name="ResumeU" Type="String" />
            <asp:Parameter Name="Field3" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="PID" SessionField="UID" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="FirstName" Type="String" />
            <asp:Parameter Name="MiddleInitial" Type="String" />
            <asp:Parameter Name="LastName" Type="String" />
            <asp:Parameter Name="Gender" Type="String" />
            <asp:Parameter DbType="Date" Name="DOB" />
            <asp:Parameter Name="Address" Type="String" />
            <asp:Parameter Name="City" Type="String" />
            <asp:Parameter Name="State" Type="String" />
            <asp:Parameter Name="Zip" Type="Int32" />
            <asp:Parameter Name="Phone1" Type="String" />
            <asp:Parameter Name="Phone2" Type="String" />
            <asp:Parameter Name="Profession" Type="String" />
            <asp:Parameter Name="ConfirmCode" Type="String" />
            <asp:Parameter Name="Picture" Type="String" />
            <asp:Parameter Name="CoverLetter" Type="String" />
            <asp:Parameter Name="Resume" Type="String" />
            <asp:Parameter Name="Objective" Type="String" />
            <asp:Parameter Name="VideoURL" Type="String" />
            <asp:Parameter Name="CoverLetterU" Type="String" />
            <asp:Parameter Name="ResumeU" Type="String" />
            <asp:Parameter Name="Field3" Type="String" />
            <asp:Parameter Name="PID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>

    </td>
    <td valign="top" align="left">
        


        <h2><asp:HyperLink ID="PProfileReferencesLink" runat="server">Update References »</asp:HyperLink></h2>
        <br />
        <h2><asp:HyperLink ID="PProfileProfessionalBackgroundsLink" runat="server">Update Professional Backgrounds »</asp:HyperLink></h2>
        <br />
        <h2><asp:HyperLink ID="PProfileExemplarsLink" runat="server">Update Portfolios »</asp:HyperLink></h2>
        <br />
        <h2><asp:HyperLink ID="PProfileAccomplishmentsAndSkillsLink" runat="server">Update Accomplishments/Skills »</asp:HyperLink></h2>
  </tr>
</table>









</asp:Content>

