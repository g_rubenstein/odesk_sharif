﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PProfileUpdate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string QID = Request.QueryString["UID"];

        PProfileReferencesLink.NavigateUrl = "PProfileReferences.aspx?uid=" + QID;
        PProfileProfessionalBackgroundsLink.NavigateUrl = "PProfileProfessionalBackgrounds.aspx?uid=" + QID;
        PProfileProfessionalBackgroundsLink.NavigateUrl = "PProfileProfessionalBackgrounds.aspx?uid=" + QID;
        PProfileExemplarsLink.NavigateUrl = "PProfileExemplars.aspx?uid=" + QID;
        PProfileAccomplishmentsAndSkillsLink.NavigateUrl = "PProfileAccomplishmentsAndSkills.aspx?uid=" + QID;

    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        Button b = (Button)sender;
        DetailsView DetailsView1 = (DetailsView)b.NamingContainer;
        Label rdl = (Label)DetailsView1.FindControl("XXX");
        FileUpload FFF = (FileUpload)DetailsView1.FindControl("FFF");
        try
        {

            if (System.IO.File.Exists(Server.MapPath("~/files/P/picture/" + FFF.FileName)))
                System.IO.File.Delete(Server.MapPath("~/files/P/picture/" + FFF.FileName));
            FFF.SaveAs(Server.MapPath("~/files/P/picture/" + FFF.FileName));
            rdl.Text = FFF.FileName;


        }
        catch
        {


        }
    }


    protected void Button2_Click(object sender, EventArgs e)
    {
        Button b = (Button)sender;
        DetailsView DetailsView1 = (DetailsView)b.NamingContainer;
        Label rdl = (Label)DetailsView1.FindControl("CCC");
        FileUpload FFF = (FileUpload)DetailsView1.FindControl("DDD");
        try
        {

            if (System.IO.File.Exists(Server.MapPath("~/files/P/coverletter/" + FFF.FileName)))
                System.IO.File.Delete(Server.MapPath("~/files/P/coverletter/" + FFF.FileName));
            FFF.SaveAs(Server.MapPath("~/files/P/coverletter/" + FFF.FileName));
            rdl.Text = FFF.FileName;


        }
        catch
        {


        }
    }





    protected void Button3_Click(object sender, EventArgs e)
    {
        Button b = (Button)sender;
        DetailsView DetailsView1 = (DetailsView)b.NamingContainer;
        Label rdl = (Label)DetailsView1.FindControl("RRR");
        FileUpload FFF = (FileUpload)DetailsView1.FindControl("SSS");
        try
        {

            if (System.IO.File.Exists(Server.MapPath("~/files/P/resume/" + FFF.FileName)))
                System.IO.File.Delete(Server.MapPath("~/files/P/resume/" + FFF.FileName));
            FFF.SaveAs(Server.MapPath("~/files/P/resume/" + FFF.FileName));
            rdl.Text = FFF.FileName;


        }
        catch
        {


        }
    }

}