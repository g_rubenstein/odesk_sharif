﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpNoCSS.master" AutoEventWireup="true" CodeFile="PProfile_Forward.aspx.cs" Inherits="PProfile_Forward" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h1>
        Forward Profile</h1>
    <br />
    <h3>Search a member to forward this profile to:</h3>
    <table>
      <tr>
            <td>
                Name &nbsp;<asp:TextBox ID="txtName" runat="server"></asp:TextBox>
            </td>
            <td>
                City &nbsp;<asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
            </td>
            <td>
                State &nbsp;<asp:DropDownList ID="ddlState" runat="server">
                    <asp:ListItem>--Select--</asp:ListItem>
                    <asp:ListItem>AL</asp:ListItem>
                    <asp:ListItem>AK</asp:ListItem>
                    <asp:ListItem>AZ</asp:ListItem>
                    <asp:ListItem>AR</asp:ListItem>
                    <asp:ListItem>CA</asp:ListItem>
                    <asp:ListItem>CO</asp:ListItem>
                    <asp:ListItem>CT</asp:ListItem>
                    <asp:ListItem>DE</asp:ListItem>
                    <asp:ListItem>DC</asp:ListItem>
                    <asp:ListItem>FL</asp:ListItem>
                    <asp:ListItem>GA</asp:ListItem>
                    <asp:ListItem>HI</asp:ListItem>
                    <asp:ListItem>ID</asp:ListItem>
                    <asp:ListItem>IL</asp:ListItem>
                    <asp:ListItem>IN</asp:ListItem>
                    <asp:ListItem>IA</asp:ListItem>
                    <asp:ListItem>KS</asp:ListItem>
                    <asp:ListItem>KY</asp:ListItem>
                    <asp:ListItem>LA</asp:ListItem>
                    <asp:ListItem>ME</asp:ListItem>
                    <asp:ListItem>MD</asp:ListItem>
                    <asp:ListItem>MA</asp:ListItem>
                    <asp:ListItem>MI</asp:ListItem>
                    <asp:ListItem>MN</asp:ListItem>
                    <asp:ListItem>MS</asp:ListItem>
                    <asp:ListItem>MO</asp:ListItem>
                    <asp:ListItem>MT</asp:ListItem>
                    <asp:ListItem>NE</asp:ListItem>
                    <asp:ListItem>NV</asp:ListItem>
                    <asp:ListItem>NH</asp:ListItem>
                    <asp:ListItem>NJ</asp:ListItem>
                    <asp:ListItem>NM</asp:ListItem>
                    <asp:ListItem>NY</asp:ListItem>
                    <asp:ListItem>NC</asp:ListItem>
                    <asp:ListItem>ND</asp:ListItem>
                    <asp:ListItem>OH</asp:ListItem>
                    <asp:ListItem>OK</asp:ListItem>
                    <asp:ListItem>OR</asp:ListItem>
                    <asp:ListItem>PA</asp:ListItem>
                    <asp:ListItem>RI</asp:ListItem>
                    <asp:ListItem>SC</asp:ListItem>
                    <asp:ListItem>SD</asp:ListItem>
                    <asp:ListItem>TN</asp:ListItem>
                    <asp:ListItem>TX</asp:ListItem>
                    <asp:ListItem>UT</asp:ListItem>
                    <asp:ListItem>VT</asp:ListItem>
                    <asp:ListItem>VA</asp:ListItem>
                    <asp:ListItem>WA</asp:ListItem>
                    <asp:ListItem>WV</asp:ListItem>
                    <asp:ListItem>WI</asp:ListItem>
                    <asp:ListItem>WY</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                Zip Code &nbsp;<asp:TextBox ID="txtZip" runat="server"></asp:TextBox>
            </td>
            <td>
                Radius &nbsp;
                <asp:DropDownList ID="ddlRadius" runat="server">
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>40</asp:ListItem>
                    <asp:ListItem>50</asp:ListItem>
                </asp:DropDownList>&nbsp;<asp:Button ID="btnSearch" runat="server" onclick="btnSearch_Click" 
        Text="Search" />
            </td>
        </tr>
    </table>
   

        <br />
    <br />
    <asp:GridView ID="GV" runat="server" AutoGenerateColumns="False" onselectedindexchanged="GV_SelectedIndexChanged" 
            CellPadding="5" Width="418px" ShowHeader="False" 
        onrowdatabound="GV_RowDataBound">
        <Columns>
         <asp:BoundField DataField="PID" SortExpression="PID" >
            <HeaderStyle CssClass="hiddencol" />
            <ItemStyle CssClass="hiddencol" />
            </asp:BoundField>
            <asp:BoundField DataField="FirstName" />
            <asp:BoundField DataField="LastName" />
            <asp:TemplateField>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Picture") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# FrmImg(Eval("Picture").ToString())%>' Width="40px" />
                </ItemTemplate>
            </asp:TemplateField>
           
        <asp:CommandField ShowSelectButton="True" ButtonType="Image" 
                SelectImageUrl="~/images/select.gif" >
            <ItemStyle Font-Bold="True" HorizontalAlign="Center" />
            </asp:CommandField>
        </Columns>
    </asp:GridView>


        <asp:Label ID="Label3" runat="server"></asp:Label>
        <br />
        
        <br />
        <asp:Label ID="Label2" runat="server" Visible="False"></asp:Label>

        <%--<asp:SqlDataSource ID="SqlDataSource_Forward" runat="server" 
            ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
            SelectCommand="SELECT [PID], [FirstName] FROM [P_Profile]">
        </asp:SqlDataSource>--%>
        <asp:Button ID="btnForward" runat="server" Font-Bold="True" Height="38px" 
            onclick="btnForward_Click" BorderWidth="2px" BorderColor="Blue" />
  
    <br />
    <asp:Label ID="Label1" runat="server" Font-Bold="True" ></asp:Label>
</asp:Content>

