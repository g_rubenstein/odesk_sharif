﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
public partial class PUpdateMembership : System.Web.UI.Page
{
    private string sEndDate = "";
    private string sStartDate = "";
    private string sMemberShip = "";
    private string sCoverLetter = "";
    private string sResume = "";
    private string sPreInterview = "";
    private string sJobVerificationGold = "";
    private string sJobVerificationPlatinum = "";
    double creditAmount = 0;
    double gold = 0.00;
    double platinum = 0.00;
    double goldAddon = 0.00;
    double platinumAddon = 0.00;
    bool _isChangeAllowed = true;
    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable dt = GetUserData();
        sEndDate = Convert.ToString(dt.Rows[0]["EndDate"]);
        sStartDate = Convert.ToString(dt.Rows[0]["StartDate"]);
        sMemberShip = Convert.ToString(dt.Rows[0]["Membership"]);
        sCoverLetter = Convert.ToString(dt.Rows[0]["CoverLetter"]);
        sResume = Convert.ToString(dt.Rows[0]["Resume"]);
        sPreInterview = Convert.ToString(dt.Rows[0]["PreInterview"]);
        sJobVerificationGold = Convert.ToString(dt.Rows[0]["JobVerificationGold"]);
        sJobVerificationPlatinum = Convert.ToString(dt.Rows[0]["JobVerificationPlatinum"]);
        creditAmount = Convert.ToDouble(dt.Rows[0]["CreditAmt"]); ;

        lblMembershipValidity.Text = sEndDate;
        gold = Convert.ToDouble(DropDownList1.SelectedValue);
        platinum = Convert.ToDouble(DropDownList2.SelectedValue);



        Label4.Text = gold.ToString();
        Label5.Text = platinum.ToString();
        if (sJobVerificationPlatinum == "0")
        {
            if (CheckBox5.Checked == true)
            {
                platinumAddon = 10.00;

            }

        }
        else
        {
            CheckBox5.Checked = true;
            CheckBox5.Enabled = false;
        }
        Label2.Text = platinumAddon.ToString();
        if (platinumAddon > 0)
            imageAddon.Enabled = true;
        else
            imageAddon.Enabled = false;
        if (sCoverLetter == "0")
        {
            if (CheckBox1.Checked == true)
            {
                goldAddon = goldAddon + 5.00;

            }

        }
        else
        {
            CheckBox1.Checked = true;
            CheckBox1.Enabled = false;
        }
        if (sResume == "0")
        {
            if (CheckBox2.Checked == true)
            {
                goldAddon = goldAddon + 5.00;

            }

        }
        else
        {
            CheckBox2.Checked = true;
            CheckBox2.Enabled = false;
        }
        if (sPreInterview == "0")
        {
            if (CheckBox3.Checked == true)
            {
                goldAddon = goldAddon + 5.00;

            }

        }
        else
        {
            CheckBox3.Checked = true;
            CheckBox3.Enabled = false;
        }
        if (sJobVerificationGold == "0")
        {
            if (CheckBox4.Checked == true)
            {
                goldAddon = goldAddon + 10.00;

            }

        }
        else
        {
            CheckBox4.Checked = true;
            CheckBox4.Enabled = false;
        }
        Label1.Text = goldAddon.ToString();
        if (goldAddon > 0)
            imageGoldAddon.Enabled = true;
        else
            imageGoldAddon.Enabled = false;
        if (Session["UID"] == null)
        {
            Response.Redirect("SessionTimeOut.aspx");
        }



    }
    private DataTable GetUserData()
    {
        DataTable dtCustomerInfo = new DataTable();
        SqlDataAdapter daCustomerInfo = new SqlDataAdapter();
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;
        SqlConnection con = null;
        string commandStr = "SELECT Membership,StartDate, EndDate,CoverLetter,Resume,PreInterview,JobVerificationGold,JobVerificationPlatinum,CreditAmt FROM P_Membership WHERE UID = " + Convert.ToInt32(Session["UID"].ToString());
        try
        {
            con = new SqlConnection(conString);
            SqlCommand myCommand = new SqlCommand(commandStr, con);
            daCustomerInfo.SelectCommand = myCommand;
            daCustomerInfo.Fill(dtCustomerInfo);


        }
        catch (Exception ex)
        {
            throw (ex);
        }
        finally
        {
            con.Close();
        }
        return dtCustomerInfo;
    }
    protected void btnSilver_Click(object sender, ImageClickEventArgs e)
    {
        string expDate = SilverCalucation(0, "1");
        if (_isChangeAllowed == true)
        {
            UpdateUserToDB_Membership(Convert.ToString(Session["UID"]), "1", Convert.ToString(Session["StartDate"]), Convert.ToString(Session["EndDate"]), Convert.ToDouble(Session["CreditAMT"]));
            Response.Redirect("MembershipConfirmation.aspx");
        }
        else
            lblWarning.Text = "You can't change because you have annual membership";
    }
    protected void btnGold_Click(object sender, ImageClickEventArgs e)
    {
        string membership = "";
        if (DropDownList1.SelectedItem.Value == "14.99")
            membership = "2";
        else
            membership = "3";
        string expDate = GoldCalucation(Convert.ToDouble(Label4.Text), membership);
        if (_isChangeAllowed == true)
            Response.Redirect("PaymentProcessing.aspx?User=P&Mode=U");
        else
            lblWarning.Text = "You can't change because you have annual membership";
    }
    protected void btnPlatinum_Click(object sender, ImageClickEventArgs e)
    {
        string membership = "";
        if (DropDownList1.SelectedItem.Value == "14.99")
            membership = "4";
        else
            membership = "5";
        string expDate = PlatinumCalucation(Convert.ToDouble(Label4.Text), membership);
        if (_isChangeAllowed == true)
            Response.Redirect("PaymentProcessing.aspx?User=P&Mode=U");
        else
            lblWarning.Text = "You can't change because you have annual membership";

    }
    private void SetAddonsINSession()
    {
        if (CheckBox1.Checked)
            Session["CoverLetter"] = "1";
        else
            Session["CoverLetter"] = "0";
        if (CheckBox2.Checked)
            Session["Resume"] = "1";
        else
            Session["Resume"] = "0";
        if (CheckBox3.Checked)
            Session["PreInterview"] = "1";
        else
            Session["PreInterview"] = "0";
        if (CheckBox4.Checked)
            Session["JobVerificationGold"] = "1";
        else
            Session["JobVerificationGold"] = "0";
        if (CheckBox5.Checked)
            Session["JobVerificationPlatinum"] = "1";
        else
            Session["JobVerificationPlatinum"] = "0";

    }
    private string GoldCalucation(double dMembsershipFee, string sProposedMembership)
    {
        string sExpiryDate = "";
        DataTable dt = GetUserData();
        //string TennureType = Convert.ToString(dt.Rows[0]["TennureType"]);
        //*---------------- Membership Description------------------------------------
        // 1  Silver
        // 2  Monthly Gold
        // 3  Annualy Gold
        // 4  Monthly Platinum
        // 5  Annualy Platinum
        if (sMemberShip == sProposedMembership)
        {
            Session["Membership"] = sProposedMembership;
            Session["MembershipFee"] = dMembsershipFee - creditAmount;
            Session["StartDate"] = Convert.ToString(DateTime.Now);
            DateTime mExpiryDate = Convert.ToDateTime(sEndDate);
            DateTime d2 = DateTime.Now;
            TimeSpan t2 = mExpiryDate - d2;
            if ((sProposedMembership == "1") && (sMemberShip == "1"))
                _isChangeAllowed = false;
            if (t2.Days > 0)
            {
                // If user selected either monthly gold 
                if ((sProposedMembership == "2"))
                    Session["EndDate"] = Convert.ToDateTime(sEndDate).AddMonths(1).ToString();
            }
            else
            { // If user selected either annual gold 
                if ((sProposedMembership == "3"))
                    Session["EndDate"] = DateTime.Now.AddYears(1).ToString();
            }

        }
        else
        {
            DateTime StartDate = new DateTime();
            StartDate = Convert.ToDateTime(sStartDate);
            DateTime ExpDate = Convert.ToDateTime(sEndDate);
            DateTime d1 = DateTime.Now;
            DateTime d2 = StartDate.AddYears(1);
            DateTime d3 = StartDate.AddMonths(1);
            TimeSpan t = ExpDate - d1;
            double NrOfDays = t.TotalDays;
            double newMembership = 0;
            Session["Membership"] = sProposedMembership;
            switch (sProposedMembership)
            {


                case "2":
                    {
                        Session["EndDate"] = DateTime.Now.AddMonths(1).ToString();
                        if (sMemberShip == "1")
                        {
                            newMembership = dMembsershipFee - creditAmount;
                            if (newMembership > 0)
                            {
                                Session["MembershipFee"] = newMembership;
                                Session["CreditAMT"] = "0";
                            }
                            else
                            {
                                Session["MembershipFee"] = dMembsershipFee;
                                Session["CreditAMT"] = creditAmount;
                            }

                        }
                        else if (sMemberShip == "3")
                        {
                            if (NrOfDays > 0)
                                creditAmount = creditAmount + ((140 / 365) * NrOfDays);
                            newMembership = dMembsershipFee - creditAmount;
                            if (newMembership > 0)
                            {
                                Session["MembershipFee"] = newMembership;
                                Session["CreditAMT"] = "0";
                            }
                            else
                            {
                                Session["MembershipFee"] = dMembsershipFee;
                                Session["CreditAMT"] = creditAmount;
                            }
                        }
                        else if (sMemberShip == "4")
                        {
                            if (NrOfDays > 0)
                                creditAmount = creditAmount + ((24.99 / 30) * NrOfDays);
                            newMembership = dMembsershipFee - creditAmount;
                            if (newMembership > 0)
                            {
                                Session["MembershipFee"] = newMembership;
                                Session["CreditAMT"] = "0";
                            }
                            else
                            {
                                Session["MembershipFee"] = dMembsershipFee;
                                Session["CreditAMT"] = creditAmount;
                            }
                        }
                        else if (sMemberShip == "5")
                        {
                            if (NrOfDays > 0)
                                creditAmount = creditAmount + ((240 / 365) * NrOfDays);
                            newMembership = dMembsershipFee - creditAmount;
                            if (newMembership > 0)
                            {
                                Session["MembershipFee"] = newMembership;
                                Session["CreditAMT"] = "0";
                            }
                            else
                            {
                                Session["MembershipFee"] = dMembsershipFee;
                                Session["CreditAMT"] = creditAmount;
                            }
                        }
                        return sExpiryDate;
                    }
                case "3":
                    {
                        Session["EndDate"] = DateTime.Now.AddYears(1).ToString();
                        if (sMemberShip == "1")
                        {
                            newMembership = dMembsershipFee - creditAmount;
                            if (newMembership > 0)
                            {
                                Session["MembershipFee"] = newMembership;
                                Session["CreditAMT"] = "0";
                            }
                            else
                            {
                                Session["MembershipFee"] = dMembsershipFee;
                                Session["CreditAMT"] = creditAmount;
                            }

                        }
                        else if (sMemberShip == "2")
                        {
                            if (NrOfDays > 0)
                                creditAmount = creditAmount + ((14.99 / 30) * NrOfDays);
                            newMembership = dMembsershipFee - creditAmount;
                            if (newMembership > 0)
                            {
                                Session["MembershipFee"] = newMembership;
                                Session["CreditAMT"] = "0";
                            }
                            else
                            {
                                Session["MembershipFee"] = dMembsershipFee;
                                Session["CreditAMT"] = creditAmount;
                            }
                        }
                        else if (sMemberShip == "4")
                        {
                            if (NrOfDays > 0)
                                creditAmount = creditAmount + ((24.99 / 30) * NrOfDays);
                            newMembership = dMembsershipFee - creditAmount;
                            if (newMembership > 0)
                            {
                                Session["MembershipFee"] = newMembership;
                                Session["CreditAMT"] = "0";
                            }
                            else
                            {
                                Session["MembershipFee"] = dMembsershipFee;
                                Session["CreditAMT"] = creditAmount;
                            }
                        }
                        else if (sMemberShip == "5")
                        {
                            if (NrOfDays > 0)
                                creditAmount = creditAmount + (240.00 / 365) * NrOfDays;
                            newMembership = dMembsershipFee - creditAmount;
                            if (newMembership > 0)
                            {
                                Session["MembershipFee"] = newMembership;
                                Session["CreditAMT"] = "0";
                            }
                            else
                            {
                                Session["MembershipFee"] = dMembsershipFee;
                                Session["CreditAMT"] = creditAmount;
                            }
                        }
                        return sExpiryDate;
                    }

            }
        }
        return sExpiryDate;

    }
    private string PlatinumCalucation(double dMembsershipFee, string sProposedMembership)
    {
        string sExpiryDate = "";
        DataTable dt = GetUserData();
        //string TennureType = Convert.ToString(dt.Rows[0]["TennureType"]);
        //*---------------- Membership Description------------------------------------
        // 1  Silver
        // 2  Monthly Gold
        // 3  Annualy Gold
        // 4  Monthly Platinum
        // 5  Annualy Platinum
        if (sMemberShip == sProposedMembership)
        {
            Session["Membership"] = sProposedMembership;
            Session["MembershipFee"] = dMembsershipFee - creditAmount;
            Session["StartDate"] = Convert.ToString(DateTime.Now);
            DateTime mExpiryDate = Convert.ToDateTime(sEndDate);
            DateTime d2 = DateTime.Now;
            TimeSpan t2 = mExpiryDate - d2;
            if ((sProposedMembership == "1") && (sMemberShip == "1"))
                _isChangeAllowed = false;
            if (t2.Days > 0)
            {
                // If user selected either monthly gold 
                if ((sProposedMembership == "4"))
                    Session["EndDate"] = Convert.ToDateTime(sEndDate).AddMonths(1).ToString();
            }
            else
            { // If user selected either annual gold 
                if ((sProposedMembership == "5"))
                    Session["EndDate"] = DateTime.Now.AddYears(1).ToString();
            }

        }
        else
        {
            DateTime StartDate = new DateTime();
            StartDate = Convert.ToDateTime(sStartDate);
            DateTime ExpDate = Convert.ToDateTime(sEndDate);
            DateTime d1 = DateTime.Now;
            DateTime d2 = StartDate.AddYears(1);
            DateTime d3 = StartDate.AddMonths(1);
            TimeSpan t = ExpDate - d1;
            double NrOfDays = t.TotalDays;
            Session["Membership"] = sProposedMembership;
            double newMembership = 0;
            switch (sProposedMembership)
            {


                case "4":
                    {
                        Session["EndDate"] = DateTime.Now.AddMonths(1).ToString();
                        if (sMemberShip == "1")
                        {
                            newMembership = dMembsershipFee - creditAmount;
                            if (newMembership > 0)
                            {
                                Session["MembershipFee"] = newMembership;
                                Session["CreditAMT"] = "0";
                            }
                            else
                            {
                                Session["MembershipFee"] = dMembsershipFee;
                                Session["CreditAMT"] = creditAmount;
                            }

                        }
                        else if (sMemberShip == "2")
                        {
                            if (NrOfDays > 0)
                                creditAmount = creditAmount + ((14.99 / 30) * NrOfDays);
                            newMembership = dMembsershipFee - creditAmount;
                            if (newMembership > 0)
                            {
                                Session["MembershipFee"] = newMembership;
                                Session["CreditAMT"] = "0";
                            }
                            else
                            {
                                Session["MembershipFee"] = dMembsershipFee;
                                Session["CreditAMT"] = creditAmount;
                            }
                        }
                        else if (sMemberShip == "3")
                        {
                            if (NrOfDays > 0)
                                creditAmount = creditAmount + ((140.00 / 365) * NrOfDays);
                            newMembership = dMembsershipFee - creditAmount;
                            if (newMembership > 0)
                            {
                                Session["MembershipFee"] = newMembership;
                                Session["CreditAMT"] = "0";
                            }
                            else
                            {
                                Session["MembershipFee"] = dMembsershipFee;
                                Session["CreditAMT"] = creditAmount;
                            }
                        }

                        else if (sMemberShip == "5")
                        {
                            if (NrOfDays > 0)
                                creditAmount = creditAmount + ((240.00 / 365) * NrOfDays);
                            newMembership = dMembsershipFee - creditAmount;
                            if (newMembership > 0)
                            {
                                Session["MembershipFee"] = newMembership;
                                Session["CreditAMT"] = "0";
                            }
                            else
                            {
                                Session["MembershipFee"] = dMembsershipFee;
                                Session["CreditAMT"] = creditAmount;
                            }
                        }
                        return sExpiryDate;
                    }
                case "5":
                    {
                        Session["EndDate"] = DateTime.Now.AddYears(1).ToString();
                        if (sMemberShip == "1")
                        {
                            newMembership = dMembsershipFee - creditAmount;
                            if (newMembership > 0)
                            {
                                Session["MembershipFee"] = newMembership;
                                Session["CreditAMT"] = "0";
                            }
                            else
                            {
                                Session["MembershipFee"] = dMembsershipFee;
                                Session["CreditAMT"] = creditAmount;
                            }

                        }
                        else if (sMemberShip == "2")
                        {
                            if (NrOfDays > 0)
                                creditAmount = creditAmount + ((14.99 / 30) * NrOfDays);
                            newMembership = dMembsershipFee - creditAmount;
                            if (newMembership > 0)
                            {
                                Session["MembershipFee"] = newMembership;
                                Session["CreditAMT"] = "0";
                            }
                            else
                            {
                                Session["MembershipFee"] = dMembsershipFee;
                                Session["CreditAMT"] = creditAmount;
                            }
                        }
                        else if (sMemberShip == "4")
                        {
                            if (NrOfDays > 0)
                                creditAmount = creditAmount + ((24.99 / 30) * NrOfDays);
                            newMembership = dMembsershipFee - creditAmount;
                            if (newMembership > 0)
                            {
                                Session["MembershipFee"] = newMembership;
                                Session["CreditAMT"] = "0";
                            }
                            else
                            {
                                Session["MembershipFee"] = dMembsershipFee;
                                Session["CreditAMT"] = creditAmount;
                            }
                        }
                        else if (sMemberShip == "3")
                        {
                            if (NrOfDays > 0)
                                creditAmount = creditAmount + ((144.00 / 365) * NrOfDays);
                            newMembership = dMembsershipFee - creditAmount;
                            if (newMembership > 0)
                            {
                                Session["MembershipFee"] = newMembership;
                                Session["CreditAMT"] = "0";
                            }
                            else
                            {
                                Session["MembershipFee"] = dMembsershipFee;
                                Session["CreditAMT"] = creditAmount;
                            }
                        }
                        return sExpiryDate;
                    }

            }
        }
        return sExpiryDate;

    }
    private string SilverCalucation(double dMembsershipFee, string sProposedMembership)
    {
        string sExpiryDate = "";
        DataTable dt = GetUserData();
        //string TennureType = Convert.ToString(dt.Rows[0]["TennureType"]);
        //*---------------- Membership Description------------------------------------
        // 1  Silver
        // 2  Monthly Gold
        // 3  Annualy Gold
        // 4  Monthly Platinum

        DateTime StartDate = new DateTime();
        StartDate = Convert.ToDateTime(sStartDate);
        DateTime ExpDate = Convert.ToDateTime(sEndDate);
        DateTime d1 = DateTime.Now;
        DateTime d2 = StartDate.AddYears(1);
        DateTime d3 = StartDate.AddMonths(1);
        TimeSpan t = ExpDate - d1;
        double NrOfDays = t.TotalDays;
        switch (sProposedMembership)
        {


            case "1":
                {
                    Session["EndDate"] = DateTime.Now.AddYears(20).ToString();
                    if (sMemberShip == "1")
                    {
                        _isChangeAllowed = false;

                    }
                    else if (sMemberShip == "2")
                    {
                        if (NrOfDays > 0)
                            creditAmount = creditAmount + ((14.99 / 30) * NrOfDays);
                        Session["CreditAMT"] = creditAmount;
                        Session["MembershipFee"] = 0;
                    }
                    else if (sMemberShip == "3")
                    {
                        if (NrOfDays > 0)
                            creditAmount = creditAmount + ((140.00 / 365) * NrOfDays);
                        Session["CreditAMT"] = creditAmount;
                        Session["MembershipFee"] = 0;
                    }
                    else if (sMemberShip == "4")
                    {
                        if (NrOfDays > 0)
                            creditAmount = creditAmount + ((24.99 / 30) * NrOfDays);
                        Session["CreditAMT"] = creditAmount;
                        Session["MembershipFee"] = 0;
                    }
                    else if (sMemberShip == "5")
                    {
                        if (NrOfDays > 0)
                            creditAmount = creditAmount + ((240.00 / 365) * NrOfDays);
                        Session["CreditAMT"] = creditAmount;
                        Session["MembershipFee"] = 0;
                    }
                    return sExpiryDate;
                }

        }
        return sExpiryDate;

    }
    protected void imageAddon_Click(object sender, ImageClickEventArgs e)
    {
        SetAddonsINSession();
        Session["MembershipFee"] = platinumAddon;
        Response.Redirect("PaymentProcessing.aspx?User=AD&Mode=U");
    }
    protected void imageGoldAddon_Click(object sender, ImageClickEventArgs e)
    {
        SetAddonsINSession();
        Session["MembershipFee"] = goldAddon;
        Response.Redirect("PaymentProcessing.aspx?User=AD&Mode=U");
    }
    public bool UpdateUserToDB_Membership(string UID, string Membership, string StartDate, string EndDate, double creditAmt)
    {
        string sUserType = "";
        if (Request.QueryString["User"] != null && Request.QueryString["User"] != "")
        {
            sUserType = Request.QueryString["User"].ToString();
        }
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;
        SqlConnection con = null;
        SqlCommand cmd = null;
        String sQuery = "";

        sQuery = "UPDATE  P_Membership SET StartDate='" + StartDate + "', EndDate='" + EndDate + "', Membership='" + Membership + "', CreditAmt='" + creditAmt + "' WHERE UID = '" + UID + "'";


        try
        {
            con = new SqlConnection(conString);
            cmd = new SqlCommand(sQuery, con);
            con.Open();
            cmd.ExecuteNonQuery();

        }

        catch
        {

        }

        finally
        {
            con.Close();
        }

        return true;
    }
}
