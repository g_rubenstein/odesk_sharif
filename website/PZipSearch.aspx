﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PZipSearch.aspx.cs" Inherits="PZipSearch"
    MasterPageFile="~/mpOut.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
        <p>
            Select all Users within
            <asp:DropDownList runat="server" ID="ddlDistance">
                <asp:ListItem Selected="True">5</asp:ListItem>
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>25</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>100</asp:ListItem>
            </asp:DropDownList>
            miles of ZIP Code
            <asp:TextBox runat="server" ID="tbZip" Columns="5" />
            <asp:RequiredFieldValidator runat="server" ID="rfvZip" ControlToValidate="tbZip"
                ErrorMessage="Please provide a ZIP Code" CssClass="warning" Display="Dynamic" />
            <asp:RegularExpressionValidator runat="server" ID="revZip" ControlToValidate="tbZip"
                ValidationExpression="^[0-9]{5}$" ErrorMessage="Please enter a valid five-digit ZIP Code"
                CssClass="warning" Display="Dynamic" />
            <asp:Button runat="server" ID="btnZip" Text="Get Users" OnClick="btnZip_Click" />
        </p>
        <asp:GridView runat="server" ID="gvZIP" AutoGenerateColumns="false" PageSize="20"
            HeaderStyle-BackColor="Yellow" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"
            AlternatingRowStyle-BackColor="WhiteSmoke" CellPadding="5">
            <Columns>
                <asp:BoundField HeaderText="PID" DataField="PID" SortExpression="PID" />
                <asp:BoundField HeaderText="First Name" DataField="FirstName" SortExpression="FirstName" />
                <asp:BoundField HeaderText="Last Name" DataField="LastName" SortExpression="LastName" />
                <%--<asp:BoundField HeaderText="Latitude" DataField="latitude" SortExpression="latitude" />
					<asp:BoundField HeaderText="Longitude" DataField="longitude" SortExpression="longitude" />
					<asp:BoundField HeaderText="Distance" DataField="distance" SortExpression="distance" />--%>
            </Columns>
        </asp:GridView>
       
    &nbsp;</p>
     <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
</asp:Content>
