﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpNoCSS.master" AutoEventWireup="true" CodeFile="PProfile_Network.aspx.cs" Inherits="PProfile_Network" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<h1>My Network</h1>
<br />
    <asp:GridView ID="GVNetwork" runat="server" AutoGenerateColumns="False" 
        CellPadding="10" ShowHeader="False" BorderWidth="0px">
        <Columns>
            <asp:TemplateField>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" NavigateUrl='<%# "pprofile.aspx?UID=" + Eval("PID") %>' Target="_parent" runat="server">
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                    &nbsp;<asp:Label ID="Label2" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>
                </asp:HyperLink>
                </ItemTemplate>
                <ItemStyle BorderWidth="0px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Picture") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# FrmImg(Eval("Picture").ToString())%>' Width="40px" />
                </ItemTemplate>
                <ItemStyle BorderWidth="0px" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <br /><br />
    <asp:GridView ID="GVMatches" runat="server">
    </asp:GridView>


</asp:Content>

