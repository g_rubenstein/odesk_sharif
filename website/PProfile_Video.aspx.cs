﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class PProfile_Video : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


        string SPID = "";
        string SVideoURL = "";


        SPID = Request.QueryString["UID"];

        DataTable iDataTable = new DataTable();

        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();

        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

        iSqlConnection.Open();

        SqlDataAdapter iSqlDataAdapter = new SqlDataAdapter("SELECT VideoURL FROM P_Profile WHERE  (PID = '" + SPID + "')", iSqlConnection);

        iSqlDataAdapter.Fill(iDataTable);

        if (iDataTable.Rows.Count > 0)
        {

            SVideoURL = iDataTable.Rows[0][0].ToString();



        }

        iSqlConnection.Close();


        lblVideo.Text = FrmText(SVideoURL);

    }


    public String FrmText(String str)
    {
        return str.Replace("\"", "'");
    }

}