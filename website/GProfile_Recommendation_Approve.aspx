﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpg.master" AutoEventWireup="true" CodeFile="GProfile_Recommendation_Approve.aspx.cs" Inherits="GProfile_Recommendation_Approve" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<br /><br />
<h1>Approve Recommendations</h1>
<br /><br />


    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="RecommendationID" DataSourceID="SqlDataSource1" 
        EmptyDataText="There are no data records to display." CellPadding="10" 
        BackColor="White" onrowdatabound="GridView1_RowDataBound">
        <Columns>
            <asp:BoundField DataField="RecommendationID" HeaderText="RecommendationID" 
                ReadOnly="True" SortExpression="RecommendationID" >
            <HeaderStyle CssClass="hiddencol" />
            <ItemStyle CssClass="hiddencol" />
            </asp:BoundField>
            <asp:BoundField DataField="UID" HeaderText="UID" SortExpression="UID" >
            <HeaderStyle CssClass="hiddencol" />
            <ItemStyle CssClass="hiddencol" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Recommendation" SortExpression="Recommendation">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Recommendation") %>' 
                        Visible="False"></asp:TextBox>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Recommendation") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Recommendation") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="From Who" SortExpression="FromWho">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("FromWho") %>' 
                        Visible="False"></asp:TextBox>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("FromWho") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("FromWho") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date" SortExpression="Date">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Date") %>' 
                        Visible="False"></asp:TextBox>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField SortExpression="Status">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" Visible="false" runat="server" Text='<%# Bind("Status") %>'></asp:TextBox>
                    <asp:DropDownList ID="DropDownList1" runat="server" 
                        SelectedValue='<%# Bind("Status") %>'>
                        <asp:ListItem Value="0">Pending</asp:ListItem>
                        <asp:ListItem Value="1">Approve</asp:ListItem>
                        <asp:ListItem Value="2">Delete</asp:ListItem>
                    </asp:DropDownList>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Status") %>' 
                        Visible="False"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowEditButton="True" EditText="Click Here" />
        </Columns>
        <HeaderStyle BackColor="#CCCCCC" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
        DeleteCommand="DELETE FROM [G_Recommendation] WHERE [RecommendationID] = @RecommendationID" 
        InsertCommand="INSERT INTO [G_Recommendation] ([UID], [Recommendation], [FromWho], [Date], [Status]) VALUES (@UID, @Recommendation, @FromWho, @Date, @Status)" 
        ProviderName="<%$ ConnectionStrings:stdbConnectionString.ProviderName %>" 
        SelectCommand="SELECT [RecommendationID], [UID], [Recommendation], [FromWho], [Date], [Status] FROM [G_Recommendation] WHERE (([UID] = @UID) AND ([Status] = @Status))" 
        UpdateCommand="UPDATE [G_Recommendation] SET [UID] = @UID, [Recommendation] = @Recommendation, [FromWho] = @FromWho, [Date] = @Date, [Status] = @Status WHERE [RecommendationID] = @RecommendationID">
        <DeleteParameters>
            <asp:Parameter Name="RecommendationID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="UID" Type="Int32" />
            <asp:Parameter Name="Recommendation" Type="String" />
            <asp:Parameter Name="FromWho" Type="Int32" />
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Status" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="UID" QueryStringField="UID" Type="Int32" />
            <asp:Parameter DefaultValue="0" Name="Status" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="UID" Type="Int32" />
            <asp:Parameter Name="Recommendation" Type="String" />
            <asp:Parameter Name="FromWho" Type="Int32" />
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Status" Type="Int32" />
            <asp:Parameter Name="RecommendationID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>

