﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UCInMailNav.ascx.cs" Inherits="UCInMailNav" %>

    <asp:HyperLink ID="MailInBoxLink" runat="server">Mail Inbox</asp:HyperLink> | 
    <asp:HyperLink ID="MailComposeLink" runat="server">Mail Compose</asp:HyperLink> | 
    <asp:HyperLink ID="MailSentLink" runat="server">Mail Sent</asp:HyperLink>