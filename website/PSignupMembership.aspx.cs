﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

public partial class PSignupMembership : System.Web.UI.Page
{
    double gold = 0.00;
    double platinum = 0.00;
    protected void Page_Load(object sender, EventArgs e)
    {

        gold = Convert.ToDouble(DropDownList1.SelectedValue);
        platinum = Convert.ToDouble(DropDownList2.SelectedValue);


        if (CheckBox1.Checked == true)
        {
            gold = gold + 5.00;

        }
        if (CheckBox2.Checked == true)
        {
            gold = gold + 5.00;

        }
        if (CheckBox3.Checked == true)
        {
            gold = gold + 5.00;

        }
        if (CheckBox4.Checked == true)
        {
            gold = gold + 10.00;

        }
        if (CheckBox5.Checked == true)
        {
            platinum = platinum + 10.00;

        }

        Label4.Text = gold.ToString();
        Label5.Text = platinum.ToString();
        //if (!IsPostBack)
        //{
        //    btnReferral.Visible = false;
        //}

        string UID = (string)Session["UID"];

        ReferencesLink.NavigateUrl = "GReferral.aspx?uid=" + UID;

    }

    public bool AddUserToDB_Membership(string UID, string Membership, string StartDate, string EndDate, int CoverLetter, int Resume, int PreInterview, int JobVerificationGold,int JobVerificationPlatinum)
    {
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;
        SqlConnection con = null;
        SqlDataAdapter da = null;
        SqlDataReader reader = null;
        SqlCommand cmd = null;

        String sQuery = "INSERT INTO P_Membership (UID, Membership, StartDate,EndDate,CoverLetter,Resume,PreInterview,JobVerificationGold,JobVerificationPlatinum,CreditAmt) VALUES ('" + UID + "','" + Membership + "','" + StartDate + "','" + EndDate + "'," + CoverLetter + "," + Resume + "," + PreInterview + "," + JobVerificationGold + "," + JobVerificationPlatinum + ",0)";

        try
        {
            con = new SqlConnection(conString);
            cmd = new SqlCommand(sQuery, con);
            con.Open();
            cmd.ExecuteNonQuery();

        }

        catch
        {

        }

        finally
        {
            con.Close();
        }

        return true;
    }

    public bool AddUserToDB_Referral(string UID, int ReferredBy)
    {
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;
        SqlConnection con = null;
        SqlDataAdapter da = null;
        SqlDataReader reader = null;
        SqlCommand cmd = null;

        String sQuery = "INSERT INTO P_Referral (UID, ReferredBy) VALUES ('" + UID + "','" + ReferredBy + "')";

        try
        {
            con = new SqlConnection(conString);
            cmd = new SqlCommand(sQuery, con);
            con.Open();
            cmd.ExecuteNonQuery();

        }

        catch
        {

        }

        finally
        {
            con.Close();
        }

        return true;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        //DataTable iDataTable = new DataTable();
        //string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        //SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

        //iSqlConnection.Open();

        //string UID = "";

        //if (Session["UID"] != null)
        //{
        //    UID = Session["UID"].ToString();
        //}


        //string Membership = rbtnlistMembership.SelectedValue;
        //string Date = DateTime.Now.ToString();

        //AddUserToDB_Membership(UID, Membership, Date);

        //Response.Redirect("PSignupConfirmation.aspx");


        //SqlDataAdapter iSqlDataAdapter = new SqlDataAdapter("Select FirstName, Email, Password from P_Profile where (PID= '" + PID + "')", iSqlConnection);
        //iSqlDataAdapter.Fill(iDataTable);

        //string FirstName = iDataTable.Rows[0][0].ToString();
        //string Email = iDataTable.Rows[0][1].ToString();
        //string Password = iDataTable.Rows[0][2].ToString();


        //MailMessage EmailMsg = new MailMessage();
        //EmailMsg.From = new MailAddress("The UBHub Team<support@theubhub.com>");
        //EmailMsg.To.Add(new MailAddress("allen@parmidahome.com"));
        //EmailMsg.Subject = "Welcome to The Ultimate Business Hub " + FirstName + "!";
        //EmailMsg.Body = "Hi " + FirstName + ",\n\nThanks for joining The UBHub! \n\nHere's your account info for logging in:\nE-mail: " + Email + "\nPassword: " + Password + "\n\nPlease keep this information in a safe place.\n\nThanks again,\nTheUbHub Team";
        //SmtpClient MailClient = new SmtpClient("mail.theubhub.com", 587);
        //MailClient.Credentials = new System.Net.NetworkCredential("support@theubhub.com", "misty.77");
        //MailClient.Send(EmailMsg);


    }

    //protected void btnReferral_Click(object sender, EventArgs e)
    //{
    //    DataTable iDataTable = new DataTable();
    //    string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
    //    SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

    //    iSqlConnection.Open();

    //    string UID = "";

    //    if (Session["UID"] != null)
    //    {
    //        UID = Session["UID"].ToString();
    //    }


    //    int ReferredBy = Convert.ToInt32(Label2.Text);

    //    AddUserToDB_Referral(UID, ReferredBy);
    //}

    //protected void btnSearch_Click(object sender, EventArgs e)
    //{

    //    string srch = txtSearch.Text;

    //    string connectionStr = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();

    //    System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(connectionStr);

    //    connection.Open();

    //  //  string commandStr = "SELECT PID, FirstName, LastName FROM P_Profile WHERE FirstName LIKE " + "'%" + srch + "%'" + " OR LastName LIKE " + "'%" + srch + "%'";
    //    string commandStr = "SELECT PID , FirstName, LastName FROM P_Profile inner join G_Profiles on P_Profile.PID= G_Profiles.uid WHERE (FirstName LIKE " + "'%" + srch + "%'" + " OR LastName LIKE " + "'%" + srch + "%') and  G_Profiles.status=1" +
    //        " union all select AID as PID,CompanyName as Firstname,'' as Lastname from A_Profile inner join G_Profiles on A_Profile.AID= G_Profiles.uid  WHERE  CompanyName LIKE " + "'%" + srch + "%' and  G_Profiles.status=1" +
    //        " union all select BID as PID,CompanyName as Firstname,'' as Lastname from B_Profile inner join G_Profiles on B_Profile.BID= G_Profiles.uid WHERE  CompanyName LIKE " + "'%" + srch + "%'and  G_Profiles.status=1" +
    //        " union all select SID AS PID ,Firstname,Lastname  from S_Profile inner join G_Profiles on S_Profile.SID= G_Profiles.uid WHERE   (FirstName LIKE " + "'%" + srch + "%'" + " OR LastName LIKE " + "'%" + srch + "%') and  G_Profiles.status=1 ";
   
    //    System.Data.SqlClient.SqlCommand myCommand = new System.Data.SqlClient.SqlCommand(commandStr, connection);

    //    System.Data.SqlClient.SqlDataReader myReader = myCommand.ExecuteReader();





    //    GV.DataSource = myReader;

    //    GV.DataBind();

    //    myReader.Close();

    //    string retGV = Convert.ToString(myCommand.ExecuteScalar());

    //    if (retGV == "")
    //    {
    //        Label3.Text = "Your search result returned no value. Please search again";
    //        btnReferral.Visible = false;

    //    }
    //    else
    //    {
    //        Label3.Visible = false;

    //    }

    //    connection.Close();

    //}

    //protected void GV_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    btnReferral.Visible = true;
    //    GridViewRow row = GV.SelectedRow;

    //    string fname = row.Cells[0].Text;
    //    string lname = row.Cells[1].Text;

    //    btnReferral.Text = "Add as your referrar: " + fname + " " + lname;

    //    Label2.Text = row.Cells[2].Text;
    //}

    //protected void btnClick_Click(object sender, EventArgs e)
    //{
    //    DataTable iDataTable = new DataTable();
    //    string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
    //    SqlConnection iSqlConnection = new SqlConnection(iConnectionString);


    //    iSqlConnection.Open();

    //    string UID = "";

    //    if (Session["UID"] != null)
    //    {
    //        UID = Session["UID"].ToString();
    //    }


    //    string Membership = rbtnlistMembership.SelectedValue;
    //    string sStartDate = DateTime.Now.ToString();
    //    string sEndDate = DateTime.Now.ToString();
    //    string sTennure = rbtnTenure.SelectedValue;
    //    if (sTennure == "1")
    //    {
    //        if (rbtnlistMembership.SelectedItem.Value == "1")
    //            sEndDate = DateTime.Now.AddYears(20).ToString();
    //        if (rbtnlistMembership.SelectedItem.Value == "2")
    //        {
    //            sEndDate = DateTime.Now.AddYears(1).ToString();
    //            _fSubscriptionCharge = 144;
    //        }
    //        if (rbtnlistMembership.SelectedItem.Value == "3")
    //        {
    //            sEndDate = DateTime.Now.AddYears(1).ToString();
    //            _fSubscriptionCharge = 240;
    //        }
    //    }
    //    else
    //    {
    //        if (rbtnlistMembership.SelectedItem.Value == "1")
    //            sEndDate = DateTime.Now.AddYears(20).ToString();
    //        if (rbtnlistMembership.SelectedItem.Value == "2")
    //        {
    //            sEndDate = DateTime.Now.AddMonths(1).ToString();
    //            _fSubscriptionCharge = 14.99;
    //        }
    //        if (rbtnlistMembership.SelectedItem.Value == "3")
    //        {
    //            sEndDate = DateTime.Now.AddMonths(1).ToString();
    //            _fSubscriptionCharge = 24.99;
    //        }
    //    }
    //    string Feature="0";
    //    if(rbnFeature.SelectedItem.Value=="-1")
    //        Feature="0";
    //    else
    //         Feature=rbnFeature.SelectedItem.Value;
    //    AddUserToDB_Membership(UID, Membership, sStartDate, sEndDate, rbtnTenure.SelectedItem.Value, Feature);

    //    if (rbtnlistMembership.SelectedItem.Value == "1") // because Silver Membership is free
    //        Response.Redirect("PSignupConfirmation.aspx");
    //    else
    //        Response.Redirect("APaymentProcessing.aspx?User=P&amount=" + _fSubscriptionCharge);// Gold and Platinum member will charged
    //}
    //protected void rbnFeature_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    chkAddon.Checked = false;
    //    if (rbtnlistMembership.SelectedItem.Value == "2")
    //    {
    //        if ((rbnFeature.SelectedItem.Value == "1") || (rbnFeature.SelectedItem.Value == "2") || (rbnFeature.SelectedItem.Value == "3") || (rbnFeature.SelectedItem.Value == "7"))
    //            chkAddon.Visible = true;
    //        else
    //        {
    //            chkAddon.Visible = false;
    //            lblAddonCharge.Visible = false;
    //        }
    //    }
    //    if (rbtnlistMembership.SelectedItem.Value == "3")
    //    {
    //        if ((rbnFeature.SelectedItem.Value == "7"))
    //            chkAddon.Visible = true;
    //        else
    //        {
    //            chkAddon.Visible = false;
    //            lblAddonCharge.Visible = false;
    //        }
    //    }
        
    //}
    //protected void chkAddon_CheckedChanged(object sender, EventArgs e)
    //{
    //    if (chkAddon.Checked == true)
    //    {
    //        lblAddonCharge.Visible = true;
    //        lblAddonCharge.Text = "Add-ons with one time free - addons will be for lifetime as long they have membership of Gold or Platinum ";
    //    }
    //    else
    //    {
    //        lblAddonCharge.Visible = false;
    //    }
    //}
    protected void btnSilver_Click(object sender, ImageClickEventArgs e)
    {
        DataTable iDataTable = new DataTable();
        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);
        string sStartDate = DateTime.Now.ToString();
        string sEndDate = DateTime.Now.ToString();

        iSqlConnection.Open();

        string UID = "";

        if (Session["UID"] != null)
        {
            UID = Session["UID"].ToString();
        }
           sEndDate = DateTime.Now.AddYears(1).ToString();

        Session["MembershipFee"] = platinum.ToString();
      
        AddUserToDB_Membership(UID, "1", sStartDate, sEndDate, 0, 0, 0, 0, 0);
        Session["MembershipFee"] = gold.ToString();

        Response.Redirect("PSignupMembershipConfirmation.aspx");
    }
    protected void btnGold_Click(object sender, ImageClickEventArgs e)
    {
        DataTable iDataTable = new DataTable();
        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);
        string sStartDate = DateTime.Now.ToString();
        string sEndDate = DateTime.Now.ToString();
        string sMembership = "";
        iSqlConnection.Open();

        string UID = "";

        if (Session["UID"] != null)
        {
            UID = Session["UID"].ToString();
        }
        if (DropDownList2.SelectedItem.Value == "14.99")
        {
            sMembership = "2";
            sEndDate = DateTime.Now.AddMonths(1).ToString();
        }
        else
        {
            sMembership = "3";
            sEndDate = DateTime.Now.AddYears(1).ToString();
        }
        Session["MembershipFee"] = platinum.ToString();
        int iCoverLetter = 0;
        if (CheckBox1.Checked)
            iCoverLetter = 1;
        int iResume = 0;
        if (CheckBox2.Checked)
            iResume = 1;
        int iPreInterview = 0;
        if (CheckBox3.Checked)
            iPreInterview = 1;
        int iJobVerificationGold = 0;
        if (CheckBox4.Checked)
            iJobVerificationGold = 1;
      
      
        AddUserToDB_Membership(UID, sMembership, sStartDate, sEndDate, iCoverLetter, iResume, iPreInterview, iJobVerificationGold, 0);
        Session["MembershipFee"] = gold.ToString();
      
        Response.Redirect("PaymentProcessing.aspx?User=P");
    }
    protected void btnPlatinum_Click(object sender, ImageClickEventArgs e)
    {
        DataTable iDataTable = new DataTable();
        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);
        string sStartDate = DateTime.Now.ToString();
        string sEndDate = DateTime.Now.ToString();
        string sMembership = "";
        iSqlConnection.Open();

        string UID = "";

        if (Session["UID"] != null)
        {
            UID = Session["UID"].ToString();
        }
        if (DropDownList2.SelectedItem.Value == "19.99")
        {
            sMembership = "4";
            sEndDate = DateTime.Now.AddMonths(1).ToString();
        }
        else
        {
            sMembership = "5";
            sEndDate = DateTime.Now.AddYears(1).ToString();
        }
       
        Session["MembershipFee"] = platinum.ToString();
       
        int iJobVerificationPlatinum = 0;
        if (CheckBox5.Checked)
            iJobVerificationPlatinum = 1;
        AddUserToDB_Membership(UID, sMembership, sStartDate, sEndDate, 0, 0, 0, 0, iJobVerificationPlatinum);
        Response.Redirect("PaymentProcessing.aspx?User=P");
    }
}
