﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpOut.master" AutoEventWireup="true" CodeFile="PSignupReferences.aspx.cs" Inherits="PSignupReferences" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<center><h1>Add references</h1></center><br /><br />




<table border="0" cellpadding="50" cellspacing="0" width="80%">
  <tr>
    <td width="50%" valign="top" align="left">

        <asp:Label ID="Label1" runat="server"></asp:Label>
    <br />
    <br />
    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
&nbsp;Name<br />
    <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
&nbsp;Title<br />
    <asp:TextBox ID="txtCompany" runat="server"></asp:TextBox>
&nbsp;Company<br />
    <asp:Button ID="btnSubmit" runat="server" Text="Add" 
        onclick="btnSubmit_Click" />   
         <br />
        <asp:Label ID="Label2" runat="server"></asp:Label>
    </td>
    <td width="50%" valign="top" align="left">
    

    Your added references: <br /><br />

        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="ReferenceID" DataSourceID="SqlDataSource1" 
            EnableModelValidation="True"    onrowdeleted="GridView1_RowDeleted"  
            EmptyDataText="You have not added any references yet" 
>
            <Columns>
                <asp:BoundField DataField="ReferenceID" HeaderText="ReferenceID" 
                    ReadOnly="True" SortExpression="ReferenceID" />
                <asp:BoundField DataField="UID" HeaderText="UID" SortExpression="UID" />
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                <asp:BoundField DataField="Company" HeaderText="Company" 
                    SortExpression="Company" />
                <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
                <asp:BoundField DataField="Field1" HeaderText="Field1" 
                    SortExpression="Field1" />
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
            DeleteCommand="DELETE FROM [P_References] WHERE [ReferenceID] = @ReferenceID" 
            InsertCommand="INSERT INTO [P_References] ([UID], [Name], [Company], [Title], [Field1]) VALUES (@UID, @Name, @Company, @Title, @Field1)" 
            SelectCommand="SELECT [ReferenceID], [UID], [Name], [Company], [Title], [Field1] FROM [P_References] WHERE ([UID] = @UID)" 
            UpdateCommand="UPDATE [P_References] SET [UID] = @UID, [Name] = @Name, [Company] = @Company, [Title] = @Title, [Field1] = @Field1 WHERE [ReferenceID] = @ReferenceID">
            <DeleteParameters>
                <asp:Parameter Name="ReferenceID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="UID" Type="Int32" />
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="Company" Type="String" />
                <asp:Parameter Name="Title" Type="String" />
                <asp:Parameter Name="Field1" Type="String" />
            </InsertParameters>
            <SelectParameters>
                <asp:SessionParameter Name="UID" SessionField="UID" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="UID" Type="Int32" />
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="Company" Type="String" />
                <asp:Parameter Name="Title" Type="String" />
                <asp:Parameter Name="Field1" Type="String" />
                <asp:Parameter Name="ReferenceID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
    
    
    </td>
  </tr>
  <tr>
    <td width="50%" valign="top" align="left">

   <h3> <a href="PSignupAccomplishmentsAndSkills.aspx">Go Next Page »</a></h3></td>
    <td width="50%" valign="top" align="left">
    

        &nbsp;</td>
  </tr>
</table>



</asp:Content>

