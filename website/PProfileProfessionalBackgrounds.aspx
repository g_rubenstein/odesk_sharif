﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpp.master" AutoEventWireup="true"
    CodeFile="PProfileProfessionalBackgrounds.aspx.cs" Inherits="PProfileProfessionalBackgrounds" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script>
    function goBack() {
        window.history.back()
    }
 </script>
    <center>
        <h1>
            Update
            Professional Backgrounds</h1>
    </center>
    <table border="0" cellpadding="50" cellspacing="0" width="80%">
        <tr>
            <td width="50%" valign="top" align="left">
                <asp:Label ID="Label1" runat="server"></asp:Label>
                <br />
                <br />
                <p>
                    Fields:</p>
                <p>
                    <asp:CheckBoxList ID="CheckBoxList1" runat="server" DataSourceID="SqlDataSource_Fields"
                        DataTextField="Field" DataValueField="Field">
                    </asp:CheckBoxList>
                </p>
                <p>
                    <asp:SqlDataSource ID="SqlDataSource_Fields" runat="server" ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>"
                        SelectCommand="SELECT [ProfessionalBackgroundsChoicesID], [Field] FROM [P_ProfessionalBackgroundsChoices] WHERE ([Field] IS NOT NULL)">
                    </asp:SqlDataSource>
                </p>
                <p>
                    &nbsp;</p>
                <p>
                    Professions:</p>
                <p>
                    <asp:CheckBoxList ID="CheckBoxList2" runat="server" DataSourceID="SqlDataSource_Professions"
                        DataTextField="Profession" DataValueField="Profession">
                    </asp:CheckBoxList>
                </p>
                <p>
                    <asp:SqlDataSource ID="SqlDataSource_Professions" runat="server" ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>"
                        SelectCommand="SELECT [ProfessionalBackgroundsChoicesID], [Profession] FROM [P_ProfessionalBackgroundsChoices] WHERE ([Profession] IS NOT NULL)">
                    </asp:SqlDataSource>
                </p>
                <p>
                    &nbsp;</p>
                <p>
                    Skills:</p>
                <p>
                    <asp:CheckBoxList ID="CheckBoxList3" runat="server" DataSourceID="SqlDataSource_Skills"
                        DataTextField="Skills" DataValueField="Skills">
                    </asp:CheckBoxList>
                    <asp:SqlDataSource ID="SqlDataSource_Skills" runat="server" ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>"
                        SelectCommand="SELECT [ProfessionalBackgroundsChoicesID], [Skills] FROM [P_ProfessionalBackgroundsChoices] WHERE ([Skills] IS NOT NULL)">
                    </asp:SqlDataSource>
                </p>
                <p>
                    &nbsp;</p>
                <p>
                    &nbsp;</p>
                <p>
                    <asp:Button ID="btnSubmit" runat="server" Text="ADD" OnClick="btnSubmit_Click" Style="height: 26px"
                        Width="200px" />
                </p>
            </td>
            <td width="50%" valign="top" align="left">
                Your added fields: &nbsp;
                <br />
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="ProfessionalBackgroundsSelectionsID"
                    DataSourceID="SqlDataSource1" EmptyDataText="There are no data records to display."
                    CellPadding="3" SelectedIndex="3" ShowHeader="False">
                    <Columns>
                        <asp:BoundField DataField="Field" HeaderText="Field" SortExpression="Field" />
                        <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>"
                    DeleteCommand="DELETE FROM [P_ProfessionalBackgroundsSelections] WHERE [ProfessionalBackgroundsSelectionsID] = @ProfessionalBackgroundsSelectionsID"
                    InsertCommand="INSERT INTO [P_ProfessionalBackgroundsSelections] ([UID], [Field]) VALUES (@UID, @Field)"
                    SelectCommand="SELECT [ProfessionalBackgroundsSelectionsID], [UID], [Field] FROM [P_ProfessionalBackgroundsSelections] WHERE ([UID] = @UID) AND ([Field]<>'') "
                    UpdateCommand="UPDATE [P_ProfessionalBackgroundsSelections] SET [UID] = @UID, [Field] = @Field WHERE [ProfessionalBackgroundsSelectionsID] = @ProfessionalBackgroundsSelectionsID">
                    <DeleteParameters>
                        <asp:Parameter Name="ProfessionalBackgroundsSelectionsID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="UID" Type="Int32" />
                        <asp:Parameter Name="Field" Type="String" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:SessionParameter Name="UID" SessionField="UID" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="UID" Type="Int32" />
                        <asp:Parameter Name="Field" Type="String" />
                        <asp:Parameter Name="ProfessionalBackgroundsSelectionsID" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                &nbsp;
                <br />
                &nbsp;
                <br />
                Your added professions:&nbsp;
                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="ProfessionalBackgroundsSelectionsID"
                    DataSourceID="SqlDataSource2" EmptyDataText="There are no data records to display."
                    CellPadding="3" ShowHeader="False">
                    <Columns>
                        <asp:BoundField DataField="Profession" HeaderText="Profession" SortExpression="Profession" />
                        <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>"
                    DeleteCommand="DELETE FROM [P_ProfessionalBackgroundsSelections] WHERE [ProfessionalBackgroundsSelectionsID] = @ProfessionalBackgroundsSelectionsID"
                    InsertCommand="INSERT INTO [P_ProfessionalBackgroundsSelections] ([UID], [Profession]) VALUES (@UID, @Profession)"
                    SelectCommand="SELECT [ProfessionalBackgroundsSelectionsID], [UID], [Profession] FROM [P_ProfessionalBackgroundsSelections] WHERE ([UID] = @UID) AND ([Profession]<>'')"
                    UpdateCommand="UPDATE [P_ProfessionalBackgroundsSelections] SET [UID] = @UID, [Profession] = @Profession WHERE [ProfessionalBackgroundsSelectionsID] = @ProfessionalBackgroundsSelectionsID">
                    <DeleteParameters>
                        <asp:Parameter Name="ProfessionalBackgroundsSelectionsID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="UID" Type="Int32" />
                        <asp:Parameter Name="Profession" Type="String" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:SessionParameter Name="UID" SessionField="UID" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="UID" Type="Int32" />
                        <asp:Parameter Name="Profession" Type="String" />
                        <asp:Parameter Name="ProfessionalBackgroundsSelectionsID" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                &nbsp;
                <br />
                &nbsp;&nbsp;&nbsp;
                <br />
                Your added skills: &nbsp;
                <br />
                <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" DataKeyNames="ProfessionalBackgroundsSelectionsID"
                    DataSourceID="SqlDataSource3" EmptyDataText="There are no data records to display."
                    CellPadding="3" ShowHeader="False">
                    <Columns>
                        <asp:BoundField DataField="Skills" HeaderText="Skills" SortExpression="Skills" />
                        <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>"
                    DeleteCommand="DELETE FROM [P_ProfessionalBackgroundsSelections] WHERE [ProfessionalBackgroundsSelectionsID] = @ProfessionalBackgroundsSelectionsID"
                    InsertCommand="INSERT INTO [P_ProfessionalBackgroundsSelections] ([UID], [Skills]) VALUES (@UID, @Skills)"
                    SelectCommand="SELECT [ProfessionalBackgroundsSelectionsID], [UID], [Skills] FROM [P_ProfessionalBackgroundsSelections] WHERE ([UID] = @UID) AND ([Skills]<>'')"
                    UpdateCommand="UPDATE [P_ProfessionalBackgroundsSelections] SET [UID] = @UID, [Skills] = @Skills WHERE [ProfessionalBackgroundsSelectionsID] = @ProfessionalBackgroundsSelectionsID">
                    <DeleteParameters>
                        <asp:Parameter Name="ProfessionalBackgroundsSelectionsID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="UID" Type="Int32" />
                        <asp:Parameter Name="Skills" Type="String" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:SessionParameter Name="UID" SessionField="UID" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="UID" Type="Int32" />
                        <asp:Parameter Name="Skills" Type="String" />
                        <asp:Parameter Name="ProfessionalBackgroundsSelectionsID" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td width="50%" valign="top" align="left">
                <h3><input type="button" value="Go Back" onclick="goBack()" style="width: 129px"></h3>
            </td>
            <td width="50%" valign="top" align="left">
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
