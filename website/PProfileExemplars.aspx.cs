﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

public partial class PProfileExemplars : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string SPID = "";
        string SFirstName = "";

        if (Session["UID"] != null)
        {
            SPID = Session["UID"].ToString();
        }

        DataTable iDataTable = new DataTable();

        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();

        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

        iSqlConnection.Open();

        SqlDataAdapter iSqlDataAdapter = new SqlDataAdapter("SELECT FirstName FROM P_Profile WHERE  (PID = '" + SPID + "')", iSqlConnection);

        iSqlDataAdapter.Fill(iDataTable);

        if (iDataTable.Rows.Count > 0)
        {
            SFirstName = iDataTable.Rows[0][0].ToString();
        }

        iSqlConnection.Close();

        Label1.Text = "Hello " + SFirstName + ", please add some example work file or videos to your profile.";

    }

    public bool AddUserToDB(string UID, string VideoURL)
    {
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;
        SqlConnection con = null;
        SqlDataAdapter da = null;
        SqlDataReader reader = null;
        SqlCommand cmd = null;

        String sQuery = "INSERT INTO P_WorkExVideos (UID, VideoURL) VALUES ('" + UID + "','" + VideoURL + "')";

        try
        {
            con = new SqlConnection(conString);
            cmd = new SqlCommand(sQuery, con);
            con.Open();
            cmd.ExecuteNonQuery();

        }

        catch
        {

        }

        finally
        {
            con.Close();
        }

        return true;
    }



    public bool AddUserToDB_WorkExFiles(string UID, string FileName)
    {
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;
        SqlConnection con = null;
        SqlDataAdapter da = null;
        SqlDataReader reader = null;
        SqlCommand cmd = null;

        String sQuery = "INSERT INTO P_WorkExFiles (UID, FileName) VALUES ('" + UID + "','" + FileName + "')";

        try
        {
            con = new SqlConnection(conString);
            cmd = new SqlCommand(sQuery, con);
            con.Open();
            cmd.ExecuteNonQuery();

        }

        catch
        {

        }

        finally
        {
            con.Close();
        }

        return true;
    }


    protected void btnFileName_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            DataTable iDataTable = new DataTable();
            string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
            SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

            iSqlConnection.Open();

            string UID = Session["UID"].ToString();

            string FileName = "";
            if (ucDoc.HasFile)
            {
                try
                {
                    FileName = Path.GetFileName(ucDoc.FileName);
                    ucDoc.SaveAs(Server.MapPath("files/P/exemplars/") + UID + "-" + FileName);

                    FileName = UID + "-" + FileName;

                }
                catch (Exception ex)
                {

                }
            }

            AddUserToDB_WorkExFiles(UID, FileName);

            iSqlConnection.Close();
        }
    
    }
    protected void btnVideoURL_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            DataTable iDataTable = new DataTable();
            string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
            SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

            iSqlConnection.Open();

            string UID = Session["UID"].ToString();

            string VideoURL = "";
            if (UCVideo.HasFile)
            {
                try
                {
                    VideoURL = Path.GetFileName(UCVideo.FileName);
                    ucDoc.SaveAs(Server.MapPath("files/P/exemplars/") + UID + "-" + VideoURL);

                    VideoURL = UID + "-" + VideoURL;

                }
                catch (Exception ex)
                {

                }
            }

            AddUserToDB(UID, VideoURL);

            iSqlConnection.Close();
        }

    }
    protected void checkfilesize(object source, ServerValidateEventArgs args)
    {
        string data = args.Value;
        args.IsValid = false;
        double filesize = ucDoc.FileContent.Length;
        if (filesize > 5000000)
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }
    protected void checkfilesize1(object source, ServerValidateEventArgs args)
    {
        string data = args.Value;
        args.IsValid = false;
        double filesize = UCVideo.FileContent.Length;
        if (filesize > 10000000)
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }
}