﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class PSignupAccomplishmentsAndSkills : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string SPID = "";
        string SFirstName = "";

        if (Session["UID"] != null)
        {
            SPID = Session["UID"].ToString();
        }

        DataTable iDataTable = new DataTable();

        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();

        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

        iSqlConnection.Open();

        SqlDataAdapter iSqlDataAdapter = new SqlDataAdapter("SELECT FirstName FROM P_Profile WHERE  (PID = '" + SPID + "')", iSqlConnection);

        iSqlDataAdapter.Fill(iDataTable);

        if (iDataTable.Rows.Count > 0)
        {
            SFirstName = iDataTable.Rows[0][0].ToString();
        }

        iSqlConnection.Close();

        Label1.Text = "Hello " + SFirstName + ", please add some accomplishments and skills to your profile.";

    }

    public bool AddUserToDB(string UID, string Accomplishments)
    {
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;
        SqlConnection con = null;
        SqlDataAdapter da = null;
        SqlDataReader reader = null;
        SqlCommand cmd = null;

        String sQuery = "INSERT INTO P_Accomplishments (UID, Accomplishments) VALUES ('" + UID + "','" + Accomplishments + "')";

        try
        {
            con = new SqlConnection(conString);
            cmd = new SqlCommand(sQuery, con);
            con.Open();
            cmd.ExecuteNonQuery();

        }

        catch
        {

        }

        finally
        {
            con.Close();
        }

        return true;
    }

    public bool AddUserToDB_Skills(string UID, string Skills)
    {
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;
        SqlConnection con = null;
        SqlDataAdapter da = null;
        SqlDataReader reader = null;
        SqlCommand cmd = null;

        String sQuery = "INSERT INTO P_Skills (UID, Skills) VALUES ('" + UID + "','" + Skills + "')";

        try
        {
            con = new SqlConnection(conString);
            cmd = new SqlCommand(sQuery, con);
            con.Open();
            cmd.ExecuteNonQuery();

        }

        catch
        {

        }

        finally
        {
            con.Close();
        }

        return true;
    }

    protected void btnAccomplishments_Click(object sender, EventArgs e)
    {

        DataTable iDataTable = new DataTable();
        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

        iSqlConnection.Open();

        string UID = "";

        if (Session["UID"] != null)
        {
            UID = Session["UID"].ToString();
        }


        string Accomplishments = txtAccomplishments.Text;

        AddUserToDB(UID, Accomplishments);

        Response.Redirect("PSignupAccomplishmentsAndSkills.aspx");

    }

    protected void btnSkills_Click(object sender, EventArgs e)
    {


        DataTable iDataTable = new DataTable();
        string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
        SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

        iSqlConnection.Open();

        string UID = "";

        if (Session["UID"] != null)
        {
            UID = Session["UID"].ToString();
        }

        string Skills = txtSkills.Text;

        AddUserToDB_Skills(UID, Skills);

        Response.Redirect("PSignupAccomplishmentsAndSkills.aspx");

    }
}