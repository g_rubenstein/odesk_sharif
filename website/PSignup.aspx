﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpOut.master" AutoEventWireup="true"
    CodeFile="PSignup.aspx.cs" Inherits="PSignup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajaxToolkit:ToolkitScriptManager runat="server" />
     <script type ="text/javascript">

         function checkfilesize(source, arguments) {
             arguments.IsValid = false;

             var axo = new ActiveXObject("Scripting.FileSystemObject");
             thefile = axo.getFile(arguments.Value);
             var size = thefile.size;
             if (size > 5000) {
                 arguments.IsValid = false;
             }
             else {
                 arguments.IsValid = true;
             }


         }
</script>
    <%--<script type="text/javascript">
        function OpenModalPopup(url) {
           // var url = "fileupload.aspx";


            var resultobject = window.showModalDialog(url, "", "");

            if (resultobject != null) {
                //some business logic
                document.getElementById('<%=txtImage.ClientID %>').value = resultobject.Name;
                //location.reload();
            }
            return false;
        }


        function GetValueFromChild(myVal, mode) {
            if (mode == 'P')
                document.getElementById('<%=txtImage.ClientID %>').value = myVal;
            if (mode == 'C')
                document.getElementById('<%=txtCover.ClientID %>').value = myVal;
            if (mode == 'R')
                document.getElementById('<%=txtResumeU.ClientID %>').value = myVal;

        }


    </script>
    <script type="text/javascript">
     // This function will execute after file uploaded successfully
     function uploadComplete() {
         document.getElementById('<%=lblResumeU.ClientID %>').innerHTML = "File Uploaded Successfully";
     }
     // This function will execute if file upload fails
     function uploadError() {
         document.getElementById('<%=lblResumeU.ClientID %>').innerHTML = "File upload Failed.";
     }
     // This function will execute after file uploaded successfully
     function uploadComplete1() {
         document.getElementById('<%=lblCoverLetterU.ClientID %>').innerHTML = "File Uploaded Successfully";
     }
     // This function will execute if file upload fails
     function uploadError1() {
         document.getElementById('<%=lblCoverLetterU.ClientID %>').innerHTML = "File upload Failed.";
     }
     // This function will execute after file uploaded successfully
     function uploadComplete2() {
         document.getElementById('<%=StatusLabel.ClientID %>').innerHTML = "File Uploaded Successfully";
     }
     // This function will execute if file upload fails
     function uploadError2() {
         document.getElementById('<%=StatusLabel.ClientID %>').innerHTML = "File upload Failed.";
     }
</script>--%>
    <center>
        <h1>
            Professional Member Sign-Up
        </h1>
    </center>
    <br />
    <br />
    <table id="tb02" border="0" cellpadding="2" cellspacing="0" width="100%">
        <tr>
            <td width="40%" align="right">
                First Name:
            </td>
            <td width="60%" align="left">
                <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirstName"
                    ErrorMessage="Please enter your first name"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                Middle Initial:
            </td>
            <td width="60%" align="left">
                <asp:TextBox ID="txtMiddleInitial" runat="server" Width="20px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                Last Name:
            </td>
            <td width="60%" align="left">
                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLastName"
                    ErrorMessage="Please enter your last name"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                Gender:
            </td>
            <td width="60%" align="left">
                <asp:DropDownList ID="dpdGender" runat="server">
                    <asp:ListItem>Female</asp:ListItem>
                    <asp:ListItem>Male</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                Date of Birth:
            </td>
            <td width="60%" align="left">
                <asp:TextBox ID="txtDOB" runat="server"></asp:TextBox>
                 <ajaxToolkit:CalendarExtender ID="CalendarExtender" runat="server" TargetControlID="txtDOB"
                                    Format="yyyy-MM-dd" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDOB"
                    ErrorMessage="Please enter your date of birth"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                Address:
            </td>
            <td width="60%" align="left">
                <asp:TextBox ID="txtAddress" runat="server" Width="450px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                City:
            </td>
            <td width="60%" align="left">
                <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                State:
            </td>
            <td width="60%" align="left">
                <asp:DropDownList ID="dpdState" runat="server">
                    <asp:ListItem Selected="True">AL</asp:ListItem>
                    <asp:ListItem>AK</asp:ListItem>
                    <asp:ListItem>AZ</asp:ListItem>
                    <asp:ListItem>AR</asp:ListItem>
                    <asp:ListItem>CA</asp:ListItem>
                    <asp:ListItem>CO</asp:ListItem>
                    <asp:ListItem>CT</asp:ListItem>
                    <asp:ListItem>DE</asp:ListItem>
                    <asp:ListItem>DC</asp:ListItem>
                    <asp:ListItem>FL</asp:ListItem>
                    <asp:ListItem>GA</asp:ListItem>
                    <asp:ListItem>HI</asp:ListItem>
                    <asp:ListItem>ID</asp:ListItem>
                    <asp:ListItem>IL</asp:ListItem>
                    <asp:ListItem>IN</asp:ListItem>
                    <asp:ListItem>IA</asp:ListItem>
                    <asp:ListItem>KS</asp:ListItem>
                    <asp:ListItem>KY</asp:ListItem>
                    <asp:ListItem>LA</asp:ListItem>
                    <asp:ListItem>ME</asp:ListItem>
                    <asp:ListItem>MD</asp:ListItem>
                    <asp:ListItem>MA</asp:ListItem>
                    <asp:ListItem>MI</asp:ListItem>
                    <asp:ListItem>MN</asp:ListItem>
                    <asp:ListItem>MS</asp:ListItem>
                    <asp:ListItem>MO</asp:ListItem>
                    <asp:ListItem>MT</asp:ListItem>
                    <asp:ListItem>NE</asp:ListItem>
                    <asp:ListItem>NV</asp:ListItem>
                    <asp:ListItem>NH</asp:ListItem>
                    <asp:ListItem>NJ</asp:ListItem>
                    <asp:ListItem>NM</asp:ListItem>
                    <asp:ListItem>NY</asp:ListItem>
                    <asp:ListItem>NC</asp:ListItem>
                    <asp:ListItem>ND</asp:ListItem>
                    <asp:ListItem>OH</asp:ListItem>
                    <asp:ListItem>OK</asp:ListItem>
                    <asp:ListItem>OR</asp:ListItem>
                    <asp:ListItem>PA</asp:ListItem>
                    <asp:ListItem>RI</asp:ListItem>
                    <asp:ListItem>SC</asp:ListItem>
                    <asp:ListItem>SD</asp:ListItem>
                    <asp:ListItem>TN</asp:ListItem>
                    <asp:ListItem>TX</asp:ListItem>
                    <asp:ListItem>UT</asp:ListItem>
                    <asp:ListItem>VT</asp:ListItem>
                    <asp:ListItem>VA</asp:ListItem>
                    <asp:ListItem>WA</asp:ListItem>
                    <asp:ListItem>WV</asp:ListItem>
                    <asp:ListItem>WI</asp:ListItem>
                    <asp:ListItem>WY</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                Zip Code:
            </td>
            <td width="60%" align="left">
                <asp:TextBox ID="txtZipCode" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtZipCode"
                    ErrorMessage="Please enter your zip code"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                Primary Phone:
            </td>
            <td width="60%" align="left">
                <asp:TextBox ID="txtPrimaryPhone" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPrimaryPhone"
                    ErrorMessage="Please enter your phone number"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                Secondary Phone:
            </td>
            <td width="60%" align="left">
                <asp:TextBox ID="txtSecondaryPhone" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                E-mail:
            </td>
            <td width="60%" align="left">
                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtEmail"
                    ErrorMessage="Please enter your email" Display="Dynamic"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                        ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="Please enter a valid email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        Display="Dynamic"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                Password:
            </td>
            <td width="60%" align="left">
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="140px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtPassword"
                    ErrorMessage="Please enter a password"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                Upload Picture:
            </td>
            <td width="60%" align="left">
                 <%-- <asp:TextBox ID="txtImage" runat="server"></asp:TextBox >
                <input id="btnOpenNewPage" type="button" runat="server" value="Upload Picture" />
                <ajaxToolkit:AjaxFileUpload Visible="false" ThrobberID="myThrobber" ContextKeys="fred"
                    AllowedFileTypes="jpg,jpeg" MaximumNumberOfFiles="1" runat="server" ID="FileUploadControl"
                    OnUploadComplete="AjaxFileUpload1_UploadComplete" />
               <ajax:AsyncFileUpload ID="FileUploadControl" OnClientUploadComplete="uploadComplete2" OnClientUploadError="uploadError2"
CompleteBackColor="White" Width="350px" runat="server" UploaderStyle="Modern" UploadingBackColor="#CCFFFF"
ThrobberID="imgLoad" OnUploadedComplete="fileUploadComplete2" />--%>
                  <asp:FileUpload id="FileUploadControl" runat="server" />
                 <asp:RegularExpressionValidator ID="uplValidator" runat="server" ControlToValidate="FileUploadControl"
 ErrorMessage=".jpg, .png & gif formats are allowed" 
 ValidationExpression="(.*?)\.(|jpeg|gif|png|jpg)$"></asp:RegularExpressionValidator>
  <asp:CustomValidator ID="cvPicture" runat="server"  
                    Text="" ToolTip="" 
                    ErrorMessage="FileSize Exceeds the Limits.Please Try uploading smaller size files." 
                    ControlToValidate="FileUploadControl"  
                    ononclient = "checkfilesize2"></asp:CustomValidator>
                <asp:Label runat="server" ID="StatusLabel" />
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                Type Your Cover Letter:
            </td>
            <td width="60%" align="left">
                <asp:TextBox ID="txtCoverLetterT" runat="server" Height="150px" TextMode="MultiLine"
                    Width="450px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                Upload Cover Letter:
            </td>
            <td width="60%" align="left">
                <asp:FileUpload id="ucCoverLetterU" runat="server" />
                <asp:RegularExpressionValidator ID="revCoverLetter" runat="server" ControlToValidate="ucCoverLetterU"
 ErrorMessage=".doc, .docx,.txt & pdf formats are allowed" 
 ValidationExpression="(.*?)\.(|doc|pdf|docx)$"></asp:RegularExpressionValidator>
               <asp:CustomValidator ID="cvCoverLetterU" runat="server"  
                    Text="" ToolTip="" 
                    ErrorMessage="FileSize Exceeds the Limits.Please Try uploading smaller size files." 
                    ControlToValidate="ucCoverLetterU"  
                    ononclient = "checkfilesize1"></asp:CustomValidator>
                <%--  <ajax:AsyncFileUpload ID="ucCoverLetterU" OnClientUploadComplete="uploadComplete1"  OnClientUploadError="uploadError1"
CompleteBackColor="White" Width="350px" runat="server" UploaderStyle="Modern" UploadingBackColor="#CCFFFF"
ThrobberID="imgLoad" OnUploadedComplete="fileUploadComplete1" />
                <asp:TextBox ID="txtCover" runat="server"></asp:TextBox>
                <input id="btnCV" type="button" runat="server" value="Upload CV"  />--%>
                <%--<ajaxToolkit:AjaxFileUpload Visible="false" ThrobberID="myThrobber" ContextKeys="fred"
                    AllowedFileTypes="jpg,jpeg" MaximumNumberOfFiles="1" runat="server" ID="ucCoverLetterU"
                    OnUploadComplete="AjaxFileUpload2_UploadComplete" />--%>
                <asp:Label runat="server" ID="lblCoverLetterU" />
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                Type Your Resume:
            </td>
            <td width="60%" align="left">
                <asp:TextBox ID="txtResume" runat="server" Height="150px" TextMode="MultiLine" Width="450px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                Upload Resume:
            </td>
            <td width="60%" align="left">
                <%--<ajax:AsyncFileUpload ID="ucResumeU" OnClientUploadComplete="uploadComplete" OnClientUploadError="uploadError"
CompleteBackColor="White" Width="350px" runat="server" UploaderStyle="Modern" UploadingBackColor="#CCFFFF"
ThrobberID="imgLoad" OnUploadedComplete="fileUploadComplete" />--%>
                <%--<asp:TextBox ID="txtResumeU" runat="server"></asp:TextBox>
                <input id="btnResume" type="button" runat="server" value="Upload Resume"  />
                <ajaxToolkit:AjaxFileUpload Visible="false" ThrobberID="myThrobber" ContextKeys="fred"
                    AllowedFileTypes="jpg,jpeg" MaximumNumberOfFiles="1" runat="server" ID="ucResumeU"
                    OnUploadComplete="AjaxFileUpload3_UploadComplete" />--%>
                 <asp:FileUpload id="ucResumeU" runat="server" />
                  <asp:RegularExpressionValidator ID="revReume" runat="server" ControlToValidate="ucResumeU"
 ErrorMessage=".doc, .docx,.txt & pdf formats are allowed" 
 ValidationExpression="(.*?)\.(|doc|pdf|docx)$"></asp:RegularExpressionValidator>
                <asp:Label runat="server" ID="lblResumeU" />
                 <asp:CustomValidator ID="NewPasswordCustomValidator" runat="server"  
                    Text="" ToolTip="" 
                    ErrorMessage="FileSize Exceeds the Limits.Please Try uploading smaller size files." 
                    ControlToValidate="ucResumeU"  
                    ononclient = "checkfilesize"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                Objective:
            </td>
            <td width="60%" align="left">
                <asp:TextBox ID="txtObjective" runat="server" Height="150px" TextMode="MultiLine"
                    Width="450px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                Type YouTube Video URL:
            </td>
            <td width="60%" align="left">
                <asp:TextBox ID="txtVideo" runat="server"></asp:TextBox>
                <asp:Button ID="btnVideo" runat="server" Text="Add" Width="70px" />
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                &nbsp;
            </td>
            <td width="60%" align="left">
                <asp:Button ID="btnSubmit" runat="server" Text="SUBMIT" OnClick="btnSubmit_Click" />
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                &nbsp;
            </td>
            <td width="60%" align="left">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="40%" align="right">
                &nbsp;
            </td>
            <td width="60%" align="left">
                &nbsp;
            </td>
        </tr>
        <asp:HiddenField ID="hidPicture" runat="server" />
        <asp:HiddenField ID="hidCoverLetter" runat="server" />
        <asp:HiddenField ID="hidResume" runat="server" />
    </table>
    
</asp:Content>
