﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpOut.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ajaxToolkit:ToolkitScriptManager runat="Server" EnablePartialRendering="true" ID="ScriptManager1" />



<div id="HomeMain">



<div id="login">


<div id="loginP">
<div style="position: absolute; top: 12px; left: 445px;">
        <ajaxToolkit:BalloonPopupExtender ID="BalloonPopupExtender1" runat="server" TargetControlID="link1" BalloonPopupControlID="Panel1" Position="TopLeft" BalloonStyle="Rectangle" BalloonSize="Medium" UseShadow="true" />
            <asp:HyperLink ID="link1" runat="server"><img src="images/q.gif"  /></asp:HyperLink>
        <asp:Panel ID="Panel1" runat="server">Professional login is blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah </asp:Panel>
</div> 


<asp:Panel ID="PanelP" runat="server"  DefaultButton="btnPLogin">
<table id="tb01" border="0" cellpadding="2" cellspacing="0" width="100%" >
        <tr>
        <td width="40%" align="right">&nbsp;</td>
        <td width="60%" align="left">
            <h3>PROFESSIONAL LOGIN</h3></td>
        </tr>
        <tr>
        <td width="40%" align="right">Email:</td>
        <td width="60%" align="left">
            <asp:TextBox ID="txtEmail" runat="server" Width="135px"></asp:TextBox></td>
        </tr>
        <tr>
        <td width="40%" align="right" >Password:</td>
        <td width="60%" align="left">
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="135px"></asp:TextBox>
            <asp:Button 
                ID="btnPLogin" runat="server" onclick="btnPLogin_Click" Text="Login" 
                BackColor="#194CDE" ForeColor="#CCCCCC" BorderColor="#194CDE" 
                Font-Bold="True" style="padding-bottom: 1px;" />
            </td>
        </tr>
        <tr>
        <td width="40%" align="right"></td>
        <td width="60%" align="left">
            <a href="ForgotPassword.aspx">Forgot password?</a>
        </td>
        </tr>
        <tr>
        <td width="40%" align="right"></td>
        <td width="60%" align="left">
            <a href="PSignup.aspx"><b>New user?</b></a>
        </td>
        </tr>
        <tr>
        <td width="40%" align="right">&nbsp;</td>
        <td width="60%" align="left">
            <asp:Label ID="Confirmation" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
        </td>
        </tr>
    </table>
</asp:Panel>
</div>

<div id="loginB">
<div style="position: absolute; top: 12px; left: 445px;">
        <ajaxToolkit:BalloonPopupExtender ID="BalloonPopupExtender2" runat="server" TargetControlID="link2" BalloonPopupControlID="Panel2" Position="TopLeft" BalloonStyle="Rectangle" BalloonSize="Medium" UseShadow="true" />
            <asp:HyperLink ID="Link2" runat="server"><img src="images/q.gif"  /></asp:HyperLink>
        <asp:Panel ID="Panel2" runat="server">Business login is blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah </asp:Panel>
</div> 

<asp:Panel ID="PanelB" runat="server"  DefaultButton="btnBLogin">
<table id="tb02" border="0" cellpadding="2" cellspacing="0" width="100%">
        <tr>
        <td width="40%" align="right">&nbsp;</td>
        <td width="60%" align="left">
            <h3>BUSINESS LOGIN</h3></td>
        </tr>
        <tr>
        <td width="40%" align="right">Email:</td>
        <td width="60%" align="left">
            <asp:TextBox ID="txtBEmail" runat="server" TabIndex="1" Width="135px"></asp:TextBox></td>
        </tr>
        <tr>
        <td width="40%" align="right">Password:</td>
        <td width="60%" align="left">
            <asp:TextBox ID="txtBPassword" runat="server" TextMode="Password" TabIndex="2" 
                Width="135px"></asp:TextBox>
                
                <asp:Button 
                ID="btnBLogin" runat="server" Text="Login" 
                BackColor="#194CDE" ForeColor="#CCCCCC" BorderColor="#194CDE" 
                Font-Bold="True" style="padding-bottom: 2px; padding-top: -2px;" onclick="btnBLogin_Click" TabIndex="3" />
            </td>
        </tr>
        <tr>
        <td width="40%" align="right"  ></td>
        <td width="60%" align="left"  >
            <a href="ForgotPassword.aspx">Forgot password?</a>
        </td>
        </tr>
        <tr>
        <td width="40%" align="right"></td>
        <td width="60%" align="left">
            <a href="BSignup.aspx"><b>New user?</b></a>
        </td>
        </tr>
        <tr>
        <td width="40%" align="right">&nbsp;</td>
        <td width="60%" align="left">
            <asp:Label ID="lblBConfirmation" runat="server" Font-Bold="True" 
                ForeColor="Red"></asp:Label>
        </td>
        </tr>
    </table>   
</asp:Panel>
</div>


<div id="loginS">
<div style="position: absolute; top: 12px; left: 445px;">
        <ajaxToolkit:BalloonPopupExtender ID="BalloonPopupExtender3" runat="server" TargetControlID="Link3" BalloonPopupControlID="Panel3" Position="TopLeft" BalloonStyle="Rectangle" BalloonSize="Medium" UseShadow="true" />
            <asp:HyperLink ID="Link3" runat="server"><img src="images/q.gif"  /></asp:HyperLink>
        <asp:Panel ID="Panel3" runat="server">Specialists login is blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah </asp:Panel>
</div> 

<asp:Panel ID="PanelS" runat="server"  DefaultButton="btnSLogin">
<table id="tb03" border="0" cellpadding="2" cellspacing="0" width="100%">
        <tr>
        <td width="40%" align="right">&nbsp;</td>
        <td width="60%" align="left">
            <h3>SPECIALIST LOGIN</h3></td>
        </tr>
        <tr>
        <td width="40%" align="right">Email:</td>
        <td width="60%" align="left">
            <asp:TextBox ID="txtSEmail" runat="server" TabIndex="4" Width="135px"></asp:TextBox></td>
        </tr>
        <tr>
        <td width="40%" align="right">Password:</td>
        <td width="60%" align="left">
            <asp:TextBox ID="txtSPassword" runat="server" TextMode="Password" TabIndex="5" 
                Width="135px"></asp:TextBox> <asp:Button 
                ID="btnSLogin" runat="server" Text="Login" 
                BackColor="#194CDE" ForeColor="#CCCCCC" BorderColor="#194CDE" 
                Font-Bold="True" style="padding-bottom: 2px; padding-top: -2px;" TabIndex="5" 
                onclick="btnSLogin_Click" />
            </td>
        </tr>
        <tr>
        <td width="40%" align="right"  ></td>
        <td width="60%" align="left"  >
            <a href="ForgotPassword.aspx">Forgot password?</a>
        </td>
        </tr>
        <tr>
        <td width="40%" align="right"></td>
        <td width="60%" align="left">
            <a href="SSignup.aspx"><b>New user?</b></a>
        </td>
        </tr>
        <tr>
        <td width="40%" align="right">&nbsp;</td>
        <td width="60%" align="left">
            <asp:Label ID="lblSConfirmation" runat="server" Font-Bold="True" 
                ForeColor="Red"></asp:Label>
        </td>
        </tr>
    </table>  
    


        <div style="position: absolute; bottom: 30px; right: 30px;">
        <a href="allspecialst.aspx"><img src="images/browse_specialists.gif" border=0 /></a></div>
    
    
    
     
</asp:Panel>
</div>

<div id="loginA">

<div style="position: absolute; top: 12px; left: 445px;">
        <ajaxToolkit:BalloonPopupExtender ID="BalloonPopupExtender4" runat="server" TargetControlID="link4" BalloonPopupControlID="Panel4" Position="TopLeft" BalloonStyle="Rectangle" BalloonSize="Medium" UseShadow="true" />
            <asp:HyperLink ID="Link4" runat="server"><img src="images/q.gif"  /></asp:HyperLink>
        <asp:Panel ID="Panel4" runat="server">Business Directory login is blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah </asp:Panel>
</div> 

<asp:Panel ID="PanelA" runat="server"  DefaultButton="btnALogin" Width="390px">
<table id="tb04" border="0" cellpadding="2" cellspacing="0" width="100%">
        <tr>
        <td width="40%" align="right">&nbsp;</td>
        <td width="60%" align="left">
            <h3>BUSINESS DIRECTORY LOGIN</h3></td>
        </tr>
        <tr>
        <td width="40%" align="right">Email:</td>
        <td width="60%" align="left">
            <asp:TextBox ID="txtAEmail" runat="server" TabIndex="6" Width="135px"></asp:TextBox></td>
        </tr>
        <tr>
        <td width="40%" align="right">Password:</td>
        <td width="60%" align="left">
            <asp:TextBox ID="txtAPassword" runat="server" TextMode="Password" TabIndex="7" 
                Width="135px"></asp:TextBox> <asp:Button 
                ID="btnALogin" runat="server" Text="Login" 
                BackColor="#194CDE" ForeColor="#CCCCCC" BorderColor="#194CDE" 
                Font-Bold="True" style="padding-bottom: 2px; padding-top: -2px;" TabIndex="8" onclick="btnALogin_Click" 
                />
            </td>
        </tr>
        <tr>
        <td width="40%" align="right"    ></td>
        <td width="60%" align="left"    >
            <a href="ForgotPassword.aspx">Forgot password?</a>
        </td>
        </tr>
        <tr>
        <td width="40%" align="right"></td>
        <td width="60%" align="left">
            <a href="aSignup.aspx"><b>New user?</b></a>
        </td>
        </tr>
        <tr>
        <td width="40%" align="right">&nbsp;</td>
        <td width="60%" align="left">
            <asp:Label ID="lblAConfirmation" runat="server" Font-Bold="True" 
                ForeColor="Red"></asp:Label>
        </td>
        </tr>




    </table>   



    
    <div style="position: absolute; bottom: 30px; right: 30px;"><a href="allads.aspx">
        <img src="images/browse_directory.gif" border=0 /></a></div>
    

</asp:Panel>
</div>




<div id="homebleft">
<iframe src="//www.youtube.com/embed/X9SK052cF3c" frameborder="0" allowfullscreen 
        style="height: 232px; width: 300px"></iframe>
</div>


<div id="homebright" >
    <div style="position: absolute; top: 18px; left: 30px;">
        <img src="images/comma.gif" style="height: 32px; width: 32px" /></div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
    This is a quote, This is a quote, This is a quote, This is a quote, This is a 
    quote, This is a quote, This is a quote, This is a quote, This is" - Joe 
    Smith, Dallas, TX<br />
    <br />
    <div style="position: absolute; top: 92px; left: 25px; height: 32px;">
        <img src="images/comma.gif" style="height: 32px; width: 32px" /></div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This is a quote, This is a quote, This is a quote, This is a quote, This is a 
    quote, This is a quote, This is a quote, This is " - Joe 
    Smith, Dallas, TX<br />
    <br />
    <div style="position: absolute; top: 146px; left: 27px;">
        <img src="images/comma.gif" style="height: 32px; width: 32px" /></div> 
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This is a quote, This is a quote, This is a quote, This is a quote, This 
    is a quote, This is a quote, This is a quote, This is a quote, &quot; - Joe Smith, 
    Dallas<br />
    <br />
    <div style="position: absolute; top: 203px; left: 27px;">
        <img src="images/comma.gif" style="height: 32px; width: 32px" /></div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This is a quote, This is a quote, This is a quote, This is a quote, This is a 
    quote, This is a quote, This is a quote, This is a quote, This is a quote, This 
    is a quote, This is a quote, This is a quote" - Joe Smith, Dallas, TX</div>




</div>



</div>

</asp:Content>

