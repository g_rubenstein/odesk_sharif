﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;
using System.IO;
using AjaxControlToolkit;

public partial class PSignup : System.Web.UI.Page
{
    string PID;
    protected void Page_Load(object sender, EventArgs e)
    {
        //btnOpenNewPage.Attributes.Add("OnClick", "javascript:return OpenModalPopup('fileupload.aspx?mode=P');");
        //btnResume.Attributes.Add("OnClick", "javascript:return OpenModalPopup('fileupload.aspx?mode=R');");
        //btnCV.Attributes.Add("OnClick", "javascript:return OpenModalPopup('fileupload.aspx?mode=C');");
    }

    public string GetRandomString()
    {
        int randomNumber = new Random().Next();
        string RandomString = string.Empty;
        for (int i = 0; i < randomNumber.ToString().Length; i++)
        {
            RandomString += ((char)Convert.ToUInt32(randomNumber.ToString()[i])).ToString();
        }
        return RandomString;
    }

    public bool AddUserToDB(string Email, string Password, string Type, string Status)
    {
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;
        SqlConnection con = null;
        SqlCommand cmd = null;

        String sQuery = "INSERT INTO G_Profiles (Email, Password, Type, Status) VALUES ('" + Email + "','" + Password + "','" + Type + "','" + Status + "')";

        try
        {
            con = new SqlConnection(conString);
            cmd = new SqlCommand(sQuery, con);
            con.Open();
            cmd.ExecuteNonQuery();

        }

        catch
        {

        }

        finally
        {
            con.Close();
        }

        return true;
    }

    public bool AddUserToDB2(string PID, string FirstName, string MiddleInitial, string LastName, string Gender, string DOB, string Address, string City, string State, string Zip, string Phone1, string Phone2, string Profession, string ConfirmCode, string Picture, string CoverLetter, string CoverLetterU, string Resume, string ResumeU, string Objective, string VideoURL)
    {
        String conString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ConnectionString;
        SqlConnection con = null;
        SqlCommand cmd = null;

        String sQuery = "INSERT INTO P_Profile (PID, FirstName, MiddleInitial, LastName, Gender, DOB, Address, City, State, Zip, Phone1, Phone2, Profession, ConfirmCode, Picture, CoverLetter, CoverLetterU, Resume, ResumeU, Objective, VideoURL) VALUES ('" + PID + "','" + FirstName + "','" + MiddleInitial + "','" + LastName + "','" + Gender + "','" + DOB + "','" + Address + "','" + City + "','" + State + "', '" + Zip + "','" + Phone1 + "','" + Phone2 + "','" + Profession + "','" + ConfirmCode + "','" + Picture + "','" + CoverLetter + "','" + CoverLetterU + "','" + Resume + "','" + ResumeU + "', '" + Objective + "','" + VideoURL + "')";

        try
        {
            con = new SqlConnection(conString);
            cmd = new SqlCommand(sQuery, con);
            con.Open();
            cmd.ExecuteNonQuery();
        }

        catch
        {
        }

        finally
        {
            con.Close();
        }

        return true;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string PEmail = txtEmail.Text;
        string PPassword = txtPassword.Text;
        string PFirstName = txtFirstName.Text;
        string PMiddleInitial = txtMiddleInitial.Text;
        string PLastName = txtLastName.Text;
        string PGender = dpdGender.SelectedValue;
        string PDOB = txtDOB.Text;
        string PAddress = txtAddress.Text;
        string PCity = txtCity.Text;
        string PState = dpdState.SelectedValue;
        string PZipCode = txtZipCode.Text;
        string PPrimaryPhone = txtPrimaryPhone.Text;
        string PSecondaryPhone = txtSecondaryPhone.Text;
        string PPicture = "";
        string PCoverLetterT = txtCoverLetterT.Text;
        string PCoverLetterU = "";
        string PResume = txtResume.Text;
        string PResumeU = "";
        string PObjective = txtObjective.Text;
        string PVideo = txtVideo.Text;
        string PProfession = "Profession";
        string PConfirmCode = GetRandomString();
        string PType = "1";
        string PStatus = "0";

        if (Page.IsValid)
        {

            DataTable iDataTable = new DataTable();
            string iConnectionString = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
            SqlConnection iSqlConnection = new SqlConnection(iConnectionString);

            iSqlConnection.Open();
            SqlDataAdapter iSqlDataAdapter = new SqlDataAdapter("SELECT Email FROM G_Profiles WHERE (Email = '" + PEmail + "')", iSqlConnection);
            iSqlDataAdapter.Fill(iDataTable);

            if (iDataTable.Rows.Count == 0)
            {
                AddUserToDB(PEmail, PPassword, PType, PStatus);

                string commandStr = "select UID from G_Profiles WHERE Email= '" + PEmail + "'";

                //System.Threading.Thread.Sleep(5000);

                System.Data.SqlClient.SqlCommand myCommand = new System.Data.SqlClient.SqlCommand(commandStr, iSqlConnection);

                System.Data.SqlClient.SqlDataReader myReader = myCommand.ExecuteReader();

                myReader.Close();

                string PID = Convert.ToString(myCommand.ExecuteScalar());

                //PPicture = hidCoverLetter.Value;
                //////pictures
                ////PPicture = txtImage.Text;
                //MoveFiles(PPicture, PID, "pictures");
                ////Covers
                ////PCoverLetterU = txtCover.Text;
                //MoveFiles(PCoverLetterU, PID, "coverletters");
                ////Resumes
                ////PResumeU = txtResumeU.Text;
                //MoveFiles(PResumeU, PID, "resumes");
                if (FileUploadControl.HasFile)
                {
                    try
                    {
                        string filename = Path.GetFileName(FileUploadControl.FileName);
                        FileUploadControl.SaveAs(Server.MapPath("files/P/pictures") + PID + "-" + filename);

                        PPicture = PID + "-" + filename;

                    }
                    catch (Exception ex)
                    {
                        StatusLabel.Text =
                            "The file could not be uploaded. The following error occured: " + ex.Message;
                    }
                }

                //coverletter

                if (ucCoverLetterU.HasFile)
                {
                    try
                    {
                        string coverletters = Path.GetFileName(ucCoverLetterU.FileName);
                        ucCoverLetterU.SaveAs(Server.MapPath("files/P/coverletters/") + PID + "-" + coverletters);

                        PCoverLetterU = PID + "-" + coverletters;

                    }
                    catch (Exception ex)
                    {
                        lblCoverLetterU.Text =
                            "The file could not be uploaded. The following error occured: " + ex.Message;
                    }
                }

                //resume

                if (ucResumeU.HasFile)
                {
                    try
                    {
                        string resumes = Path.GetFileName(ucResumeU.FileName);
                        ucResumeU.SaveAs(Server.MapPath("files/P/resumes/") + PID + "-" + resumes);

                        PResumeU = PID + "-" + resumes;
                    }
                    catch (Exception ex)
                    {
                        lblResumeU.Text =
                            "The file could not be uploaded. The following error occured: " + ex.Message;
                    }
                }



                AddUserToDB2(PID, PFirstName, PMiddleInitial, PLastName, PGender, PDOB, PAddress, PCity, PState, PZipCode, PPrimaryPhone, PSecondaryPhone, PProfession, PConfirmCode, PPicture, PCoverLetterT, PCoverLetterU, PResume, PResumeU, PObjective, PVideo);

                Session["UID"] = PID;
                Response.Redirect("PSignupReferences.aspx");

            }

            if (iDataTable.Rows.Count != 0)
            {
                Label1.Text = "Sorry this email already exisits in our system!</br>Please type a different email address and try again.";
            }

            iSqlConnection.Close();
        }

    }
    //protected void fileUploadComplete(object sender, AsyncFileUploadEventArgs e)
    //{
    //    //string filename = System.IO.Path.GetFileName(ucResumeU.FileName);
    //    //ucResumeU.SaveAs(Server.MapPath("files/resumes/") + PID + "-" + filename);
    //}

    //protected void fileUploadComplete1(object sender, AsyncFileUploadEventArgs e)
    //{

    //}
    //protected void fileUploadComplete2(object sender, AsyncFileUploadEventArgs e)
    //{

    //}
    //protected void AjaxFileUpload1_UploadComplete(object sender, AjaxControlToolkit.AjaxFileUploadEventArgs e)
    //{
    //    string sPicture = System.IO.Path.GetFileName(e.FileName);
    //    FileUploadControl.SaveAs(Server.MapPath("Temp/") + sPicture);
    //    //Session["Picture"] = sPicture;

    //}
    //protected void AjaxFileUpload2_UploadComplete(object sender, AjaxControlToolkit.AjaxFileUploadEventArgs e)
    //{
    //    string sCoverletter = System.IO.Path.GetFileName(e.FileName);
    //    ucCoverLetterU.SaveAs(Server.MapPath("Temp/") + sCoverletter);
    //    //Session["Coverletter"] = sCoverletter;

    //}
    //protected void AjaxFileUpload3_UploadComplete(object sender, AjaxControlToolkit.AjaxFileUploadEventArgs e)
    //{
    //    string sResume = System.IO.Path.GetFileName(e.FileName);
    //    ucCoverLetterU.SaveAs(Server.MapPath("Temp/") + sResume);
    //   hidResume.Value = sResume;
    //    // Session["Resume"] = sResume;
    //}

    private void MoveFiles(string sFileName, string sUID, string sDestination)
    {
        string sSourceFilePath = Server.MapPath("Temp/") + sFileName;
        string sNewFileName = sUID + "-" + sFileName;

        string sDestinationFilePath = Server.MapPath("files" + @"\" + sDestination + @"\") + sNewFileName;
        if(File.Exists(sSourceFilePath))
         File.Move(sSourceFilePath, sDestinationFilePath);

    }
    protected void checkfilesize(object source, ServerValidateEventArgs args)
    {
        string data = args.Value;
        args.IsValid = false;
        double filesize = ucResumeU.FileContent.Length;
        if (filesize > 5000000)
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }
    protected void checkfilesize2(object source, ServerValidateEventArgs args)
    {
        string data = args.Value;
        args.IsValid = false;
        double filesize = FileUploadControl.FileContent.Length;
        if (filesize > 5000000)
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }
    protected void checkfilesize1(object source, ServerValidateEventArgs args)
    {
        string data = args.Value;
        args.IsValid = false;
        double filesize = ucCoverLetterU.FileContent.Length;
        if (filesize > 5000000)
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }
}
