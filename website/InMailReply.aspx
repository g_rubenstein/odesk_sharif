﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpg.master" AutoEventWireup="true" CodeFile="InMailReply.aspx.cs" Inherits="InMailReply" %>
<%@ Register src="UCInMailNav.ascx" tagname="UCInMailNav" tagprefix="Nav" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <br />
    <br />
    <br />
<center>
<nav:ucinmailnav ID="UCInMailNav1" runat="server" />  
</center>
    <br />
    <br />
    <br />
    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" 
        DataKeyNames="InMailID" DataSourceID="SqlDataSource1" DefaultMode="Insert" 
        Height="50px" Width="750px" CellPadding="5">
        <Fields>
            <asp:BoundField DataField="InMailID" HeaderText="InMailID" 
                InsertVisible="False" ReadOnly="True" SortExpression="InMailID" />
            <asp:TemplateField HeaderText="Date" SortExpression="Date">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Date") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate> 
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Date") %>'    Value='<%# DateTime.Now.ToString() %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="hiddencol" />
                <ItemStyle CssClass="hiddencol" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Email Subject" SortExpression="Subject">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Subject") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Subject") %>' 
                        Width="613px"></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("Subject") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Email Message" SortExpression="Msg">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Msg") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Height="172px" 
                        Text='<%# Bind("Msg") %>' TextMode="MultiLine" Width="613px"></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("Msg") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Status" HeaderText="Status" 
                SortExpression="Status" >
            <HeaderStyle CssClass="hiddencol" />
            <ItemStyle CssClass="hiddencol" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="mailto" SortExpression="mailto">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("mailto") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("mailto") %>'   Value='<%# Request.QueryString["topid"] %>' ></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("mailto") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="hiddencol" />
                <ItemStyle CssClass="hiddencol" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="mailfrom" SortExpression="mailfrom">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("mailfrom") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("mailfrom") %>'   Value='<%# (string)Session["UID"] %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("mailfrom") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle CssClass="hiddencol" />
                <ItemStyle CssClass="hiddencol" />
            </asp:TemplateField>
            <asp:CommandField ShowInsertButton="True" InsertText="Send Mail" 
                ButtonType="Image" CancelImageUrl="~/images/cancel.gif" 
                InsertImageUrl="~/images/submit.gif" />
        </Fields>
    </asp:DetailsView>
    <br />
    <br />
    <br />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
        DeleteCommand="DELETE FROM [G_InMail] WHERE [InMailID] = @InMailID" 
        InsertCommand="INSERT INTO [G_InMail] ([Date], [Subject], [Msg], [Status], [mailto], [mailfrom]) VALUES (@Date, @Subject, @Msg, 1, @mailto, @mailfrom)" 
        ProviderName="<%$ ConnectionStrings:stdbConnectionString.ProviderName %>" 
        SelectCommand="SELECT [InMailID], [Date], [Subject], [Msg], [Status], [mailto], [mailfrom] FROM [G_InMail]" 
        UpdateCommand="UPDATE [G_InMail] SET [Date] = @Date, [Subject] = @Subject, [Msg] = @Msg, [Status] = @Status, [mailto] = @mailto, [mailfrom] = @mailfrom WHERE [InMailID] = @InMailID">
        <DeleteParameters>
            <asp:Parameter Name="InMailID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Subject" Type="String" />
            <asp:Parameter Name="Msg" Type="String" />
            <asp:Parameter Name="Status" Type="Int32" />
            <asp:Parameter Name="mailto" Type="Int32" />
            <asp:Parameter Name="mailfrom" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Subject" Type="String" />
            <asp:Parameter Name="Msg" Type="String" />
            <asp:Parameter Name="Status" Type="Int32" />
            <asp:Parameter Name="mailto" Type="Int32" />
            <asp:Parameter Name="mailfrom" Type="Int32" />
            <asp:Parameter Name="InMailID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    <br />



</asp:Content>

