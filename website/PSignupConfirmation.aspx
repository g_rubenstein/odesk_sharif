﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpOut.master" AutoEventWireup="true" CodeFile="PSignupConfirmation.aspx.cs" Inherits="PSignupConfirmation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



    <p>
        Thans you for signing up!</p>
    <p>
        &nbsp;</p>
    <p>
        We have sent you a membership verification email. Please check your email and 
        follow the instruction to complete your signup.</p>
    <p>
        &nbsp;</p>
    <p>
        Thanks for being a UBHub member!</p>
    <p>
        &nbsp;</p>
    <p>
        - The UBHub Team.
    </p>
</asp:Content>

