﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpNoCSS.master"  ValidateRequest="false"  AutoEventWireup="true" CodeFile="PProfile_Exemplars.aspx.cs" Inherits="PProfile_Exemplars" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1>Portfolio Files</h1>

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="WorkExFilesID" DataSourceID="SqlDataSource1" 
        EmptyDataText="There are no data records to display." BorderWidth="0px" 
        ShowHeader="False">
        <Columns>
            <asp:TemplateField HeaderText="FileName" SortExpression="FileName">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FileName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "files/examplersfiles/" + Eval("FileName") %>'>
                    <h3><asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label></h3>
                    </asp:HyperLink>
                    
                </ItemTemplate>
                <HeaderStyle BorderWidth="0px" />
                <ItemStyle BorderWidth="0px" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
        DeleteCommand="DELETE FROM [P_WorkExFiles] WHERE [WorkExFilesID] = @WorkExFilesID" 
        InsertCommand="INSERT INTO [P_WorkExFiles] ([UID], [FileName]) VALUES (@UID, @FileName)" 
        SelectCommand="SELECT [WorkExFilesID], [UID], [FileName] FROM [P_WorkExFiles] WHERE ([UID] = @UID)" 
        UpdateCommand="UPDATE [P_WorkExFiles] SET [UID] = @UID, [FileName] = @FileName WHERE [WorkExFilesID] = @WorkExFilesID">
        <DeleteParameters>
            <asp:Parameter Name="WorkExFilesID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="UID" Type="Int32" />
            <asp:Parameter Name="FileName" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="UID" QueryStringField="UID" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="UID" Type="Int32" />
            <asp:Parameter Name="FileName" Type="String" />
            <asp:Parameter Name="WorkExFilesID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    <br />

<h1>Portfolio Video Files</h1>

    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="WorkExVideosID" DataSourceID="SqlDataSource2" 
        EmptyDataText="There are no data records to display." BorderWidth="0px" 
        ShowHeader="False">
        <Columns>
            <asp:TemplateField HeaderText="VideoURL" SortExpression="VideoURL">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("VideoURL") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "files/examplersfiles/" + Eval("VideoURL") %>'>
                    <h3><asp:Label ID="Label1" runat="server" Text='<%# Eval("VideoURL") %>'></asp:Label></h3>
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
        DeleteCommand="DELETE FROM [P_WorkExVideos] WHERE [WorkExVideosID] = @WorkExVideosID" 
        InsertCommand="INSERT INTO [P_WorkExVideos] ([UID], [VideoURL]) VALUES (@UID, @VideoURL)" 
        SelectCommand="SELECT [WorkExVideosID], [UID], [VideoURL] FROM [P_WorkExVideos] WHERE ([UID] = @UID)" 
        UpdateCommand="UPDATE [P_WorkExVideos] SET [UID] = @UID, [VideoURL] = @VideoURL WHERE [WorkExVideosID] = @WorkExVideosID">
        <DeleteParameters>
            <asp:Parameter Name="WorkExVideosID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="UID" Type="Int32" />
            <asp:Parameter Name="VideoURL" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="UID" QueryStringField="UID" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="UID" Type="Int32" />
            <asp:Parameter Name="VideoURL" Type="String" />
            <asp:Parameter Name="WorkExVideosID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />

<br /><br />


</asp:Content>

