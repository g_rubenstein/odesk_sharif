﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mpg.master" AutoEventWireup="true" CodeFile="InMailSentRead.aspx.cs" Inherits="InMailSentRead" %>
<%@ Register src="UCInMailNav.ascx" tagname="UCInMailNav" tagprefix="Nav" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <br />
    <br />
    <br />
<center>
<nav:ucinmailnav ID="UCInMailNav1" runat="server" />  
</center>
    <br />
    <br />
    <br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="InMailID" DataSourceID="SqlDataSource1" 
        EmptyDataText="There are no data records to display." 
        EnableModelValidation="True">
        <Columns>
            <asp:BoundField DataField="InMailID" HeaderText="InMailID" ReadOnly="True" 
                SortExpression="InMailID" />
            <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" />
            <asp:BoundField DataField="Subject" HeaderText="Subject" 
                SortExpression="Subject" />
            <asp:TemplateField HeaderText="Msg" SortExpression="Msg">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Msg") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Msg") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Status" HeaderText="Status" 
                SortExpression="Status" />
            <asp:BoundField DataField="mailto" HeaderText="mailto" 
                SortExpression="mailto" />
            <asp:BoundField DataField="mailfrom" HeaderText="mailfrom" 
                SortExpression="mailfrom" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:stdbConnectionString %>" 
        DeleteCommand="DELETE FROM [G_InMail] WHERE [InMailID] = @InMailID" 
        InsertCommand="INSERT INTO [G_InMail] ([Date], [Subject], [Msg], [Status], [mailto], [mailfrom]) VALUES (@Date, @Subject, @Msg, @Status, @mailto, @mailfrom)" 
        SelectCommand="SELECT [InMailID], [Date], [Subject], [Msg], [Status], [mailto], [mailfrom] FROM [G_InMail] WHERE (([mailfrom] = @mailfrom) AND ([InMailID] = @InMailID))" 
        UpdateCommand="UPDATE [G_InMail] SET [Date] = @Date, [Subject] = @Subject, [Msg] = @Msg, [Status] = @Status, [mailto] = @mailto, [mailfrom] = @mailfrom WHERE [InMailID] = @InMailID">
        <DeleteParameters>
            <asp:Parameter Name="InMailID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Subject" Type="String" />
            <asp:Parameter Name="Msg" Type="String" />
            <asp:Parameter Name="Status" Type="Int32" />
            <asp:Parameter Name="mailto" Type="Int32" />
            <asp:Parameter Name="mailfrom" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="mailfrom" SessionField="UID" Type="Int32" />
            <asp:QueryStringParameter Name="InMailID" QueryStringField="InMailID" 
                Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Date" Type="DateTime" />
            <asp:Parameter Name="Subject" Type="String" />
            <asp:Parameter Name="Msg" Type="String" />
            <asp:Parameter Name="Status" Type="Int32" />
            <asp:Parameter Name="mailto" Type="Int32" />
            <asp:Parameter Name="mailfrom" Type="Int32" />
            <asp:Parameter Name="InMailID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>

