﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class mpg : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        PanelMemberViewP.Visible = false;

        string UID = "";
        string QID = "";

        UID = (string)Session["UID"];
        QID = Request.QueryString["UID"];

        HomeLink.NavigateUrl = "default.aspx";
        LogOutLink.NavigateUrl = "logout.aspx";

        if (UID == QID)
        {
            PanelMemberViewP.Visible = true;
            HomeLink.Visible = false;
            MemberHome.Visible = true;
            LogOutLink.Visible = true;
        }
        else
        {
            PanelMemberViewP.Visible = false;

            HomeLink.Visible = true;
        }

        lblID.Text = UID;

// Type
        if (UID != null)
        {
            string connectionStr = ConfigurationManager.ConnectionStrings["stdbConnectionString"].ToString();
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(connectionStr);
            connection.Open();
            string commandStr = "SELECT Type FROM G_Profiles WHERE UID =" + UID;
            System.Data.SqlClient.SqlCommand myCommand = new System.Data.SqlClient.SqlCommand(commandStr, connection);
            System.Data.SqlClient.SqlDataReader myReader = myCommand.ExecuteReader();
            myReader.Close();
            string type = Convert.ToString(myCommand.ExecuteScalar());

            if (type == "1")
            {
                MemberHome.NavigateUrl = "pprofile.aspx?uid=" + UID;
                ProfileUpdateLink.NavigateUrl = "pProfileUpdate.aspx?uid=" + QID;
                //CalendarLink.NavigateUrl = "pCalendar.aspx?uid=" + QID;      
            }
            if (type == "2")
            {
                MemberHome.NavigateUrl = "bprofile.aspx?uid=" + UID;
                ProfileUpdateLink.NavigateUrl = "bProfileUpdate.aspx?uid=" + QID;
                //CalendarLink.NavigateUrl = "bCalendar.aspx?uid=" + QID;      
            }
            if (type == "3")
            {
                MemberHome.NavigateUrl = "sprofile.aspx?uid=" + UID;
                ProfileUpdateLink.NavigateUrl = "sProfileUpdate.aspx?uid=" + QID;
                //CalendarLink.NavigateUrl = "sCalendar.aspx?uid=" + QID;      
            }
            if (type == "4")
            {
                MemberHome.NavigateUrl = "aprofile.aspx?uid=" + UID;
                ProfileUpdateLink.NavigateUrl = "aProfileUpdate.aspx?uid=" + QID;
                //CalendarLink.NavigateUrl = "aCalendar.aspx?uid=" + QID;
            }
            connection.Close();
        }
        else
        {
            PanelMemberViewP.Visible = false;
            HomeLink.Visible = true;
            MemberHome.Visible = false;
            LogOutLink.Visible = false;
        }

//


        SearchLink.NavigateUrl = "search.aspx?uid=" + QID;
        InMailLink.NavigateUrl = "inmail.aspx?uid=" + QID;
        SettingsUpdateLink.NavigateUrl = "GUpdateSettings.aspx?uid=" + QID;
        RecommendationApproveLink.NavigateUrl = "GProfile_Recommendation_Approve.aspx?uid=" + QID;
  
    }
}
