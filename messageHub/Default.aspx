﻿<%@ Page Title="Home Page" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="messageHub._Default" %>

<html>
<head>
    <title></title>
    <script src="Scripts/json2.js"></script>
    <script src="Scripts/jquery-1.8.2.min.js"></script>
    <script src="Scripts/jquery.signalR-1.1.3.min.js"></script>
    <script src="Scripts/underscore-min.js"></script>
    <script src="Scripts/backbone-min.js"></script>
    <script src="Scripts/chat.js"></script>
    <script src="Scripts/date.format.js"></script>
    <script src="signalr/hubs"></script>
    <link rel="stylesheet" type="text/css" href="Content/signalr-chat.css" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
</head>

<body>
    <h2>Message Hub. User ID=<%=Request.QueryString["id"] ?? "1"%></h2>
    <script type="text/javascript">
        $(function () {
            $.connection.hub.qs = { 'UserId': '<%=Request.QueryString["id"] ?? "1"%>' };

            window._signalrChat.initialize("#chatList", "#messagesContainer");

            $.connection.hub.disconnected(function () {
                setTimeout(function () {
                    $.connection.hub.start().done(function () {
                        window._signalrChat.start();
                    });
                }, 5000);
            });

            $.connection.hub.start();

        });
    </script>

    <div id="chatList">
    </div>
    
    <div id="messagesContainer">
    </div>

    <script type='text/template' id='chat-contactlist-template'>
    <div class="sidebar_container"  style="overflow:hidden; height:32px;">
        <div id="chat_header" class="header_1">
            <div class="user_header">Chat</div>
        </div>
        <!-- INNER body -->
        <div class="scroll_body_content scrollable" style="height: 200px; margin-bottom: 10px;">
            <div class="body_content">
                <table class="uiGrid _51mz conversationContainer" width="360px" cellspacing="0" cellpadding="0" role="log">
                    <tbody>
                        <tr class="_51mx">
                            <td width="100%" class="_51m- vBot _51mw">
                                <div class="conversation" aria-live="polite" aria-atomic="false" id="contactItems">
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
     </div>
    </script>

    <script type='text/template' id='chat-contactitem-template'>
        <div style="display:{{online ? 'inherit' : 'none'}};" class="contact-row">
        <div>
            <div class="_50ke">
                <a class="profileLink" href="#" user-id="{{userId}}">
                    <img class="profilePhoto" src="{{avatar}}">
                </a>
            </div>
            <div class="user_id_cell">
                <div class="_kso fsm direction_ltr _55r0" data-jsid="message">
                    <span>{{ displayName }}</span>
                </div>
            </div>
        </div>
        <div class="vdivider"></div>
        </div>
    </script>






    <script type='text/template' id='chat-message-window-template'>
        <div class="docklet_container" style="overflow:hidden; height:310px;" id="chat_msgw_{{userId}}_container">
            <div class="header_1">
                <div class="user_header"><span>Chat with {{ displayName }}</span>
                    <a class="closeButton"></a>
                    <a class="minButton"></a>
                </div>
            </div>
            <!-- INNER body -->
            <div class="scroll_body_content scrollable" style="height: 253px;">
                <div class="body_content">
                    <table class="uiGrid _51mz conversationContainer" width="360px" cellspacing="0" cellpadding="0" role="log">
                        <tbody>
                            <tr class="_51mx">
                                <td width="100%" class="_51m- vBot _51mw">
                                    <div class="conversation" aria-live="polite" aria-atomic="false" id="u_0_v">
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="chatFooter">
                <div class="chatMessageInput">
                    <textarea></textarea>
                </div>
            </div>
        </div>
    </script>
    
    <script type='text/template' id='chat-message-row-template'>
        <div class="mhs mbs pts myChatConvItem _50dw clearfix small">
            <div class="_50ke">
                <div class="_50x5">
                </div>
                <a {{fromMe ? 'style="display:none;"' : ''}} class="profileLink" href="#">
                    <img class="profilePhoto" src="{{avatar}}">
                </a>
            </div>
            <div class="messages">
                <div class="{{fromMe ? '_kso_r' : '_kso'}} fsm direction_ltr _55r0" data-jsid="message">
                    <span>{{message}}</span><div class="msgDate">{{dateFormat(receiveDate, 'dddd, mmmm dS h:MM TT')}}</div>
                </div>
            </div>
        </div>
        <div class="vdivider"></div>
    </script>
</body>


</html>
