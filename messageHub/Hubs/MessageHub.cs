﻿using messageHubInterfaces;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace messageHub.Hubs
{
    /*
    public class MessageHub : Hub
    {
        private readonly IConnectionInformation _connectionInformation;
        private readonly IMessageStorage _storage;

        #region Constants
        // A user ID parameter from WEB
        readonly string c_UserIdParameter = "UserID";
        #endregion

        #region Connection handling
        public override Task OnConnected()
        {
            Connected();
            return base.OnConnected();
        }

        public override Task OnReconnected()
        {
            Connected();
            return base.OnReconnected();
        }

        public override Task OnDisconnected()
        {
            Disconnected();
            return base.OnDisconnected();
        }

        private void Connected()
        {
            var userId = Context.QueryString[c_UserIdParameter];
            Groups.Add(Context.ConnectionId, userId);
        }

        private void Disconnected()
        {
            var userId = Context.QueryString[c_UserIdParameter];
            Groups.Remove(Context.ConnectionId, userId);
        }
        #endregion

        public MessageHub(IConnectionInformation connectionInformation, IMessageStorage storage)
        {
            _connectionInformation = connectionInformation;
            _storage = storage;
        }

        public void Send(string toUserId, string message)
        {
            var sourceUserId = _connectionInformation.UserIdByConnectionId(Context.ConnectionId);
            var dateSent = DateTime.Now;

            ForwardMessage(toUserId, message, sourceUserId, dateSent);

            StoreMessage(toUserId, message, sourceUserId, dateSent);
        }

        private void ForwardMessage(string toUserId, string message, string sourceUserId, DateTime dateSent)
        {
            if (!string.IsNullOrEmpty(toUserId))
                Clients.Group(toUserId).notifyMessageReceived(sourceUserId, message, false, dateSent);

            if (!string.IsNullOrEmpty(sourceUserId))
                Clients.Group(sourceUserId).notifyMessageReceived(toUserId, message, true, dateSent);
        }

        private void StoreMessage(string toUserId, string message, string sourceUserId, DateTime dateSent)
        {
            if (_storage == null)
                return;

            _storage.Store(new Message()
            {
                FromUserId = sourceUserId,
                ToUserId = toUserId,
                MessageText = message,
                TimeSent = dateSent
            });
        }
     }
     */
}