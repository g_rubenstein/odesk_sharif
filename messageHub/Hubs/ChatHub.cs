﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using messageHubInterfaces;

namespace messageHub.Hubs
{
    public class ChatHub : Hub
    {
        #region Private
            private readonly IUserConnection _userConnectionNotifier = null;
            private readonly IUserRelations _userRelations = null;
            private readonly IUserProfiles _userProfiles = null;
            private readonly IActiveUsers _activeUsers = null;
            private readonly IConnectionInformation _connectionInformation;
            private readonly IMessageStorage _storage;
        #endregion

        #region Constants
            // A user ID parameter from WEB
            readonly string c_UserIdParameter = "UserID";
        #endregion

            public ChatHub(IUserConnection userConnectionNotifier, IUserRelations userRelations, IUserProfiles userProfiles, IActiveUsers activeUsers, IConnectionInformation connectionInformation, IMessageStorage storage)
                : base()
        {
            _userConnectionNotifier = userConnectionNotifier;
            _userRelations = userRelations;
            _userProfiles = userProfiles;
            _activeUsers = activeUsers;
            _connectionInformation = connectionInformation;
            _storage = storage;
        }

        #region Connection handling
        public override Task OnConnected()
        {
            return base.OnConnected().ContinueWith(_ => Connected());
        }

        public override Task OnReconnected()
        {
            return base.OnReconnected().ContinueWith(_ => Connected());
        }

        public override Task OnDisconnected()
        {
            return base.OnDisconnected().ContinueWith(_ => Disconnected());
        }

        private void Connected()
        {
            var userId = Context.QueryString[c_UserIdParameter];
            Groups.Add(Context.ConnectionId, userId).ContinueWith(_ =>
            {
                if (_userConnectionNotifier != null)
                    _userConnectionNotifier.UserConnected(userId, Context.ConnectionId);
                
                //Reply with friends list
                GetFriends();
            });
        }

        private void Disconnected()
        {
            var userId = Context.QueryString[c_UserIdParameter];
            if (_userConnectionNotifier != null)
                _userConnectionNotifier.UserDisconnected(userId, Context.ConnectionId);
        }
        #endregion

        public void EnsureInGroup()
        {
            var userId = Context.QueryString[c_UserIdParameter];
            Groups.Add(Context.ConnectionId, userId);
        }

        #region Contact List handling
        
        public void GetFriends()
        {
            var userId = Context.QueryString[c_UserIdParameter];

            var friends = _userRelations.GetFriends(userId);

            var profiles = _userProfiles.GetUserProfiles(friends);

            var activeUsers = new HashSet<string>(_activeUsers.GetActiveUsers(friends));

            var result = profiles.ToList().ConvertAll(
                p => new { profile = p, 
                    isOnline = activeUsers.Contains(p.UserID) }
                );

            Clients.Client(Context.ConnectionId).notifyGetFriends(result);
        }

        #endregion

        #region IM

        public void Send(string toUserId, string message)
        {
            var sourceUserId = _connectionInformation.UserIdByConnectionId(Context.ConnectionId);
            var dateSent = DateTime.Now;

            ForwardMessage(toUserId, message, sourceUserId, dateSent);

            StoreMessage(toUserId, message, sourceUserId, dateSent);
        }

        private void ForwardMessage(string toUserId, string message, string sourceUserId, DateTime dateSent)
        {
            if (!string.IsNullOrEmpty(toUserId))
                Clients.Group(toUserId).notifyMessageReceived(sourceUserId, message, false, dateSent);

            if (!string.IsNullOrEmpty(sourceUserId))
                Clients.Group(sourceUserId).notifyMessageReceived(toUserId, message, true, dateSent);
        }
        #endregion

        #region Message presistence
        private void StoreMessage(string toUserId, string message, string sourceUserId, DateTime dateSent)
        {
            if (_storage == null)
                return;

            _storage.Store(new Message()
            {
                FromUserId = sourceUserId,
                ToUserId = toUserId,
                MessageText = message,
                TimeSent = dateSent
            });
        }

        public void LoadHistory(string peerUserId)
        {
            TimeSpan daysBack = TimeSpan.FromDays(7);
            
            var userId = Context.QueryString[c_UserIdParameter];

            var messages = _storage.Get(userId, peerUserId, daysBack);

            _storage.Delete(daysBack);

            Clients.Caller.notifyMessageHistory(messages, peerUserId);
        
        }
        #endregion

    }
}