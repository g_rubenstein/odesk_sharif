﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.Configuration;
using Microsoft.AspNet.SignalR;
using Castle.Windsor;
using Castle.Windsor.Installer;
using messageHub;
using Castle.MicroKernel.Registration;
using Microsoft.AspNet.SignalR.Hubs;


namespace messageHub
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            BootstrapContainer();
            
            IntergrateDependencyResolver();

            InitializeSignalRHubs();
        }

        private void InitializeSignalRHubs()
        {
            GlobalHost.Configuration.ConnectionTimeout = TimeSpan.FromSeconds(10);
            GlobalHost.Configuration.DisconnectTimeout = TimeSpan.FromSeconds(20);
            GlobalHost.Configuration.KeepAlive = TimeSpan.FromSeconds(5);

            var hubUrl = WebConfigurationManager.AppSettings["HubURL"] ?? "signalr";
            RouteTable.Routes.MapHubs(hubUrl, new HubConfiguration());
        }

        private void IntergrateDependencyResolver()
        {
            Container.Register(Classes.FromThisAssembly().BasedOn(typeof(IHub)).LifestyleTransient());
            GlobalHost.DependencyResolver = new SignalR.Castle.Windsor.WindsorDependencyResolver(Container);
        }

        void Application_End(object sender, EventArgs e)
        {
            container.Dispose();
        }

        void Application_Error(object sender, EventArgs e)
        {
        }
        
        private static IWindsorContainer container;

        private static void BootstrapContainer()
        {
            container = new WindsorContainer()
                .Install(
                    FromAssembly.This(),
                    FromAssembly.Named("messageHubImplementation")
                );
        }

        public static IWindsorContainer Container
        {
            get { return container; }
        }
    }
}
