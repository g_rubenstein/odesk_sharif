﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.MicroKernel.SubSystems.Configuration;
using messageHubInterfaces;

namespace messageHub.Plumbing
{
    public class UserConnectionNotifierInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IConnectionNotifier>().
                ImplementedBy<ConnectionNotifier>().
                LifeStyle.Transient);
        }
    }
}
