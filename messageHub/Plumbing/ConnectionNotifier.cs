﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using messageHubInterfaces;
using Microsoft.AspNet.SignalR;
using messageHub.Hubs;

namespace messageHub.Plumbing
{
    public class ConnectionNotifier : IConnectionNotifier
    {
        private IUserRelations _userRelations = null;

        public ConnectionNotifier(IUserRelations userRelations)
        {
            _userRelations = userRelations;
        }

        public void UserBecomesOnline(string userId)
        {
            if (_userRelations == null)
                return;

            var friends = _userRelations.GetFriends(userId);

            var hubContext = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
            foreach (var friend in friends)
                hubContext.Clients.Group(friend).notifyUserBecomsOnline(userId);
        }

        public void UserBecomesOffline(string userId)
        {
            if (_userRelations == null)
                return;

            var friends = _userRelations.GetFriends(userId);

            var hubContext = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
            foreach (var friend in friends)
                hubContext.Clients.Group(friend).notifyUserBecomsOffline(userId);
        }

    }
}