﻿// Chat based on signal-r
// Chat class is a singleton
window._signalrChat = {

    _proxy: null,

    initialize: function (chatList, messagesContainer) {

        _.templateSettings = {
            interpolate: /\{\{(.+?)\}\}/g
        };
        this.messageWindowFactory._chat = this;
        this.messageWindowFactory._messagesContainer = messagesContainer;

        this._proxy = $.connection.chatHub;
        var _this = this;

        this._proxy.client.notifyUserBecomsOnline = function (userId) {
            _this.OfflineManager.UserBecomsOnline(userId);
            var models = _this.ChatRowListInstance.where({ userId: userId });
            if(models.length == 0)
                return;
            _.first(models).setOnline(true);
        };

        this._proxy.client.notifyUserBecomsOffline = function (userId) {
            _this.OfflineManager.UserBecomsOffline(userId);
        };

        this._proxy.client.notifyGetFriends = function (friends) {
            _this.notifyGetFriends(friends);
        }

        this._proxy.client.notifyMessageReceived = function (sourceUserId, message, fromMe, receiveDate) {
            _this.messageWindowFactory.messageReceived(sourceUserId, message, fromMe, receiveDate);
        }
        this._proxy.client.notifyMessageHistory = function (messages, peerUserId) {

            _.each(messages, function (message) {
                var fromMe = peerUserId == message.ToUserId;
                var himId = fromMe ? message.ToUserId : message.FromUserId;
                _this.messageWindowFactory.messageReceived(himId, message.MessageText, fromMe, message.TimeSent);
            });
            
        }

        // Row model for chat contact
        this.ChatRowModel = Backbone.Model.extend({
            url: '',
            defaults: function () {
                return {
                    online: false
                };
            },
            setOnline: function (isOnline) {
                this.save({ online: isOnline });
            },
            sync: function (method, collection, options) {
            }
        });

        this.ChatRowList = Backbone.Collection.extend({
            model: _this.ChatRowModel,
            url: ''
        });


        this.ChatRowListInstance = new this.ChatRowList();

        this.OfflineManager.SetContainer(this.ChatRowListInstance);

        // Chat list item view
        this.SignalrChatListItem = Backbone.View.extend({
            tagName: "div",
            template: _.template($('#chat-contactitem-template').html()),
            events: {
                "click .contact-row" : "rowClicked"
            },
            initialize: function () {
                this.listenTo(this.model, 'change', this.render);
                this.listenTo(this.model, 'destroy', this.remove);
            },
            render: function () {
                this.$el.html(this.template(this.model.toJSON()));
                return this;
            },
            rowClicked: function () {
                _this.messageWindowFactory.show(this.model);
                _this._proxy.server.loadHistory(this.model.get("userId"));
            }
        });


        // Chat contact list window view
        var SignalrChatChatListView = Backbone.View.extend({
            template: _.template($('#chat-contactlist-template').html()),
            events: {
                "click .header_1" : "headerClick"
            },

            initialize: function () {
                this.listenTo(_this.ChatRowListInstance, 'reset', this.addAll);
            },

            render: function () {
                this.$el.html(this.template());
            },

            addAll: function () {
                this.render();
                _this.ChatRowListInstance.each(function (model) {
                    var row = new _this.SignalrChatListItem({model: model});
                    $("#contactItems").append(row.render().el);
                }, this);
            },
            headerClick: function () {
                var container = $('.sidebar_container', this.$el);
                if (container.height() <= 32) {
                    container.animate({ height: 242 }, 200);
                }
                else
                {
                    container.animate({ height: 32 }, 200);
                }
            }
        });

        // Chat message window view
        this.SignalrChatMessageView = Backbone.View.extend({
            template: _.template($('#chat-message-window-template').html()),
            events: {
                "click .closeButton": "closeWindow",
                "click .minButton": "minWindow",
                "click .header_1": "minWindow",
                "keypress .chatMessageInput textarea": "keyPressed"
            },

            initialize: function () {
            },

            render: function () {
                this.$el.append(this.template(this.model.toJSON()));
            },
            closeWindow: function () {
                this.options.windowFactory.removeWindow(this.model.get("userId"));
                this.options.windowFactory.relayout();
            },
            minWindow: function() {
                var container = $('.docklet_container', this.$el);
                if (this.isWindowMinimized()) {
                    container.animate({ height: 310 }, 200);
                    $('.header_1', this.$el).removeClass("hasNewMessages");
                }
                else {
                    container.animate({ height: 32 }, 200);
                }
            },
            isWindowMinimized: function()
            {
                return $('.docklet_container', this.$el).height() <= 32;
            },
            messageReceived: function (sourceUserId, message, fromMe, model, receiveDate) {
                var row = new _this.SignalrChatMessageRow();
                row.setElement($('div.conversation', this.$el));
                row.render({
                    fromMe : fromMe,
                    avatar: fromMe ? '' : model.get("avatar"),
                    message: message,
                    receiveDate: _this.dateFromString(receiveDate)
                });

                jQuery(".scroll_body_content", this.$el).scrollTop(jQuery(".body_content", this.$el)[0].scrollHeight);
                if (this.isWindowMinimized()) {
                    $('.header_1', this.$el).addClass("hasNewMessages");
                }
            },
            keyPressed: function (e)
            {
                if (e.keyCode != 13) return;
                var inp = $(".chatMessageInput textarea", this.$el);
                var message = inp.val();
                inp.val('');
                this.options.chat.sendMesage(message, this.model.get("userId"));
                return false;
            }
        });

        this.SignalrChatMessageRow = Backbone.View.extend({
            template: _.template($('#chat-message-row-template').html()),
            render: function (messageObject) {
                this.$el.append(this.template(messageObject));
                return this;
            }
        });

        this._contactList = new SignalrChatChatListView();

        this._contactList.setElement($(chatList));

        this._contactList.render();

    },

    notifyGetFriends: function (friends) {
        var coll = _.map(friends, function (f) {
            return {
                online: f.isOnline,
                avatar : f.profile.AvatarUrl,
                displayName : f.profile.DisplayName,
                userId: f.profile.UserID
            }
        });

        this.ChatRowListInstance.reset(coll);
    },

    messageWindowFactory: {
        _windowInstances: {},

        show: function (chatRowModel) {

            var userId = chatRowModel.get("userId");

            var messageDivId = "chat_msgw_" + userId;

            if ($('#' + messageDivId).length > 0)
                return;

            $("<div/>", {
                id: messageDivId,
                "data-UserId": userId
            }).appendTo(this._messagesContainer);

            var window = new this._chat.SignalrChatMessageView({ model: chatRowModel, windowFactory: this, chat: this._chat });

            window.setElement($('#' + messageDivId));

            window.render();

            var openedWindows = $('div' + this._messagesContainer + ' > div').length;

            $("#chat_msgw_" + userId + "_container").css("margin-right", (openedWindows * 300) - 50);

            this._windowInstances[userId] = window;

        },

        removeWindow: function (userId)
        {
            var windowInstance = this._windowInstances[userId];
            if (!windowInstance)
                return;
            this._windowInstances[userId] = null;
            windowInstance.remove();
        },

        relayout: function () {
            var openedWindows = $('div' + this._messagesContainer + ' > div');
            for (var i = 0; i < openedWindows.length; ++openedWindows) {
                var userId = $(openedWindows[i]).attr("data-UserId");
                $("#chat_msgw_" + userId + "_container").css("margin-right", ((i + 1) * 300) - 50);
            }

        },

        messageReceived: function (sourceUserId, message, fromMe, receiveDate)
        {
            var models = this._chat.ChatRowListInstance.where({ userId: sourceUserId });
            var model = _.first(models);
            if (!model)
                return;

            var windowInstance = this._windowInstances[sourceUserId];
            if (!windowInstance) {
                this.show(model);
            }

            windowInstance = this._windowInstances[sourceUserId];
            windowInstance.messageReceived(sourceUserId, message, fromMe, model, receiveDate);
        }
    },
    
    sendMesage: function (message, userId) {
        this._proxy.server.send(userId, message);
    },
    dateFromString: function(stringDate)
    {
      var a=stringDate.split("T");
      var d=a[0].split("-");
      var t=a[1].split(":");
      return new Date(d[0],(d[1]-1),d[2],t[0],t[1]);
    },

    OfflineManager : {
        _container : null,
        _offlineQueue: [],

        OfflineItem : function(offlineManager, userId, container)
        {
            this._userId = userId;
            this._container = container;
            this._offlineManager = offlineManager;

            var _this = this;

            this.destroy = function () {
                if (_this._timer)
                    clearTimeout(_this._timer);
                this._offlineManager.deleteFromQueue(_this._userId);
            };

            this.restart = function () {
                if (_this._timer)
                    clearTimeout(_this._timer);
                _this._timer = setTimeout(_this.onTimer, 2000);
            };

            this.onTimer = function () {
                var models = _this._container.where({ userId: _this._userId });
                if (models.length != 0)
                    _.first(models).setOnline(false);
                _this.destroy();
            }

            this.restart();
        },
        

        SetContainer: function (container) {
            this._container = container;
        },
        
        UserBecomsOnline: function (userId)
        {
            var existingItem = _.find(this._offlineQueue, function (item) { return item._userId == userId; });
            if (!existingItem)
                return;
            existingItem.destroy();
        },

        UserBecomsOffline: function (userId)
        {
            var existingItem = _.find(this._offlineQueue, function (item) { return item._userId == userId; });
            if (existingItem) {
                existingItem.restart();
                return;
            }
            this._offlineQueue.push(new this.OfflineItem(this, userId, this._container));
        },

        deleteFromQueue: function (userId) {
            this._offlineQueue = _.where(this._offlineQueue, function (item) { return item._userId != userId; });
        }

    }
    

};

