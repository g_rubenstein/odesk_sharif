﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace messageHubInterfaces
{
    public class Message
    {
        public string FromUserId { get; set; }
        public string ToUserId {get; set;}
        public string MessageText { get; set; }
        public DateTime TimeSent { get; set; }
    }
}
