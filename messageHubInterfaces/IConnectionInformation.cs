﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace messageHubInterfaces
{
    public interface IConnectionInformation
    {
        string UserIdByConnectionId(string connectionID);
    }
}
