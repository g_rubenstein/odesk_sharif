﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace messageHubInterfaces
{
    public interface IUserConnection
    {
        void UserConnected(string UserID, string ConnectionID);
        void UserDisconnected(string UserID, string ConnectionID);
    }
}
