﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace messageHubInterfaces
{
    public interface IUserRelations
    {
        // Return friends User ID list for specified user ID
        IList<string> GetFriends(string UserId);
    }
}
