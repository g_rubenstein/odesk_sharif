﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace messageHubInterfaces
{
    public interface IConnectionNotifier
    {
        void UserBecomesOnline(string userId);
        void UserBecomesOffline(string userId);
    }
}
