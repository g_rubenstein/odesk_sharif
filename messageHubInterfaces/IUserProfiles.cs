﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace messageHubInterfaces
{
    public class UserProfile
    {
        public string AvatarUrl { get; set; }
        public string DisplayName { get; set; }
        public string UserID { get; set; }
    }
    
    public interface IUserProfiles
    {
        IList<UserProfile> GetUserProfiles(IList<string> userIds);
    }
}
