﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace messageHubInterfaces
{
    public interface IMessageStorage
    {
        void Store(Message message);
        IList<Message> Get(string fromUserId, string toUserId, TimeSpan fromNow);
        void Delete(TimeSpan fromNow);
    }
}
