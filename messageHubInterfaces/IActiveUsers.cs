﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace messageHubInterfaces
{
    public interface IActiveUsers
    {
        IList<string> GetActiveUsers();
        IList<string> GetActiveUsers(IList<string> users);
    }
}
