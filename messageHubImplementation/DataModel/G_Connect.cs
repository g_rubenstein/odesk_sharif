﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace messageHubImplementation.DataModel
{
    public class G_Connect
    {
        [Key]
        public int ConnectID { get; set; }
        public int CUID { get; set; }
        public int UID { get; set; }
    }
}
