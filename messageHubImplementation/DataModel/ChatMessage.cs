﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace messageHubImplementation.DataModel
{
    public class ChatMessage
    {
        [Key, Column(Order = 1)]
        public int fromId { get; set; }
        [Key, Column(Order = 2)]
        public int toId { get; set; }
        public string message { get; set; }
        [Key, Column(Order = 0)]
        public DateTime timeSent { get; set; }
    }
}
