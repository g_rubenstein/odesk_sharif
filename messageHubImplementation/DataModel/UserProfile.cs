﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace messageHubImplementation.DataModel
{
    public class UserProfile
    {
        public string Email { get; set; }
        [Key]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Picture { get; set; }
        public int? MembershipType { get; set; }
        public string Code { get; set; }
    }
}
