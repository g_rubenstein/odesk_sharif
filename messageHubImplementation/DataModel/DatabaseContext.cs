﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace messageHubImplementation.DataModel
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("Name=stdbConnectionString")
        {
        }

        public System.Data.Entity.DbSet<G_Connect> G_Connect { get; set; }
        public System.Data.Entity.DbSet<ChatMessage> ChatMessages { get; set; }
    }

}
