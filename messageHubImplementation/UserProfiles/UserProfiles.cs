﻿using messageHubInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace messageHubImplementation.UserProfiles
{
    public class UserProfiles : IUserProfiles
    {
        private readonly string _profileQuery = @"SELECT g.email, p.*, m.membership
            FROM [G_Profiles] g
            join (
            select pid as id, firstName, lastName, picture, 'P' as code from p_profile
            union
            select bid as id, firstName, lastName, logo as picture, 'B' as code from b_profile
            union
            select sid as id, firstName, lastName, picture, 'S' as code   from s_profile
            ) p on p.id = g.uid
            left outer join P_Membership m on m.uid = g.uid";
        
        private readonly string _defaultPicture = "noAvatar.png";

        private readonly Dictionary<string, UserProfile> _profiles = new Dictionary<string, UserProfile>();

        public UserProfiles()
        {
            using (var ctx = new messageHubImplementation.DataModel.DatabaseContext())
            {
                var profiles = ctx.Database.SqlQuery<messageHubImplementation.DataModel.UserProfile>(_profileQuery).ToList();
                
                foreach (var profile in profiles)
                {
                    string key = profile.Id.ToString();
                    if (_profiles.ContainsKey(key))
                        continue;
                    _profiles.Add(key, BuildProfile(profile));

                }
            }        
        }
        private UserProfile BuildProfile(messageHubImplementation.DataModel.UserProfile profile)
        {
            var userProfile = new UserProfile()
            {
                UserID = profile.Id.ToString()
            };
            if (!string.IsNullOrEmpty(profile.FirstName))
                userProfile.DisplayName = profile.FirstName;

            if (!string.IsNullOrEmpty(profile.LastName))
            {
                if (!string.IsNullOrEmpty(userProfile.DisplayName))
                    userProfile.DisplayName += ", ";
                userProfile.DisplayName += profile.LastName;
            }

            if (string.IsNullOrEmpty(userProfile.DisplayName))
                userProfile.DisplayName = profile.Email;

            if (string.IsNullOrEmpty(userProfile.DisplayName))
                userProfile.DisplayName = "#" + userProfile.UserID;

            userProfile.AvatarUrl = string.Format("/files/{0}/picture/{1}", profile.Code, 
                !string.IsNullOrEmpty(profile.Picture) ? profile.Picture : _defaultPicture);

            return userProfile;
        }
        
        public IList<UserProfile> GetUserProfiles(IList<string> userIds)
        {
            return userIds.Where(userId => _profiles.ContainsKey(userId)).ToList().ConvertAll(userId => _profiles[userId]).ToList();
        }
    }
}
