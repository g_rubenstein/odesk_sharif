﻿using messageHubImplementation.DataModel;
using messageHubInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace messageHubImplementation.UserRelations
{
    public class UserRelations : IUserRelations
    {
        private readonly Dictionary<int, HashSet<int>> _friendsRelation = new Dictionary<int, HashSet<int>>();

        public UserRelations()
        {
            using (var ctx = new DatabaseContext())
            {
                foreach (var connectRow in ctx.G_Connect)
                {
                    // Skip self frend relations
                    if (connectRow.CUID == connectRow.UID)
                        continue;
                    
                    // Add forward relations
                    if (!_friendsRelation.ContainsKey(connectRow.CUID))
                    {
                        _friendsRelation.Add(connectRow.CUID, new HashSet<int>());
                    }

                    _friendsRelation[connectRow.CUID].Add(connectRow.UID);
                    
                    // Add backward relations
                    if (!_friendsRelation.ContainsKey(connectRow.UID))
                    {
                        _friendsRelation.Add(connectRow.UID, new HashSet<int>());
                    }

                    _friendsRelation[connectRow.UID].Add(connectRow.CUID);
                }
            }
        
        }

        public IList<string> GetFriends(string userId)
        {
            int key = int.Parse(userId);

            if (!_friendsRelation.ContainsKey(key))
                return new List<string>();

            return _friendsRelation[key].ToList().ConvertAll(uid => uid.ToString()).ToList();
        }
    }
}
