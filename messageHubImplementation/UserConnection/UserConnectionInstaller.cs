﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.MicroKernel.SubSystems.Configuration;
using messageHubInterfaces;

namespace messageHubImplementation.Installers
{
    public class UserConnectionInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IUserConnection, IActiveUsers, IConnectionInformation>().
                ImplementedBy<UserConnection>().
                LifeStyle.Singleton);
        }    
    }
}
