﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using messageHubInterfaces;

namespace messageHubImplementation
{
    public class UserConnection : IUserConnection, IActiveUsers, IConnectionInformation
    {
        // user id => connection ID. One user can be logged in many times so use set to store different connection ids
        private Dictionary<string, HashSet<string>> _onlineUsers = new Dictionary<string, HashSet<string>>();
        private Dictionary<string, string> _connectionsToUserIds = new Dictionary<string, string>();
        private object _locker = new object();

        private IConnectionNotifier _connectionNotifier = null;

        public UserConnection(IConnectionNotifier connectionNotifier)
        {
            _connectionNotifier = connectionNotifier;
        }

        public void UserConnected(string userID, string connectionID)
        {
            lock (_locker)
            {
                if (_connectionsToUserIds.ContainsKey(connectionID))
                    _connectionsToUserIds.Remove(connectionID);

                _connectionsToUserIds.Add(connectionID, userID);

                if (_onlineUsers.ContainsKey(userID))
                {
                    _onlineUsers[userID].Add(connectionID);
                    return;
                }

                _onlineUsers.Add(userID, new HashSet<string>() { connectionID });

                if (_connectionNotifier != null)
                    _connectionNotifier.UserBecomesOnline(userID);
            }
        }

        public void UserDisconnected(string userID, string connectionID)
        {
            lock (_locker)
            {
                _connectionsToUserIds.Remove(connectionID);

                if (!_onlineUsers.ContainsKey(userID))
                    return;

                _onlineUsers[userID].Remove(connectionID);

                if (_onlineUsers[userID].Count > 0)
                    return;

                _onlineUsers.Remove(userID);

                if (_connectionNotifier != null)
                    _connectionNotifier.UserBecomesOffline(userID);
            }
        }


        public IList<string> GetActiveUsers()
        {
            lock (_locker)
            {
                return _onlineUsers.Keys.ToList();
            }
        }

        public IList<string> GetActiveUsers(IList<string> users)
        {
            lock (_locker)
            {
                var usersHash = new HashSet<string>(users);

                return _onlineUsers.Keys.Where(k => usersHash.Contains(k)).ToList();
            }
        }

        public string UserIdByConnectionId(string connectionID)
        {
            lock (_locker)
            {
                if (!_connectionsToUserIds.ContainsKey(connectionID))
                    return string.Empty;

                return _connectionsToUserIds[connectionID];
            }
        }
    }
}
