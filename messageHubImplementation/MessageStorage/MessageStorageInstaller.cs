﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using messageHubInterfaces;
namespace messageHubImplementation.Installers
{
    public class MessageStorageInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IMessageStorage>().
                ImplementedBy<messageHubImplementation.MessageStorage.MessageStorage>().
                LifeStyle.Singleton);
        }    
    }
}
