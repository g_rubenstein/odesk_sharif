﻿using messageHubImplementation.DataModel;
using messageHubInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace messageHubImplementation.MessageStorage
{
    public class MessageStorage : IMessageStorage
    {
        public void Store(Message message)
        {
            try
            {
                using (var ctx = new messageHubImplementation.DataModel.DatabaseContext())
                {
                    var messageRow = new ChatMessage()
                    {
                        fromId = int.Parse(message.FromUserId),
                        toId = int.Parse(message.ToUserId),
                        message = message.MessageText,
                        timeSent = message.TimeSent
                    };
                    ctx.ChatMessages.Add(messageRow);
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
            }
        }

        public IList<Message> Get(string fromUserId, string toUserId, TimeSpan fromNow)
        {
            int fromId = int.Parse(fromUserId);
            int toId = int.Parse(toUserId);

            DateTime rangeDate = DateTime.Now - fromNow;

            using (var ctx = new messageHubImplementation.DataModel.DatabaseContext())
            {
                var msgs = (from m in ctx.ChatMessages
                           where ((m.fromId == fromId && m.toId == toId) || (m.fromId == toId && m.toId == fromId) && m.timeSent > rangeDate)
                           orderby(m.timeSent) descending
                           select m).Take(20);

                var asList = msgs.ToList().OrderBy(m => m.timeSent).ToList();
                
                return asList.ConvertAll(m => new Message()
                           {
                               FromUserId = m.fromId.ToString(),
                               ToUserId = m.toId.ToString(),
                               MessageText = m.message,
                               TimeSent = m.timeSent
                           }).ToList();

            }
        }

        public void Delete(TimeSpan fromNow)
        {
            DateTime rangeDate = DateTime.Now - fromNow;
            using (var ctx = new messageHubImplementation.DataModel.DatabaseContext())
            {
                ctx.Database.ExecuteSqlCommand("delete from chatMessages where timeSent < {0}", rangeDate);
            }
        }
    }
}
